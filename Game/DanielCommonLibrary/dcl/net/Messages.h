#ifndef _DCL_MESSAGES_H_
#define _DCL_MESSAGES_H_

#include <boost\asio.hpp>
#include <iostream>
#include <vector>

namespace dcl {
	namespace net {
		typedef unsigned char byte;

		/* Class that represents and stores the standard protocol in the system:
		 * MessageCode (byte)
		 * Arguments (Vector<string>)
		 */
		template<typename T>
		class ReceivedMessage {
		private:
			byte m_Code;
			T m_Value;
		public:
			ReceivedMessage(byte code, T value = T());
			byte getMessageCode();
			T& getValue();
		};

		template<typename T>
		ReceivedMessage<T>::ReceivedMessage(byte code, T value) : m_Code(code), m_Value(value) {}

		template<typename T>
		byte ReceivedMessage<T>::getMessageCode() {
			return this->m_Code;
		}

		template<typename T>
		T& ReceivedMessage<T>::getValue() {
			return this->m_Value;
		}
	}
}


#endif