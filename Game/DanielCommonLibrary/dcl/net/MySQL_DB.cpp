#include "MySQL_DB.h"

#include "dcl\tools\Utils.h"

using namespace dcl::net;
using namespace std;
using namespace sql;

MySQL_DB::MySQL_DB() {
	this->_connection = nullptr;
}

MySQL_DB::~MySQL_DB() {
	disconnect();
}

MySQL_DB::MySQL_DB(const MySQL_DB& other) :
	_connection(other._connection) {}

MySQL_DB& MySQL_DB::operator=(const MySQL_DB& other) {
	_connection = other._connection;
	return *this;
}

void MySQL_DB::connect(const string& url, const string& username, const string& password, const string& database) {
	// Gets the driver and creates a new connection using the given url and credentials
	Driver* driver = get_driver_instance();
	_connection = driver->connect(url + "?allowMultiQueries=true&autoReconnect=true", username, password);
	_connection->setSchema(database);
}

void MySQL_DB::disconnect() {
	if (_connection) {
		if (!_connection->isClosed()) {
			_connection->close();
		}
		SAFE_DELETE(_connection);
	}
}

bool MySQL_DB::reconnect() {
	return _connection->reconnect();
}

bool MySQL_DB::isConnected() {
	return (_connection && !_connection->isClosed());
}

Connection* MySQL_DB::getConnection() {
	return _connection;
}
