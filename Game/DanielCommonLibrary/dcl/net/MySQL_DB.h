#ifndef _DCL_DATABASE_CONNECTOR_H_
#define _DCL_DATABASE_CONNECTOR_H_

#pragma warning(push)
#pragma warning(disable:4251)
#include <mysql_connection.h>
#include <cppconn\driver.h>
#include <cppconn\exception.h>
#include <cppconn\resultset.h>
#include <cppconn\prepared_statement.h>
#pragma warning(pop)

namespace dcl {
	namespace net {
		// Defines the underlying layer of the MySQL connection
		class MySQL_DB {
		private:
			sql::Connection* _connection;
		protected:
			sql::Connection* getConnection();

		public:
			MySQL_DB();
			
			virtual ~MySQL_DB();
			MySQL_DB(const MySQL_DB&);
			MySQL_DB& operator=(const MySQL_DB&);
			
			// Connects to the server on @url as @username with password @password
			void connect(const std::string& url, const std::string& username, const std::string& password, const std::string& database);
			void disconnect();
			bool reconnect();
			bool isConnected();

			
		};
	}
}

#endif