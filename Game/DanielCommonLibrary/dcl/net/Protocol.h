#ifndef _DCL_PROTOCOL_H_
#define _DCL_PROTOCOL_H_

#include "dcl\structures\Buffer.h"
#include <iostream>
#include <list>

namespace dcl {
	namespace net {
		typedef unsigned char byte;
		typedef unsigned short ushort;
		typedef unsigned int uint;

		// Contains useful static functions for extracting and inserting data into a buffer
		class Protocol {
		public:
			/*// Reads info of type @T from buffer @buffer. @T must be a non-pointer and not contain any pointer members
			template<typename T, size_t size>
			static typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value, T&>::type
			ReadFromBuffer(dcl::structures::Buffer<byte, size>& buffer);

			// Write the info in type @Value of type @T into @buffer. @T must be a non-pointer and not contain any pointer members
			template<typename T, size_t size>
			static typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value, T&>::type
				WriteToBuffer(const T& value, dcl::structures::Buffer<byte, size>& buffer);*/

			// Converts @value of type @T to a string of bytes.
			template<typename T>
			static typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value, std::string>::type
				ToString(const typename std::common_type<T>::type& value);

			// Reads a string of length @length
			template<size_t size>
			static std::string ReadStringFromBuffer(dcl::structures::Buffer<byte, size>& buffer, size_t bytesNum);

			// Writes a string of length @length
			template<size_t size>
			static void WriteStringToBuffer(dcl::structures::Buffer<byte, size>& buffer, const std::string& str);
		};

		/*template<typename T, size_t size>
		typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value, T&>::type
			Protocol::ReadFromBuffer(dcl::structures::Buffer<byte, size>& buffer) {

			union {
				T value;
				byte bytes[sizeof(T)];
			} u;

			buffer.read(u.bytes, sizeof(T));
			return u.value;
		}

		template<typename T, size_t size>
		typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value, T&>::type
			Protocol::WriteToBuffer(const T& value, dcl::structures::Buffer<byte, size>& buffer) {

			union {
				T value;
				byte bytes[sizeof(T)];
			} u;

			buffer.write(u.bytes, sizeof(T));
			return u.value;
		}*/

		template<typename T>
		typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value, std::string>::type
			Protocol::ToString(const typename std::common_type<T>::type& value) {

			union {
				T value;
				byte bytes[sizeof(T)];
			} u;

			u.value = value;
			std::string s((char*) u.bytes, sizeof(T));
			return s;
		}

		template<size_t size>
		std::string Protocol::ReadStringFromBuffer(dcl::structures::Buffer<byte, size>& buffer, size_t length) {
			return string((char*)buffer.Read(length), length);
		}
		
		template<size_t size>
		static void Protocol::WriteStringToBuffer(dcl::structures::Buffer<byte, size>& buffer, const std::string& str) {
			buffer.Write((byte*)str.data(), str.size());
		}
	}
}

#endif