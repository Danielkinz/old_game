#include "Session.h"

#include <boost\bind.hpp>

using namespace std;
using namespace dcl::net;
using namespace dcl::structures;
using namespace boost::asio;
using namespace boost::asio::ip;

unsigned long long Session::nextSessionID = 0;

/* --------------------------- Object --------------------------- */

Session::Session(io_service& service) 
	: _sessionID(nextSessionID++), 
	_socket(make_shared<tcp::socket>(service)) {}

Session::Session(const Session& other)
	: _sessionID(other._sessionID), 
	_socket(other._socket) {}

Session::~Session() {}

Session& Session::operator=(const Session& other) {
	this->_sessionID = other._sessionID;
	this->_socket = other._socket;
	return *this;
}

void Session::start_listening() {
	_buffer.clear();
	_socket->async_read_some(boost::asio::buffer(_buffer.data(), _buffer.capacity()), 
		boost::bind(&Session::listen, this, boost::asio::placeholders::error, 
			boost::asio::placeholders::bytes_transferred));
}

void Session::listen(const boost::system::error_code& error, size_t bytesRead) {
	_buffer.SetWritePointer(bytesRead);
	if (handleRead(_buffer, error)) // Stop reading if the handler says to stop!
		start_listening();
}

void Session::startListening() {
	if (listening) return;
	listening = true;
	start_listening();
}

bool Session::handleRead(Buffer<byte>&, const boost::system::error_code&) { 
	// We dont want to continue reading if we aren't handeling
	listening = false;
	return false;
}

void Session::disconnect() {
	if (_socket->is_open()) _socket->close();
}

bool Session::isConnected() const {
	return _socket->is_open();
}

/* --------------------------- Operators --------------------------- */

tcp::socket& Session::getSocket() {
	return *this->_socket;
}

const tcp::socket& Session::getSocket() const {
	return *this->_socket;
}

bool Session::operator<(const Session& other) const {
	return this->_sessionID < other._sessionID;
}

bool Session::operator>(const Session& other) const {
	return this->_sessionID > other._sessionID;
}

bool Session::operator<=(const Session& other) const {
	return this->_sessionID <= other._sessionID;
}

bool Session::operator>=(const Session& other) const {
	return this->_sessionID >= other._sessionID;
}

bool Session::operator==(const Session& other) const {
	return this->_sessionID == other._sessionID;
}

/* --------------------------- IO Operations --------------------------- */

dcl::structures::Buffer<byte>& Session::read() {
	_buffer.clear();
	_buffer.SetWritePointer(_socket->read_some(boost::asio::buffer(_buffer.data(), _buffer.capacity())));
	return _buffer;
}
