#ifndef _DCL_SESSION_H_
#define _DCL_SESSION_H_

#include "dcl\structures\Buffer.h"
#include <boost\asio.hpp>
#include <string>
#include <mutex>

namespace dcl {
	namespace net {
		typedef unsigned char byte;

		// The underlying class for all session types
		class Session {
		private: // Object
			static unsigned long long nextSessionID;

			// A unique ID for each session
			unsigned long long _sessionID = 0;
			std::shared_ptr<boost::asio::ip::tcp::socket> _socket;
			dcl::structures::Buffer<byte> _buffer;
			bool listening = false;

			// Handles new received messages and calls
			void start_listening();

			std::mutex _transferMutex;

			// Function that receives the new messages and passes it to the handler
			void listen(const boost::system::error_code&, size_t);
		protected: // Object
			// Activates the listening sequence (and the event handler)
			void startListening();
			virtual bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

		public: // Object
			Session(boost::asio::io_service&);

			virtual ~Session();
			Session(const Session& other);
			Session& operator=(const Session& other);

			virtual void disconnect();
			bool isConnected() const;

		public: // Operators
			bool operator<(const Session& other) const;
			bool operator>(const Session& other) const;
			bool operator<=(const Session& other) const;
			bool operator>=(const Session& other) const;
			bool operator==(const Session& other) const;
			boost::asio::ip::tcp::socket& getSocket();
			const boost::asio::ip::tcp::socket& getSocket() const;

		public: // IO operations
			template<size_t size>
			dcl::structures::Buffer<byte, size>& read(dcl::structures::Buffer<byte, size>&);

			dcl::structures::Buffer<byte>& read();

			template<size_t size>
			void sendData(dcl::structures::Buffer<byte, size>&);

			template<size_t size>
			void sendData(dcl::structures::Buffer<byte, size>&, size_t length);
		};

		template<size_t size>
		dcl::structures::Buffer<byte, size>& Session::read(dcl::structures::Buffer<byte, size>& buffer) {
			byte arr[size] = { 0 };
			this->_socket->read_some(boost::asio::buffer(arr, size));
			buffer.clear();
			buffer.Write(arr, size);
			return buffer;
		}

		template<size_t size>
		void Session::sendData(dcl::structures::Buffer<byte, size>& buffer) {
			//std::lock_guard<std::mutex> guard(_transferMutex);
			this->_socket->write_some(boost::asio::buffer(buffer.data(), buffer.length()));
		}

		template<size_t size>
		void Session::sendData(dcl::structures::Buffer<byte, size>& buffer, size_t length) {
			//std::lock_guard<std::mutex> guard(_transferMutex);
			this->_socket->write_some(boost::asio::buffer(buffer.data(), length));
		}
	}
}

#endif