#include "TCPClient.h"

#include <boost\bind.hpp>

using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;
using namespace dcl::structures;
using namespace dcl::net;

/* --------------------------- Object --------------------------- */

TCPClient::TCPClient(io_service& service, const string& host, int port)
	: _socket(new tcp::socket(service)), disconnectionHandler(nullptr) {

	tcp::resolver resolver(service);
	tcp::resolver::query query(tcp::v4(), host, to_string(port));
	tcp::resolver::iterator iterator = resolver.resolve(query);

	connect(*_socket, iterator);
}

TCPClient::TCPClient(io_service& service, unsigned int host, int port) : _socket(new tcp::socket(service)), disconnectionHandler(nullptr) {
	vector<tcp::endpoint> points;
	points.push_back(tcp::endpoint(address(address_v4(host)), port));
	connect(*_socket, points);
}

TCPClient::TCPClient(const TCPClient& other)
	: _socket(other._socket), disconnectionHandler(other.disconnectionHandler) {}

TCPClient::~TCPClient() {}

TCPClient& TCPClient::operator=(const TCPClient& other) {
	this->_socket = other._socket;
	disconnectionHandler = other.disconnectionHandler;
	return *this;
}

void TCPClient::start_listening() {
	if (_socket == nullptr) return;
	_buffer.clear();
	_socket->async_read_some(boost::asio::buffer(_buffer.data(), _buffer.capacity()),
		boost::bind(&TCPClient::listen, this, boost::asio::placeholders::error,
			boost::asio::placeholders::bytes_transferred));
}

void TCPClient::listen(const boost::system::error_code& error, size_t bytesRead) {
	_buffer.SetWritePointer(bytesRead);
	if (handleRead(_buffer, error)) // Stop reading if the handler says to stop!
		start_listening();
}

void TCPClient::startListening() {
	if (listening) return;
	listening = true;
	start_listening();
}

bool TCPClient::handleRead(Buffer<byte>&, const boost::system::error_code&) {
	// We dont want to continue reading if we aren't handeling
	listening = false;
	return false;
}

void TCPClient::disconnect() {
	if (_socket) {
		//_socket->cancel();
		_socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both);
		_socket->close();
	}
	
	if (disconnectionHandler != nullptr) disconnectionHandler();
}

tcp::socket& TCPClient::getSocket() {
	return *_socket;
}

const tcp::socket& TCPClient::getSocket() const {
	return *_socket;
}

void TCPClient::addDisconnectionHandler(DisconnectionHandler handler) {
	this->disconnectionHandler = handler;
}

/* --------------------------- IO Operations --------------------------- */

Buffer<byte>& TCPClient::read() {
	_buffer.clear();
	_buffer.SetWritePointer(_socket->read_some(boost::asio::buffer(_buffer.data(), _buffer.capacity())));
	return _buffer;
}
