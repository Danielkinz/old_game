#ifndef _DCP_TCP_CLIENT_
#define _DCP_TCP_CLIENT_

#include "dcl\structures\Buffer.h"
#include <boost\asio.hpp>
#include <string>

namespace dcl {
	namespace net {
		typedef unsigned char byte;
		typedef void(*DisconnectionHandler)();

		// A class that is used as the underlying layer for TCP clients
		class TCPClient {
		private: // Object
			std::shared_ptr<boost::asio::ip::tcp::socket> _socket;
			dcl::structures::Buffer<byte> _buffer;
			bool listening = false;

			// Message Listener
			void start_listening();

			// Handles the new message and passes it to the sub-class handler
			void listen(const boost::system::error_code&, size_t);

			// Called when the connection is lost
			DisconnectionHandler disconnectionHandler;

		protected: // Object
			// Activates the listener and the event handler
			void startListening();

			// The event handler from the sub-class
			virtual bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

		public: // Object
			TCPClient(boost::asio::io_service&, const std::string& host, int port);
			TCPClient(boost::asio::io_service&, unsigned int host, int port);

			virtual ~TCPClient();
			TCPClient(const TCPClient& other);
			TCPClient& operator=(const TCPClient& other);

			void disconnect();
			void addDisconnectionHandler(DisconnectionHandler h);

		public: // Operators
			boost::asio::ip::tcp::socket& getSocket();
			const boost::asio::ip::tcp::socket& getSocket() const;

		public: // IO operations
			template<size_t size>
			dcl::structures::Buffer<byte, size>& read(dcl::structures::Buffer<byte, size>&);

			dcl::structures::Buffer<byte>& read();

			template<size_t size>
			void sendData(dcl::structures::Buffer<byte, size>&);

			template<size_t size>
			void sendData(dcl::structures::Buffer<byte, size>&, size_t length);
		};

		template<size_t size>
		dcl::structures::Buffer<byte, size>& TCPClient::read(dcl::structures::Buffer<byte, size>& buffer) {
			byte arr[size] = { 0 };
			this->_socket->read_some(boost::asio::buffer(arr, size));
			buffer.clear();
			buffer.Write(arr, size);
			return buffer;
		}

		template<size_t size>
		void TCPClient::sendData(dcl::structures::Buffer<byte, size>& buffer) {
			this->_socket->write_some(boost::asio::buffer(buffer.data(), buffer.length()));
		}

		template<size_t size>
		void TCPClient::sendData(dcl::structures::Buffer<byte, size>& buffer, size_t length) {
			this->_socketPtr->write_some(boost::asio::buffer(buffer.data(), length));
		}
	}
}

#endif