#include "TCPServer.h"

using namespace dcl::net;
using namespace dcl::tools;
using namespace std;
using namespace boost::asio;
using namespace boost::asio::ip;

TCPServer::TCPServer(io_service& context, ushort port) 
	: _ioService(context), _acceptor(context, tcp::endpoint(tcp::v4(), port)), _isRunning(true) {
	
	start_accept();
}

TCPServer::~TCPServer() {
	_isRunning = false;
	shutDown();
}

void TCPServer::start_accept() {
	Session* new_session = new Session(_ioService);
	_acceptor.async_accept(new_session->getSocket(), 
		boost::bind(&TCPServer::accept_handler, this, new_session, 
			boost::asio::placeholders::error));
}

void TCPServer::acceptHandler(Session*, const boost::system::error_code&) {}

void TCPServer::accept_handler(Session* new_session, const boost::system::error_code& err) {
	if (!_acceptor.is_open()) return;
	acceptHandler(new_session, err);
	start_accept();
}

void TCPServer::shutDown() {
	try { this->_acceptor.cancel();
	} catch (...) {}
	if (this->_acceptor.is_open()) this->_acceptor.close();
}

