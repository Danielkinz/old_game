#ifndef _DCL_TCP_SERVER_
#define _DCL_TCP_SERVER_

#include "dcl\structures\GuardedQueue.h"
#include "dcl\net\Protocol.h"
#include "dcl\net\Messages.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\Utils.h"
#include "dcl\tools\Exception.h"
#include "dcl\net\Session.h"
#include <mutex>
#include <sstream>
#include <boost\bind.hpp>

namespace dcl {
	namespace net{
		typedef unsigned short ushort;

		// A class that defines the underlying layer of the servers
		class TCPServer {
		private:
			bool _isRunning;

			boost::asio::io_service& _ioService;
			boost::asio::ip::tcp::acceptor _acceptor;

			// New connection listener
			void start_accept();

			// Accepts new sessions and passes them to the event handler
			void accept_handler(Session*, const boost::system::error_code&);

		protected:
			TCPServer(boost::asio::io_service&, ushort port);

			// Handlers

			// New connection event handler
			virtual void acceptHandler(Session*, const boost::system::error_code&);
			void shutDown();

		public:
			virtual ~TCPServer();
		};
	}
}

#endif
