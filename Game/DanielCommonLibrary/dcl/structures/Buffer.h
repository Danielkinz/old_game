#ifndef _DCL_BUFFER_H_
#define _DCL_BUFFER_H_

#include <iterator>
#include <list>
#include <boost\utility.hpp>
#include <boost\move\move.hpp>
#include "dcl\tools\Exception.h"
#include "dcl\tools\Utils.h"

#define ITERATOR_TEMPLATE typename boost::enable_if<typename boost::is_same<typename std::iterator_traits<Iterator>::value_type, Iterator>::value>::type

namespace dcl {
	namespace structures {

		typedef unsigned char byte;

		// Class used to read/write to an array of objects
		template<typename type_name, size_t size = 512>
		class Buffer final {
		public:

		private: // Types
			typedef Buffer<type_name, size> buffer_type;
			BOOST_MOVABLE_BUT_NOT_COPYABLE(Buffer)

			class BufferPointer {
			public:
				size_t location = 0;
				size_t operator++(int);
				BufferPointer& operator=(size_t);
				BufferPointer& operator=(BufferPointer& other);
			};

		private: // Variables
			type_name _buffer[size];
			BufferPointer _readPointer;
			BufferPointer _writePointer;
			std::list<type_name*> _allocatedData;

		public: // Object
			Buffer();
			Buffer(type_name* ptr, size_t length = size);

			template<class Iterator>
			Buffer(ITERATOR_TEMPLATE begin, ITERATOR_TEMPLATE end);

			~Buffer();
			Buffer(BOOST_RV_REF(buffer_type) other);
			Buffer& operator=(BOOST_RV_REF(buffer_type) other);

			void ResetReadPointer();
			void ResetWritePointer();
			type_name* Read(size_t length);
			type_name& Read();
			type_name* Read(type_name* target, size_t length);
			void Write(type_name);
			void Write(type_name*, size_t length);

			// Reads info of type @T. @T must be a non-pointer and not contain any pointer members
			template<typename T>
			typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value && 
				std::is_same<type_name, byte>::value, T&>::type
				Read();

			template<typename T>
			typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value &&
				std::is_same<type_name, byte>::value, Buffer<type_name, size>&>::type
				Read(T* target, size_t length = 1);

			// Write the info in type @Value of type @T. @T must be a non-pointer and not contain any pointer members
			template<typename T>
			typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value && 
				std::is_same<type_name, byte>::value, Buffer<type_name, size>&>::type
				Write(const typename std::common_type<T>::type& value);


			void SetReadPointer(size_t location);
			void SetWritePointer(size_t location);
			size_t GetReadPointer();
			size_t GetWritePointer();

			template<class Iterator>
			void Write(ITERATOR_TEMPLATE begin, ITERATOR_TEMPLATE end);

			size_t capacity();
			type_name* data();
			size_t length();
			size_t writtenLength();
			void clear();
		};

		template<typename type_name, size_t size>
		size_t Buffer<type_name, size>::BufferPointer::operator++(int) {
			if (this->location > size) throw dcl::tools::BufferOverflowException();
			return this->location++;
		}

		template<typename type_name, size_t size>
		typename Buffer<type_name, size>::BufferPointer& Buffer<type_name, size>::BufferPointer::operator=(size_t loc) {
			if (loc > size) throw dcl::tools::BufferOverflowException();
			this->location = loc;
			return *this;
		}

		template<typename type_name, size_t size>
		typename Buffer<type_name, size>::BufferPointer& Buffer<type_name, size>::BufferPointer::operator=(BufferPointer& other) {
			this->location = other.location;
			return *this;
		}

		template<typename type_name, size_t size>
		Buffer<type_name, size>::Buffer() {}

		template<typename type_name, size_t size>
		Buffer<type_name, size>::Buffer(type_name* ptr, size_t length) {
			if (length > size) throw dcl::tools::BufferOverflowException();
			Write(ptr, length);
		}

		template<typename type_name, size_t size>
		template<class Iterator>
		Buffer<type_name, size>::Buffer(ITERATOR_TEMPLATE begin, ITERATOR_TEMPLATE end) {
			Write(begin, end);
		}

		template<typename type_name, size_t size>
		Buffer<type_name, size>::~Buffer() {
			for (auto it = _allocatedData.begin(); it != _allocatedData.end(); it++)
				delete[] * (it);
		}

		template<typename type_name, size_t size>
		Buffer<type_name, size>::Buffer(BOOST_RV_REF(buffer_type) other) {
			operator=(other);
		}

		template<typename type_name, size_t size>
		Buffer<type_name, size>& Buffer<type_name, size>::operator=(BOOST_RV_REF(buffer_type) other) {
			int i = 0;
			for (; i < size; i++) _buffer[i] = other._buffer[i];
			_readPointer = other.readPointer;
			_writePointer = other.writePointer;
			_allocatedData = other._allocatedData;
			other._allocatedData.clear();
			return *this;
		}

		template<typename type_name, size_t size>
		void Buffer<type_name, size>::ResetReadPointer() {
			this->_readPointer = 0;
		}

		template<typename type_name, size_t size>
		void Buffer<type_name, size>::ResetWritePointer() {
			this->_writePointer = 0;
		}

		template<typename type_name, size_t size>
		type_name* Buffer<type_name, size>::Read(size_t length) {
			type_name* t = new type_name[length];
			int i = 0;

			for (; i < length; i++) {
				if (_readPointer.location >= _writePointer.location) throw dcl::tools::IOException();
				t[i] = _buffer[_readPointer++];
			}

			_allocatedData.push_back(t);
			return t;
		}

		template<typename type_name, size_t size>
		type_name& Buffer<type_name, size>::Read() {
			type_name* t;

			if (_readPointer.location >= _writePointer.location) throw dcl::tools::IOException();

			t = new type_name[1];
			t[0] = _buffer[_readPointer++];
			_allocatedData.push_back(t);
			return *t;
		}

		template<typename type_name, size_t size>
		type_name* Buffer<type_name, size>::Read(type_name* target, size_t length) {
			int i = 0;

			for (; i < length; i++) {
				if (_readPointer.location >= _writePointer.location) throw dcl::tools::IOException();
				target[i] = _buffer[_readPointer++];
			}

			return target;
		}

		template<typename type_name, size_t size>
		void Buffer<type_name, size>::Write(type_name t) {
			_buffer[_writePointer++] = t;
		}

		template<typename type_name, size_t size>
		void Buffer<type_name, size>::Write(type_name* data, size_t length) {
			int i = 0;
			for (; i < length; i++)
				_buffer[_writePointer++] = data[i];
		}

		template<typename type_name, size_t size>
		template<typename T>
		typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value 
			&& std::is_same<type_name, byte>::value, T&>::type
			Buffer<type_name, size>::Read() {

			union {
				T value;
				byte bytes[sizeof(T)];
			} u;

			Read(u.bytes, sizeof(T));
			return u.value;
		}

		template<typename type_name, size_t size>
		template<typename T>
		typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value &&
			std::is_same<type_name, byte>::value, Buffer<type_name, size>&>::type
			Buffer<type_name, size>::Read(T* target, size_t length) {

			T* it = target;
			for (uint i = 0; i < length; i++) {
				*it++ = Read<T>();
			}

			return *this;
		}
		
		template<typename type_name, size_t size>
		template<typename T>
		typename std::enable_if<!std::is_pointer<T>::value && std::is_trivially_copyable<T>::value && 
			std::is_same<type_name, byte>::value, Buffer<type_name, size>&>::type
			Buffer<type_name, size>::Write(const typename std::common_type<T>::type& value) {

			union {
				T value;
				byte bytes[sizeof(T)];
			} u;

			u.value = value;
			Write(u.bytes, sizeof(T));
			return *this;
		}

		template<typename type_name, size_t size>
		void Buffer<type_name, size>::SetReadPointer(size_t location) { this->_readPointer = location; }

		template<typename type_name, size_t size>
		void Buffer<type_name, size>::SetWritePointer(size_t location) { this->_writePointer = location; }

		template<typename type_name, size_t size>
		size_t Buffer<type_name, size>::GetReadPointer() { return this->_readPointer.location; }

		template<typename type_name, size_t size>
		size_t Buffer<type_name, size>::GetWritePointer() { return this->_writePointer.location; }

		template<typename type_name, size_t size>
		template<class Iterator>
		void Buffer<type_name, size>::Write(ITERATOR_TEMPLATE begin, ITERATOR_TEMPLATE end) {
			for (auto it = begin; it != end; it++) {
				_buffer[_writePointer++] = *it;
			}
		}

		template<typename type_name, size_t size>
		size_t Buffer<type_name, size>::capacity() {
			return size;
		}

		template<typename type_name, size_t size>
		type_name * Buffer<type_name, size>::data() {
			return _buffer;
		}

		template<typename type_name, size_t size>
		size_t Buffer<type_name, size>::length() {
			return _writePointer.location - _readPointer.location;
		}

		template<typename type_name, size_t size>
		inline size_t Buffer<type_name, size>::writtenLength() {
			return _writePointer.location;
		}

		template<typename type_name, size_t size>
		void Buffer<type_name, size>::clear() {
			this->_readPointer = 0;
			this->_writePointer = 0;
			for (auto it = _allocatedData.begin(); it != _allocatedData.end(); it++)
				SAFE_DELETE_ARRAY(*it);
		}
	}
}

#undef ITERATOR_TEMPLATE
#undef CHECK_WRITE
#undef CHECK_READ

#endif