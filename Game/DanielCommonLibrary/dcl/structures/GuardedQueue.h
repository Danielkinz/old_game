#ifndef _DCL_GUARDED_QUEUE_
#define _DCL_GUARDED_QUEUE_

#include <queue>
#include <deque>
#include <mutex>

namespace dcl {
	namespace structures {

		// An overload of std::queue with synchronized functions
		template<typename value_type, typename container_type = std::deque<value_type>>
		class GuardedQueue : public std::queue<value_type, container_type> {
		
		private:
			mutable std::recursive_mutex _lock;

		public:
			explicit GuardedQueue(const container_type& ctnr = container_type());

			bool empty() const;
			auto size() const;
			auto& front();
			const auto& front() const;
			auto& back();
			const auto& back() const;
			void push(const value_type& val);
			void pop();
			void pop(value_type& ret);
		};

// Locks the mutex for the duration of the function
#define GUARDED std::lock_guard<std::recursive_mutex> guard(_lock);

		template<typename value_type, typename container_type>
		inline GuardedQueue<value_type, container_type>::GuardedQueue(const container_type & ctnr) : 
			std::queue<value_type, container_type>(ctnr) {}

		template<typename value_type, typename container_type>
		inline bool GuardedQueue<value_type, container_type>::empty() const {
			GUARDED return std::queue<value_type, container_type>::empty();
		}

		template<typename value_type, typename container_type>
		inline auto GuardedQueue<value_type, container_type>::size() const {
			GUARDED return std::queue<value_type, container_type>::size();
		}

		template<typename value_type, typename container_type>
		inline auto & GuardedQueue<value_type, container_type>::front() {
			GUARDED return std::queue<value_type, container_type>::front();
		}

		template<typename value_type, typename container_type>
		inline const auto & GuardedQueue<value_type, container_type>::front() const {
			GUARDED return std::queue<value_type, container_type>::front();
		}

		template<typename value_type, typename container_type>
		inline auto & GuardedQueue<value_type, container_type>::back() {
			GUARDED return std::queue<value_type, container_type>::back();
		}

		template<typename value_type, typename container_type>
		inline const auto & GuardedQueue<value_type, container_type>::back() const {
			GUARDED return std::queue<value_type, container_type>::back();
		}

		template<typename value_type, typename container_type>
		inline void GuardedQueue<value_type, container_type>::push(const value_type & val) {
			GUARDED std::queue<value_type, container_type>::push(val);
		}

		template<typename value_type, typename container_type>
		inline void GuardedQueue<value_type, container_type>::pop() {
			GUARDED std::queue<T>::pop();
		}

		template<typename value_type, typename container_type>
		inline void GuardedQueue<value_type, container_type>::pop(value_type & ret) {
			GUARDED ret = std::queue<value_type, container_type>::front();
			std::queue<value_type, container_type>::pop();
		}

#undef GUARDED

	}
}

#endif