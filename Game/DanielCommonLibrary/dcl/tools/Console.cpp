#include "Console.h"
#include <string>

using namespace std;

mutex dcl::tools::Console::_consoleMtx;

// An overload of the stream operator for printing string vectors
std::ostream& dcl::tools::operator<<(std::ostream& os, const std::vector<string>& vec) {
	for (unsigned int i = 0; i < vec.size(); i++) {
		os << vec[i];
		if (i != vec.size() - 1) os << ", ";
	}
	return os;
}