#ifndef _DCL_CONSOLE_H_
#define _DCL_CONSOLE_H_

#include <iostream>
#include <mutex>
#include <conio.h>
#include <vector>
#include <string>

namespace dcl {
	namespace tools {
		/* Defines synchronised write functions. Can accept any argument for printing
		 * The write functions behave similarly to the python3 print(Args...) function
		 */
		class Console {
		private:
			static std::mutex _consoleMtx;

		public:
			template <typename T>
			static void write(const T& t);

			template <typename T, typename... Args>
			static void write(const T& t, const Args&... args);

			template <typename T>
			static void writeln(const T& t);

			template <typename T, typename... Args>
			static void writeln(const T& t, const Args&... args);
		};

		std::ostream& operator<<(std::ostream& os, const std::vector<std::string>& vec);

		template <typename T>
		void Console::write(const T& t) {
			Console::_consoleMtx.lock();
			cout << t;
			Console::_consoleMtx.unlock();
		}

		template <typename T, typename... Args>
		void Console::write(const T& t, const Args&... args) {
			Console::_consoleMtx.lock();
			cout << t;
			dcl::tools::unpackToStream(cout, args...);
			Console::_consoleMtx.unlock();
		}

		template <typename T>
		void Console::writeln(const T& t) {
			Console::_consoleMtx.lock();
			cout << t << endl;
			Console::_consoleMtx.unlock();
		}

		template <typename T, typename... Args>
		void Console::writeln(const T& t, const Args&... args) {
			Console::_consoleMtx.lock();
			cout << t;
			dcl::tools::unpackToStream(cout, args...);
			cout << endl;
			Console::_consoleMtx.unlock();
		}
	};
};

#endif