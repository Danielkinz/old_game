#include "Exception.h"

using namespace dcl::tools;
using namespace std;

#undef Exception
#undef IOException
#undef BufferOverflowException

Exception::Exception(int lineNumber, const char* fileName) noexcept 
	: _lineNumber(lineNumber), _fileName(fileName), _error("Generic Error") {}

Exception::~Exception() {}

Exception& Exception::operator=(const Exception& other) noexcept  {
	this->_error = other._error;
	return *this;
}

const string& Exception::what() const noexcept {
	return this->_error;
}

int Exception::getLineNumber() const noexcept {
	return this->_lineNumber;
}

const string& Exception::getFileName() const noexcept {
	return this->_fileName;
}

Exception::operator const std::string&() const noexcept {
	return this->_error;
}

Exception::operator std::string() const noexcept {
	return this->_error;
}

/* --------------------- Input/Output Operators --------------------- */

void Exception::printOn(std::ostream& out) const {
	out << _error;
}

std::ostream& dcl::tools::operator<<(std::ostream& out, const Exception& e) {
	e.printOn(out);
	return out;
}

#define Exception(...) Exception(__LINE__, __FILE__, __VA_ARGS__)
