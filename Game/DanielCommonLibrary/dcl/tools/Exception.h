#ifndef _DCL_EXCEPTION_H_
#define _DCL_EXCEPTION_H_

#include <string>
#include <sstream>
#include <ostream>
#include "Utils.h"

namespace dcl {
	namespace tools {

		/* An Exception that also includes the file name and line number in which it was created, and allows
		 * any kind of parameters and functions to be specified upon creation (converted to string).
		 * When inherited, use #define <exception_name>(...) <exception_name>(__LINE__, __FILE__, __VA_ARGS__)
		 * To automatically include the file name and line number when constructing the object.
		 * Make sure to undefine and re-define in the .cpp file
		 */
		class Exception {
		public: // I/O
			friend std::ostream& operator<<(std::ostream&, const Exception&);

		private: // Object
			std::string _error;
			std::string _fileName;
			int _lineNumber;

		public: // Object
			virtual ~Exception();
			Exception& operator=(const Exception& other) noexcept;

			const std::string& what() const noexcept;
			int getLineNumber() const noexcept;
			const std::string& getFileName() const noexcept;
			operator const std::string&() const noexcept;
			operator std::string() const noexcept;

			Exception(int lineNumber, const char* fileName) noexcept;

			template <typename T>
			Exception(int lineNumber, const char* fileName, const T& t) noexcept;

			template <typename T, typename... Args>
			Exception(int lineNumber, const char* fileName, const T& t, const Args&... args) noexcept;

			template<> Exception(int lineNumber, const char* fileName, const std::string& str) noexcept;
		
		protected: // I/O
			virtual void printOn(std::ostream& out) const;

		};

		std::ostream& operator<<(std::ostream&, const Exception&);

		/* --------------------- Template Definitions --------------------- */

		template <typename T>
		Exception::Exception(int lineNumber, const char* fileName, const T& t) noexcept : _lineNumber(lineNumber), _fileName(fileName) {
			std::stringstream ss;
			ss << t;
			this->_error = ss.str();
		}

		template <typename T, typename... Args>
		Exception::Exception(int lineNumber, const char* fileName, const T& t, const Args&... args) noexcept : _lineNumber(lineNumber), _fileName(fileName) {
			std::stringstream ss;
			ss << t;
			dcl::tools::unpackToString(ss, args...);
			this->_error = ss.str();
		}

		template<> Exception::Exception(int lineNumber, const char* fileName, const std::string& str) noexcept {
			this->_error = str;
		}

		/* --------------------- Exception Sub-Types --------------------- */

// defines a standard exception format
#define CREATE_EXCEPTION(NAME, ERROR_STR) \
class NAME : public dcl::tools::Exception {\
public: NAME (int lineNumber, const char* fileName) : dcl::tools::Exception(lineNumber, fileName, ERROR_STR) {}};

// Defines a standard final exception format
#define CREATE_FINAL_EXCEPTION(NAME, ERROR_STR) \
class NAME final : public dcl::tools::Exception {\
public: NAME (int lineNumber, const char* fileName) : dcl::tools::Exception(lineNumber, fileName, ERROR_STR) {}};

// Creates the required sub-types of exceptions
		CREATE_FINAL_EXCEPTION(BufferOverflowException, "Buffer Overflow");
		CREATE_FINAL_EXCEPTION(IOException, "I/O Exception");
		CREATE_FINAL_EXCEPTION(ValidationException, "Validation failed");
		CREATE_FINAL_EXCEPTION(DisconnectException, "Disconnected");

#undef CREATE_EXCEPTION
#undef CREATE_FINAL_EXCEPTION

// Defines the exception macros
#define Exception(...) Exception(__LINE__, __FILE__, __VA_ARGS__)
#define IOException(...) IOException(__LINE__, __FILE__, __VA_ARGS__)
#define BufferOverflowException(...) BufferOverflowException(__LINE__, __FILE__, __VA_ARGS__)
#define ValidationException(...) ValidationException(__LINE__, __FILE__, __VA_ARGS__)
#define DisconnectException(...) DisconnectException(__LINE__, __FILE__, __VA_ARGS__)

	}
}

#endif