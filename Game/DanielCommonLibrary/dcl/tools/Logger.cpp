#include "Logger.h"
#include <iostream>
#include "Console.h"
#include "Utils.h"

using namespace std;
using namespace dcl::tools;

/* --------------------- Log Level --------------------- */

LogLevel::LogLevel(const string& name) : _name(name) {}
LogLevel::operator const char *() const { return _name.c_str();}
LogLevel::operator const std::string &() const { return _name; }
const string& LogLevel::name() const { return _name; }
bool LogLevel::operator==(const LogLevel& other) const { return this->_name == other._name; }

LogLevel const LogLevel::info("INFO");
LogLevel const LogLevel::warning("WARNING");
LogLevel const LogLevel::error("ERROR");
LogLevel const LogLevel::fatal("FATAL");
LogLevel const LogLevel::debug("DEBUG");

LogLevel::LogLevel(const LogLevel& other) : _name(other._name) {}

LogLevel& LogLevel::operator=(const LogLevel& other) {
	this->_name = other._name;
	return *this;
}

/* --------------------- Logger Callbacks --------------------- */

void DefaultLoggerHandler::callback(const string& name, const string& message, const LogLevel& level) const {
	Console::writeln("[", level, "][", name, "] ", message);
}

void DefaultLoggerHandler::callback(const string& name, const dcl::tools::Exception& exception, const LogLevel& level) const {
	Console::writeln("[", level, "][", name, "] ", "Exception: \"", exception ,"\" in ", exception.getFileName(), ":", exception.getLineNumber());
}

void DefaultLoggerHandler::callback(const string& name, const string& message, const dcl::tools::Exception& exception, const LogLevel& level) const {
	Console::writeln("[", level, "][", name, "] ", message);
}

/* --------------------- Logger --------------------- */

Logger::Logger(const string& name, const LogLevel& level) : _name(name), _level(&level), _handlers{ make_shared<DefaultLoggerHandler>() } {}

Logger::Logger(const Logger& other) : _name(other._name), _level(other._level), _handlers(other._handlers) {}

Logger& Logger::operator=(const Logger& other) {
	this->_name = other._name;
	this->_level = other._level;
	this->_handlers = other._handlers;
	return *this;
}

const string& Logger::getName() const {
	return this->_name;
}

Logger& Logger::setName(std::string& name) {
	this->_name = name;
	return *this;
}

const LogLevel& Logger::getLogLevel() const {
	return *this->_level;
}

Logger& Logger::setLogLevel(const LogLevel& level) {
	this->_level = &level;
	return *this;
}

const Logger& Logger::log(const std::string& message) const {
	for (const shared_ptr<LoggerHandler>& handler : _handlers) {
		handler->callback(_name, message, *this->_level);
	}
	return *this;
}

const Logger& Logger::log(const std::string& message, const LogLevel& level) const {
	for (const shared_ptr<LoggerHandler>& handler : _handlers) {
		handler->callback(_name, message, level);
	}

	return *this;
}

const Logger& Logger::log(const dcl::tools::Exception& e, const LogLevel& level) const {
	for (const shared_ptr<LoggerHandler>& handler : _handlers) {
		handler->callback(_name, e, level);
	}

	return *this;
}

Logger& Logger::clearHandlers() {
	_handlers.clear();

	return *this;
}

std::ostream& dcl::tools::operator<<(std::ostream & out, const LogLevel & level) {
	return out << level._name;
}
