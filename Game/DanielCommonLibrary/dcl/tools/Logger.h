#ifndef _DCL_LOGGER_H_
#define _DCL_LOGGER_H_

#include <boost\utility.hpp>
#include <boost\type_traits\is_base_and_derived.hpp>
#include <string>
#include <vector>
#include <memory>
#include "Exception.h"

namespace dcl {
	namespace tools {
		typedef unsigned char byte;
		
		/* --------------------- Log Level --------------------- */

		// Static objects that represent the severity level of the info
		class LogLevel {
		private: // Object
			std::string _name;
			LogLevel(const std::string& name = "");

		public: // Object
			operator const std::string&() const;
			operator const char*() const;
			bool operator==(const LogLevel& other) const;

			const std::string& name() const;
			
			LogLevel(const LogLevel& other);
			LogLevel& operator=(const LogLevel& other);

			friend std::ostream& operator<<(std::ostream&, const LogLevel&);

			static const LogLevel info;
			static const LogLevel warning;
			static const LogLevel error;
			static const LogLevel fatal;
			static const LogLevel debug;
		};

		std::ostream& operator<<(std::ostream&, const LogLevel&);

		/* --------------------- Logger Callbacks --------------------- */

		// Used to store callback functions for the logger
		class LoggerHandler {
		public:
			// Functions that are called from the logger when new data is logged

			virtual void callback(const std::string& name, const std::string& message, const LogLevel& level) const = 0;
			virtual void callback(const std::string& name, const dcl::tools::Exception& exception, const LogLevel& level = LogLevel::error) const = 0;
			virtual void callback(const std::string& name, const std::string& message, const dcl::tools::Exception& exception, const LogLevel& level = LogLevel::error) const = 0;
			
			virtual ~LoggerHandler() = default;
			LoggerHandler() = default;
			LoggerHandler(const LoggerHandler&) = default;
			LoggerHandler& operator=(const LoggerHandler&) = default;
			LoggerHandler(LoggerHandler&&) = default;
		};

		// A default logger that logs the info to the standard output
		class DefaultLoggerHandler final : virtual public LoggerHandler {
		public:
			void callback(const std::string& name, const std::string& message, const LogLevel& level) const;
			void callback(const std::string& name, const dcl::tools::Exception& exception, const LogLevel& level = LogLevel::error) const;
			void callback(const std::string& name, const std::string& message, const dcl::tools::Exception& exception, const LogLevel& level = LogLevel::error) const;
		};
		
		/* --------------------- Logger --------------------- */

		// A class that handles the logging of info
		class Logger final {
		private:
			const LogLevel* _level;
			std::string _name;
			std::vector<std::shared_ptr<LoggerHandler>> _handlers;

		public:
			Logger(const std::string& name, const LogLevel& level = LogLevel::info);
			Logger(const Logger&);
			Logger& operator=(const Logger&);

			// Getters and setters

			const std::string& getName() const;
			Logger& setName(std::string&);
			const LogLevel& getLogLevel() const;
			Logger& setLogLevel(const LogLevel&);

			// Log functions
			const Logger& log(const std::string& message) const;
			const Logger& log(const std::string& message, const LogLevel& level) const;
			const Logger& log(const dcl::tools::Exception&, const LogLevel& level = LogLevel::error) const;
			
			// Adds a @LoggerHandler to the list of active handelrs
			template<typename T>
			typename boost::enable_if<boost::is_base_of<LoggerHandler, T>, Logger&>::type addHandler();

			Logger& clearHandlers();
		};

		template<typename T>
		typename boost::enable_if<boost::is_base_of<LoggerHandler, T>, Logger&>::type Logger::addHandler() {
			_handlers.push_back(make_shared<T>());
			return *this;
		}
	}
}

#endif