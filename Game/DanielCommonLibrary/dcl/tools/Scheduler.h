#pragma once

#ifndef _DCL_SCHEDULER_H
#define _DCL_SCHEDULER_H

#include <chrono>
#include <thread>

namespace dcl {
	namespace tools {
		template<typename Runnable>
		class Scheduler {
		private:
			Runnable m_Run;
			std::thread t;
			bool running = true;

		public:
			Scheduler(const Runnable& run);
			~Scheduler();

			void Join();
			void Stop();

			void RunTaskLater(double delay);
			void RunTaskTimer(double timer, double delay = 0.0);
		};

		template<typename Runnable>
		Scheduler<Runnable>::Scheduler(const Runnable& run) : m_Run(run) {}

		template<typename Runnable>
		Scheduler<Runnable>::~Scheduler() {
			Join();
		}

		template<typename Runnable>
		void Scheduler<Runnable>::Join() {
			running = false;
			if (t.joinable()) t.join();
		}

		template<typename Runnable>
		void Scheduler<Runnable>::Stop() {
			running = false;
		}

		template<typename Runnable>
		void Scheduler<Runnable>::RunTaskLater(double delay) {
			t = thread([](const Scheduler<Runnable>* _scheduler, double _delay) {
				std::this_thread::sleep_for(std::chrono::milliseconds(long(_delay * 1000)));
				_scheduler->m_Run();
			}, this, delay);
		}

		template<typename Runnable>
		void Scheduler<Runnable>::RunTaskTimer(double timer, double delay) {
			t = thread([](const Scheduler<Runnable>* _scheduler, double _timer, double _delay) {
				std::this_thread::sleep_for(std::chrono::milliseconds(long(_delay*1000)));

				while (_scheduler->running) {
					_scheduler->m_Run();
					std::this_thread::sleep_for(std::chrono::milliseconds(long(_timer * 1000)));
				}
			}, this, timer, delay);
		}
	}
}

#endif