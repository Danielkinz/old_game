#include "StringBuilder.h"

using namespace dcl::tools;


const std::string& StringBuilder::str() {
	return _str;
}

StringBuilder::operator const std::string&() {
	return _str;
}

StringBuilder::operator std::string() {
	return _str;
}
