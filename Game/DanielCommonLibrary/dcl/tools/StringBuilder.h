#ifndef _DCL_STRING_BUILDER_H_
#define _DCL_STRING_BUILDER_H_

#include <string>
#include <sstream>
#include "Utils.h"

namespace dcl {
	namespace tools {
		// An class that can convert a list of other objects into a single string
		class StringBuilder {
		private:
			std::string _str;
		
		public:
			template<typename T>
			StringBuilder(const T& t);

			template<typename T, typename... Args>
			StringBuilder(const T& t, const Args&... args);

			const std::string& str();
			operator const std::string&();
			operator std::string();

			template<typename T>
			std::string build(const T& t);

			template<typename T, typename... Args>
			std::string build(const T& t, const Args&... args);
		};

		template<typename T>
		StringBuilder::StringBuilder(const T& t) {
			std::stringstream ss;
			ss << t;
			_str = ss.str();
		}

		template<typename T, typename... Args>
		StringBuilder::StringBuilder(const T& t, const Args&... args) {
			std::stringstream ss;
			ss << t;
			_str = dcl::tools::unpackToString(ss, args...).str();
		}

		template<typename T>
		std::string StringBuilder::build(const T& t) {
			std::stringstream ss;
			ss << t;
			return ss.str();
		}

		template<typename T, typename... Args>
		std::string StringBuilder::build(const T& t, const Args&... args) {
			std::stringstream ss;
			ss << t;
			return dcl::tools::unpackToString(ss, args...).str();
		}
	}
}

#endif