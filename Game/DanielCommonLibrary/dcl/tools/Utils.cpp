#include "Utils.h"
#include <iterator>
#include <algorithm>
#include <fstream>
#include "Exception.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string.hpp>

using namespace std;
using namespace boost;

bool dcl::tools::ReadFile(const char* pFileName, string& outFile) {
	ifstream f(pFileName);

	bool ret = false;

	if (f.is_open()) {
		string line;
		while (getline(f, line)) {
			outFile.append(line);
			outFile.append("\n");
		}

		f.close();

		ret = true;
	} else {
		throw dcl::tools::Exception("Could not open file '", pFileName, ",");
	}

	return ret;
}

void dcl::tools::split(const std::string& str, vector<string>& tokens, const std::string& delimiter) {
	string s = str;
	size_t pos = 0;
	std::string token;
	while ((pos = s.find(delimiter)) != std::string::npos) {
		token = s.substr(0, pos);
		tokens.push_back(token);
		s.erase(0, pos + delimiter.length());
	}
	tokens.push_back(s);
}
