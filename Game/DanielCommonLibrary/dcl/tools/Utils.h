#ifndef _DCL_UTILS_H_
#define _DCL_UTILS_H_

// A file that stores constants, includes standard libraries
// and defines utility functions that are used across the program

#ifndef _WIN32
#include <unistd.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <vector>
#include <string.h>
#include <assert.h>
#include <sstream>
#include <ostream>

// Utility macros

#define ZERO_MEM(a) memset(a, 0, sizeof(a));
#define ZERO_MEM_VAR(var) memset(&var, 0, sizeof(var));
#define ARRAY_SIZE_IN_ELEMENTS(a) (sizeof(a)/sizeof(a[0]))
#define SAFE_DELETE(p) if (p) {delete p; p = nullptr;}
#define SAFE_DELETE_ARRAY(p) if (p) {delete p; p = nullptr;}

// Windows
#ifdef _WIN32 
#define SNPRINTF _snprintf_s
#define VSNPRINTF vsnprintf_s
#define RANDOM rand
#define SRANDOM srand((unsigned int) time(NULL))
// Linux
#else 
#define SNPRINTF snprintf
#define VSNPRINTF vsnprintf
#define RANDOM rand
#define SRANDOM srand(getpid())
#endif

typedef unsigned int uint;

#define CONVERTER(FROM, TO, VALUE) union {} 

namespace dcl {
	namespace tools {

		bool ReadFile(const char* pFileName, std::string& outFile);

		void split(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters = " ");

		template<typename T>
		std::stringstream& unpackToString(std::stringstream& ss, const T& t) {
			ss << t;

			return ss;
		}

		template<typename T, typename... Args>
		std::stringstream& unpackToString(std::stringstream& ss, const T& t, const Args&... args) {
			ss << t;
			return unpackToString(ss, args...);
		}

		template<typename T>
		std::ostream& unpackToStream(std::ostream& ss, const T& t) {
			ss << t;
			return ss;
		}

		template<typename T, typename... Args>
		std::ostream& unpackToStream(std::ostream& ss, const T& t, const Args&... args) {
			ss << t;
			return unpackToStream(ss, args...);
		}
	};
}

#endif
