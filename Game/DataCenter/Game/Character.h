#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>

namespace data_center {
	namespace game {
		class Character {
		private:
			std::string _username;
			std::string _characterName;
		public:
			Character(const std::string& username, const std::string& charname);
			~Character();
			Character& operator=(const Character& other);
			Character(const Character& other);

			const std::string& getCharacterName() const;
			const std::string& getUsername() const;
		};
	}
}

#endif