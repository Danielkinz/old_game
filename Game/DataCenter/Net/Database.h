#ifndef DATABASE_H
#define DATABASE_H

#include <mutex>
#include "game\Character.h"
#include "dcl\net\MySQL_DB.h"

#define DEFAULT_URL "localhost:3306"
#define DEFAULT_USERNAME "Daniel"
#define DEFAULT_PASSWORD "1234"

namespace data_center {
	namespace net {
		class Database : public dcl::net::MySQL_DB {
		private:
			// Prepared Statement ID's
			enum Statements {
				ADD_USER, SET_USER_SESSION,
				IS_USER_LOGGED_IN, GET_USER_BY_SESSION,
				CREATE_CHARACTER_REQ, GET_CHARACTER, 
				GET_CHARACTERS_LIST, IS_CHARACTER_NAME_TAKEN
			};
			// Map of prepared statement id to prepared statement
			std::map<Statements, sql::PreparedStatement*> _statements;
			
			// lock to make all of the functions synchronized
			std::recursive_mutex _mtx;

		public:
			Database();
			virtual ~Database();
			void connect(const std::string& url = DEFAULT_URL, const std::string& username = DEFAULT_USERNAME, const std::string& password = DEFAULT_PASSWORD);

			// Adds a new user to the database
			int addUser(const std::string& username);

			// Updates the user session
			int setUserSession(const std::string& username, const std::string& sessionID);
			
			// Returns the user that the session belongs to
			std::string getUserBySession(const std::string& session);

			// Creates a new character called @characterName that belongs to the user @username
			std::shared_ptr<game::Character> createCharacter(const std::string& username, const std::string& characterName);
			
			// Returns the info about the character called @characterName
			std::shared_ptr<game::Character> getCharacter(const std::string& characterName);
			
			// Returns a list of all the characters that belong to the user @username
			std::list<std::shared_ptr<game::Character>> getCharactersList(const std::string& username);
			
			// Returns true if a character called @characterName exists
			bool isCharacterNameTaken(const std::string& characterName);
		};
	}
}

#undef DEFAULT_URL
#undef DEFAULT_USERNAME
#undef DEFAULT_PASSWORD

#endif