#include "Server.h"

#include <thread>

using namespace std;
using namespace data_center::net;
using namespace data_center::net::clients;
using namespace data_center::game;
using namespace dcl::net;
using namespace dcl::tools;
using namespace dcl::structures;
using namespace boost::asio;

/* ---------------------------- DATA ---------------------------- */

Server* __server = nullptr;
io_context* __context = nullptr;

/* ---------------------------- OBJECT ---------------------------- */

Server::Server(io_service& service, const string& name, ushort port, const std::string& loginServer, ushort loginServerPort) 
	: TCPServer(service, port), _loginServer(service, loginServer, loginServerPort) {
	ServerData::init(name, port);
	Player::init();
	LoginServer::init();
	GameServer::init();
	_loginServer.addDisconnectionHandler(stop);
}

void Server::acceptHandler(Session* new_session, const boost::system::error_code& error) {
	if (error) {
		delete new_session;
		return;
	}

	Buffer<byte>& payload = new_session->read();
	byte b = payload.Read<byte>();

	if (b == 0) { // Client
		SERVER_LOGGER.log("New player connected (" + new_session->getSocket().remote_endpoint().address().to_string() + ")");
		auto player = std::make_shared<Player>(*new_session);
		player->setSelf(player);
		_players.insert(player);
	} else if (b == 1) { // Game Server
		try {
			shared_ptr<GameServer> gameServer = make_shared<GameServer>(*new_session);
			gameServer->SetSelf(gameServer);
			_gameServers.insert(gameServer);

		} catch (ValidationException& e) {
			SERVER_LOGGER.log(e);
		}
	} else { // Error
		Console::writeln("Error while connecting to new peer: Invalid login code (", new_session->getSocket().remote_endpoint().address().to_string(), ")");
		new_session->disconnect();
	}

	delete new_session;
}

Server::~Server() {
	for (auto player : _players)
		player->disconnect();

	_players.clear();

	_loginServer.disconnect();
}

/* ---------------------------- STATIC ---------------------------- */

void Server::start(boost::asio::io_service& service, const string& name, ushort port, const std::string& loginServer, ushort loginServerPort) {
	if (__server) throw Exception("Server already running!");
	__context = &service;
	__server = new Server(service, name, port, loginServer, loginServerPort);
	service.run();
}

void Server::stop() {
	static bool stopping = false;
	if (stopping) return;
	stopping = true;

	SAFE_DELETE(__server);
	if (!__context->stopped()) __context->stop();

	stopping = false;
}
