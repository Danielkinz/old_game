#ifndef SERVER_H
#define SERVER_H

#include <boost\asio.hpp>
#include "net\clients\LoginServer.h"
#include "net\ServerData.h"
#include "dcl\net\TCPServer.h"

namespace data_center {
	namespace net {
		typedef unsigned short ushort;

		class Server : public dcl::net::TCPServer {
		private: // Object
			Server(boost::asio::io_service&, const std::string&, ushort port, const std::string& loginServer, ushort loginServerPort);

			clients::LoginServer _loginServer;

			// Event handler for new connections (TCPServer)
			void acceptHandler(dcl::net::Session*, const boost::system::error_code&);

		public: // Object
			~Server();

		public: // Static
			static void start(boost::asio::io_service&, const std::string& name, ushort port, const std::string& loginServer, ushort loginServerPort);
			static void stop();
		};
	}
}

#endif