#include "ServerData.h"

using namespace std;
using namespace boost;
using namespace dcl::tools;
using namespace data_center::net;
using namespace data_center::net::clients;

Database ServerData::database;
unordered_set<std::shared_ptr<Player>> ServerData::players;
unordered_set<std::shared_ptr<GameServer>> ServerData::gameServers;
Logger ServerData::logger("Server");

unsigned short ServerData::port;
string ServerData::name;

void ServerData::init(const string& name, unsigned short port) {
	ServerData::database.connect();
	ServerData::name = name;
	ServerData::port = port;
}