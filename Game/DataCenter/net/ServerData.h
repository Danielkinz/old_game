#ifndef SERVER_DATA
#define SERVER_DATA

#include <boost\unordered_map.hpp>
#include <boost\unordered_set.hpp>
#include "net\clients\Player.h"
#include "net\Database.h"
#include "net\clients\GameServer.h"
#include "dcl\tools\Logger.h"

// This file is used to share constants and info between the rest of the objects in the program

namespace data_center {
	namespace net {
		// Class that allows sharing global info between all the other classes
		class ServerData final {
		public:
			static unsigned short port;
			static std::string name;
			static Database database;
			static dcl::tools::Logger logger;
			static boost::unordered_set<std::shared_ptr<data_center::net::clients::Player>> players;
			static boost::unordered_set<std::shared_ptr<data_center::net::clients::GameServer>> gameServers;

			static void init(const std::string& name, unsigned short port);
		};
	}
}

// Constants that represent the static members of the class

#define _players ServerData::players
#define _database ServerData::database
#define SERVER_LOGGER ServerData::logger
#define SERVER_PORT ServerData::port
#define SERVER_NAME ServerData::name
#define _gameServers ServerData::gameServers

// Communication constants

#define SESSION_LENGTH 64
#define DATA_CENTER_CERTIFICATE "ZDQ3NGMzbjczcmMzcjcxZjFjNDczdjRsMWQ0NzEwbg"
#define DATA_CENTER_CERTIFICATE_LENGTH strlen(DATA_CENTER_CERTIFICATE)
#define GAME_SERVER_CERTIFICATE "NTkyNjc1MDViYmRhZjc4ZWFlODkxOTZlMTg5YTBkZjI3OTI0Y2QzZQ"
#define GAME_SERVER_CERTIFICATE_LENGTH strlen(GAME_SERVER_CERTIFICATE)

// Connection constants

#define DEFAULT_LOGIN_SERVER_NAME "127.0.0.1"
#define DEFAULT_LOGIN_SERVER_PORT 5300
#define DEFAULT_SERVER_PORT 5301

// Player distribution settings

// Don't allow players to log in
#define DISTRIBUTION_OFF 0

// Send the player to the first available server
#define DISTRIBUTION_FIRST 1

// Send the player to the server with the least players
#define DISTRIBUTION_ROUND_ROBIN 2

// Send the player to the server with the least used capacity (NOT IMPLEMENTED)
//#define DISTRIBUTION_PERCENT 3

// The active distribution mode
#define DISTRIBUTION_MODE DISTRIBUTION_ROUND_ROBIN

#endif