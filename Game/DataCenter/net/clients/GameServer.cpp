#include "GameServer.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"
#include <dcl\tools\Exception.h>

using namespace std;
using namespace data_center::net;
using namespace data_center::net::clients;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;

boost::unordered_map<GameServer::MessageType, GameServer::ProtocolHandler> GameServer::_handlers;
unsigned int GameServer::NextServerID = 1;

/* ---------------------------- STATIC ---------------------------- */

void GameServer::init() {
	_handlers[MessageType::DISCONNECT] = &GameServer::DisconnectHandler;
}

/* ---------------------------- OBJECT ---------------------------- */

GameServer::GameServer(dcl::net::Session& session) : Session(session), _name("Game Server "), _logger(_name) {
	string str;
	Buffer<byte> buffer;

	// Validates the DataCenter using the certificate
	sendData(MessageType::VALIDATION);
	read(buffer);
	str = string((char*)buffer.Read(GAME_SERVER_CERTIFICATE_LENGTH), GAME_SERVER_CERTIFICATE_LENGTH);

	if (str != GAME_SERVER_CERTIFICATE) {
		disconnect();
		throw ValidationException();
	}

	// Retrieves the port on which the data center receives clients
	sendData(MessageType::PORT_REQ);
	read(buffer);
	_port = buffer.Read<ushort>();

	_name += to_string(NextServerID++);

	_logger.setName(_name);

	_logger.log(StringBuilder("Game server connected from ", getSocket().remote_endpoint().address().to_string(), ":", _port));
}

GameServer::GameServer(GameServer& other) : Session(other), _name(other._name), _logger(other._name) {}

GameServer& GameServer::operator=(dcl::net::Session& other) {}

GameServer& GameServer::operator=(GameServer& other) {}

void GameServer::sendData(byte code, const string& payload) {
	Buffer<byte> buff(&code, 1);
	buff.Write((byte*)payload.data(), payload.length());
	Session::sendData(buff);
}

void GameServer::sendData(MessageType code, const std::string& payload) {
	sendData((byte)code, payload);
}

const string& GameServer::GetName() const {
	return _name;
}

unsigned short GameServer::GetPort() const {
	return _port;
}

unsigned int GameServer::GetIP() const {
	return getSocket().remote_endpoint().address().to_v4().to_uint();
}

byte GameServer::GetPlayerCount() {
	byte ret;
	Buffer<byte> buff;
	sendData(MessageType::PLAYER_COUNT);
	read(buff);
	ret = buff.Read<byte>();
	_logger.log(StringBuilder((int) ret, " players are currently online"));
	return ret;
}

void GameServer::disconnect() {
	sendData(MessageType::DISCONNECT);
	Session::disconnect();
}

/* ---------------------------- Handlers ---------------------------- */

GameServer::Message GameServer::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::DISCONNECT:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void GameServer::SetSelf(std::shared_ptr<GameServer>& ptr) {
	_self = ptr;
}

void GameServer::DisconnectHandler(GameServer::Message& msg) {
	_logger.log(StringBuilder("disconnected"));
	_gameServers.erase(_self);
	Session::disconnect();
	_self = nullptr;
}
