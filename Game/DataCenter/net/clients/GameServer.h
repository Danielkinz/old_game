#ifndef DATA_CENTER_H
#define DATA_CENTER_H

#include "net\Database.h"
#include <boost\unordered_map.hpp>
#include "dcl\net\Messages.h"
#include "dcl\net\Session.h"
#include "dcl\structures\Buffer.h"
#include "dcl\tools\Logger.h"

namespace data_center {
	namespace net {
		namespace clients {

			class GameServer final : public dcl::net::Session {
			private: // Types
				typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
				typedef std::shared_ptr<message_t> Message;
				typedef void(GameServer::*ProtocolHandler)(Message&);

				typedef unsigned char byte;

				// The message codes that define the protocol
				enum class MessageType : byte {
					VALIDATION, PORT_REQ, PLAYER_COUNT,
					DISCONNECT = 255
				};

			private: // Static

				static unsigned int NextServerID;
					 // maps message codes to their handlers
				static boost::unordered_map<MessageType, ProtocolHandler> _handlers;

			public: // Static
				static void init();

			private: // Object
				std::string _name;
				dcl::tools::Logger _logger;
				unsigned short _port;
				std::shared_ptr<GameServer> _self;

				void sendData(byte code, const std::string& payload = "");
				void sendData(MessageType code, const std::string& payload = "");

			public: // Object
				GameServer(dcl::net::Session&);
				GameServer(GameServer&);
				GameServer& operator=(dcl::net::Session&);
				GameServer& operator=(GameServer&);

				void SetSelf(std::shared_ptr<GameServer>&);
				virtual void disconnect();

				unsigned int GetIP() const;
				const std::string& GetName() const;
				unsigned short GetPort() const;

				byte GetPlayerCount();

			private: // Handlers
					 // Parses the arguments from the received message
				Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

				// Protocol Handlers
				void DisconnectHandler(Message&);
			};
		}
	}
}

#endif