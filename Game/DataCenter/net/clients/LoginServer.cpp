#include "LoginServer.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"
#include "net\ServerData.h"

using namespace std;
using namespace data_center::net;
using namespace data_center::net::clients;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;
using namespace boost::asio;

boost::unordered_map<LoginServer::MessageType, LoginServer::ProtocolHandler> LoginServer::_handlers;

/* ---------------------------- STATIC ---------------------------- */

void LoginServer::init() {
	_handlers[MessageType::VALIDATION] = &LoginServer::ValidationHandler;
	_handlers[MessageType::NAME_REQ] = &LoginServer::NameRequestHandler;
	_handlers[MessageType::SESSION_TRANSFER] = &LoginServer::TransferSessionHandler;
	_handlers[MessageType::PORT_REQ] = &LoginServer::portRequestHandler;

	_handlers[MessageType::DISCONNECT] = &LoginServer::DisconnectHandler;
}

/* ---------------------------- OBJECT ---------------------------- */

LoginServer::LoginServer(io_service& service, const std::string& host, unsigned short port) 
	: TCPClient(service, host, port), _logger("Login Server") {

	Buffer<byte> buff;
	buff.Write(1);
	TCPClient::sendData(buff);

	TCPClient::startListening();
}

LoginServer::LoginServer(LoginServer& other) 
	: TCPClient(other), _logger(other._logger) {

	TCPClient::startListening();
}

LoginServer& LoginServer::operator=(LoginServer& other) {
	TCPClient::operator=(other);
	_logger = other._logger;
	TCPClient::startListening();
}

void LoginServer::sendData(const MessageType code, const string& payload) {
	Buffer<byte> buff((byte*)&code, 1);
	buff.Write((byte*)payload.data(), payload.length());
	TCPClient::sendData(buff);
}

void LoginServer::sendData(const string& payload) {
	Buffer<byte> buff((byte*)payload.data(), payload.length());
	TCPClient::sendData(buff);
}

/* ---------------------------- Handlers ---------------------------- */

bool LoginServer::handleRead(dcl::structures::Buffer<byte>& buffer, const boost::system::error_code& error) {
	Message msg;

	if (error) {
		if (error.value() == 10009) return false; // Disconnected
		else _logger.log(StringBuilder("Read error: (", error.value(), ") ", error.message()));
		disconnect();
		return false;
	}

	msg = buildReceivedMessage(buffer);
	(this->*(_handlers[(MessageType)msg->getMessageCode()]))(msg);
	return true;
}

LoginServer::Message LoginServer::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::SESSION_TRANSFER: // user, session
		arguments.push_back(Protocol::ReadStringFromBuffer(payload, payload.Read<byte>()));
		arguments.push_back(Protocol::ReadStringFromBuffer(payload, payload.Read<byte>()));
	case MessageType::NAME_REQ:
	case MessageType::VALIDATION:
	case MessageType::DISCONNECT:
	case MessageType::PORT_REQ:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void LoginServer::DisconnectHandler(Message& msg) {
	_logger.log("disconnected");
	disconnect();
}

void LoginServer::NameRequestHandler(Message& msg) {
	_logger.log("Received name request, answering");
	sendData(Protocol::ToString<byte>(SERVER_NAME.length()) + SERVER_NAME);
}

void LoginServer::ValidationHandler(Message& msg) {
	_logger.log("Received verification request from server, answering");
	sendData(DATA_CENTER_CERTIFICATE);
}

void LoginServer::portRequestHandler(Message& msg) {
	_logger.log("Received port request from server, answering");
	sendData(Protocol::ToString<ushort>(SERVER_PORT));
}

void LoginServer::TransferSessionHandler(Message& msg) {
	string& user = msg->getValue()[0];
	string& session = msg->getValue()[1];

	_logger.log(StringBuilder("User session update. User: \"", user, "\", Session: \"", session, "\""));
	_database.addUser(user);
	_database.setUserSession(user, session);
	sendData("\1");
}
