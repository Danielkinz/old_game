#ifndef LOGIN_SERVER
#define LOGIN_SERVER

#include <boost\unordered_map.hpp>
#include "net\Database.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\net\Messages.h"
#include "dcl\net\TCPClient.h"
#include "dcl\tools\Logger.h"

namespace data_center {
	namespace net {
		namespace clients {
			class LoginServer : public dcl::net::TCPClient {
			private: // Types
				typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
				typedef std::shared_ptr<message_t> Message;
				typedef void(LoginServer::*ProtocolHandler)(Message&);

				// Protocol message codes
				enum class MessageType : byte {
					VALIDATION, NAME_REQ, PORT_REQ,
					SESSION_TRANSFER,
					DISCONNECT = 255
				};

			private: // Static
				// Map of message codes to handler function
				static boost::unordered_map<MessageType, ProtocolHandler> _handlers;

			public: // Static
				static void init();

			private: // Object
				dcl::tools::Logger _logger;
				void sendData(const std::string& payload);
				void sendData(const MessageType code, const std::string& payload = "");

			public: // Object
				LoginServer(boost::asio::io_service&, const std::string& host = DEFAULT_LOGIN_SERVER_NAME, unsigned short port = DEFAULT_LOGIN_SERVER_PORT);

				LoginServer(LoginServer&);
				LoginServer& operator=(LoginServer&);

			protected: // Handlers
				// Handles read event from Session
				bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

			private: // Handlers
				// Parses the arguments from the message
				Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

				// Protocol Handler functions
				void DisconnectHandler(Message&);
				void ValidationHandler(Message&);
				void NameRequestHandler(Message&);
				void portRequestHandler(Message&);
				void TransferSessionHandler(Message&);
			};
		}
	}
}

#endif