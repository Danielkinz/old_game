#include "Player.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"

using namespace std;
using namespace data_center::game;
using namespace data_center::net;
using namespace data_center::net::clients;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;

boost::unordered_map<Player::MessageType, Player::ProtocolHandler> Player::_handlers;

/* ---------------------------- STATIC ---------------------------- */

void Player::init() {
	_handlers[MessageType::DISCONNECT] = &Player::DisconnectHandler;
	_handlers[MessageType::CREATE_CHARACTER_REQ] = &Player::CreateCharacterHandler;
	_handlers[MessageType::SERVER_GET] = &Player::ServerGetHandler;
}

/* ---------------------------- OBJECT ---------------------------- */

Player::Player(dcl::net::Session& session) : Session(session), _logger("Unregistered Player") {
	Buffer<byte> buffer;
	string session_id;
	list<Character*> characters;

	// Retrieve the session
	sendData(MessageType::SESSION);
	read(buffer);
	session_id = Protocol::ReadStringFromBuffer(buffer, buffer.Read<byte>());

	_username = _database.getUserBySession(session_id);
	_logger.setName(_username).log(StringBuilder("Connected from ", Session::getSocket().remote_endpoint().address().to_string()));

	sendCharacters();

	Session::startListening();
}

Player::Player(Player& other) : Session(other), _logger(other._logger) {
	this->_username = other._username;
	Session::startListening();
}

Player::~Player() {}

Player& Player::operator=(dcl::net::Session& other) {
	Session::operator=(other);
	Session::startListening();
}

Player& Player::operator=(Player& other) {
	Session::operator=(other);

	this->_username = other._username;
	this->_logger = other._logger;
	Session::startListening();
}

void Player::sendData(const MessageType code, const string& payload) {
	Buffer<byte> buff((byte*)&code, 1);
	buff.Write((byte*)payload.data(), payload.length());
	Session::sendData(buff);
}

void Player::setSelf(shared_ptr<Player>& self) {
	_self = self;
}

void Player::disconnect() {
	_players.erase(_self);
	Session::disconnect();
}

const string& Player::getUsername() const {
	if (_username.empty()) throw Exception("Username not set");
	return _username;
}

Character& Player::getCharacter() const {
	if (_character == nullptr) throw Exception("Character not selected");
	return (*_character);
}

void Player::sendCharacters() {
	// Send the characters
	_logger.log("Sending character list");
	list<shared_ptr<Character>> characters = _database.getCharactersList(_username);
	string data = Protocol::ToString<byte>(characters.size());
	for (auto c : characters) {
		data += Protocol::ToString<byte>(c->getCharacterName().size()) + c->getCharacterName();
	}
	sendData(MessageType::CHARACTER_LIST, data);
}

/* ---------------------------- Handlers ---------------------------- */

bool Player::handleRead(dcl::structures::Buffer<byte>& buffer, const boost::system::error_code& error) {
	Message msg;

	if (error) {
		// Logs the error
		if (error.value() == 10009); // Disconnected message already displayed
		else if (error.value() == 2) _logger.log("disconnected");
		else _logger.log(StringBuilder("Read error: (", error.value(), ") ", error.message()));
		return false;
	}
	try {
		// Parses the message and passes it through the handlers
		msg = buildReceivedMessage(buffer);
		(this->*(_handlers[(MessageType)msg->getMessageCode()]))(msg);
		return true;
	} catch (Exception& e) {
		_logger.log(e);
		_logger.log("Disconnecting player");
		return false;
	}
}

Player::Message Player::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::SERVER_GET: // character_name
	case MessageType::CREATE_CHARACTER_REQ: // character_name
		arguments.push_back(Protocol::ReadStringFromBuffer(payload, payload.Read<byte>()));
	case MessageType::DISCONNECT:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void Player::ServerGetHandler(Message& msg) {
	// Defined in ServerData.h
#if DISTRIBUTION_MODE == DISTRIBUTION_ROUND_ROBIN
	Buffer<byte> buff;
	_logger.log(StringBuilder("Requested game server for character \"", msg->getValue()[0], "\""));
	shared_ptr<GameServer> currentServer;
	byte currentMinimum = 255;
	byte nextMinimum = 0;

	// Checks which server has the least players
	for (auto it = _gameServers.begin(); it != _gameServers.end(); it++) {
		nextMinimum = (*it)->GetPlayerCount();
		if (nextMinimum < currentMinimum) currentServer = *it;
		currentMinimum = nextMinimum;
		if (!nextMinimum) break;
	}

	// Sends the server info the the client
	buff.Write<byte>((byte) MessageType::SERVER_RET);
	buff.Write<uint>(currentServer->GetIP());
	buff.Write<ushort>(currentServer->GetPort());
	sendData(buff);
#elif DISTRIBUTION_MODE == DISTRIBUTION_FIRST
	// Sends the info of the first server in the list
	Buffer<byte> buff;
	_logger.log(StringBuilder("Requested game server for character \"", msg->getValue()[0], "\""));
	buff.Write<byte>((byte)MessageType::SERVER_RET);
	buff.Write<uint>((*_gameServers.begin())->GetIP());
	buff.Write<ushort>((*_gameServers.begin())->GetPort());
	sendData(buff);
#endif
}

void Player::CreateCharacterHandler(Player::Message& msg) {
	_logger.log(StringBuilder("Character create request: \"", msg->getValue()[0], "\""));
	shared_ptr<Character> c = _database.createCharacter(this->_username, msg->getValue()[0]);
	
	if (c == nullptr) {
		// Character creation failed
		sendData(MessageType::CREATE_CHARACTER_RET, Protocol::ToString<byte>(1));
	} else {
		// Character creation successful
		sendData(MessageType::CREATE_CHARACTER_RET, Protocol::ToString<byte>(0));
		sendCharacters();
	}
}

void Player::DisconnectHandler(Player::Message& msg) {
	_logger.log("disconnected");
	_players.erase(_self);
	Session::disconnect();
}
