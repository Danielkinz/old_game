#ifndef PLAYER_H
#define PLAYER_H

#include <boost\unordered_map.hpp>
#include "Net\Database.h"
#include "Game\Character.h"
#include "dcl\net\Messages.h"
#include "dcl\net\Session.h"
#include "dcl\structures\Buffer.h"
#include "dcl\tools\Logger.h"

namespace data_center {
	namespace net {
		namespace clients {
			typedef unsigned char byte;

			class Player final : public dcl::net::Session {
			private: // Types
				typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
				typedef std::shared_ptr<message_t> Message;
				typedef void(Player::*ProtocolHandler)(Message&);

				// The protocol messages
				enum class MessageType : byte {
					SESSION, CHARACTER_LIST,
					CREATE_CHARACTER_REQ, CREATE_CHARACTER_RET,
					SERVER_GET, SERVER_RET,
					DISCONNECT = 255
				};

			private: // Static
				// List of protocol handlers
				static boost::unordered_map<MessageType, ProtocolHandler> _handlers;

			public: // Static
				static void init();

			private: // Object
				std::string _username;
				dcl::tools::Logger _logger;
				std::shared_ptr<Player> _self;
				std::shared_ptr<game::Character> _character;

				using dcl::net::Session::sendData;
				void sendData(const MessageType code, const std::string& payload = "");

			public: // Object
				Player(dcl::net::Session&);
				Player(Player&);
				~Player();
				Player& operator=(dcl::net::Session&);
				Player& operator=(Player&);

				void setSelf(std::shared_ptr<Player>&);
				void disconnect();

				// Returns the username or an empty string
				const std::string& getUsername() const;

				// Returns the current character on which the player is playing
				game::Character& getCharacter() const;

				// Sends a list of characters to the player
				void sendCharacters();

			protected: // Handlers
				// Handles the read even from Session
				bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

			private: // Handlers
				// Parses the arguments from the received data
				Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

				// Protocol Handler functions
				void ServerGetHandler(Message&);
				void CreateCharacterHandler(Message&);
				void DisconnectHandler(Message&);
			};
		}
	}
}

#endif