#include "Character.h"

using namespace game_server::game;
using namespace std;

Character::Character(const string& username, const string& characterName) {
	this->_username = username;
	this->_characterName = characterName;
}

Character::~Character() {}

Character::Character(const Character& other) {
	this->_username = other._username;
	this->_characterName = other._characterName;
}

Character& Character::operator=(const Character& other) {
	this->_username = other._username;
	this->_characterName = other._characterName;
	return *this;
}

const string& Character::getCharacterName() const {
	return this->_characterName;
}

const string& Character::getUsername() const {
	return this->_username;
}