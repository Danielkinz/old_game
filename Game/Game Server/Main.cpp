#ifndef MAIN_CPP
#define MAIN_CPP

#include <iostream>
#include <boost\asio.hpp>
#include "net\Server.h"
#include "net\ServerData.h"
#include "dcl\tools\Logger.h"
#include "dcl\tools\StringBuilder.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\StringBuilder.h"
#include <chrono>

#define STOP getchar(); return 1;

using namespace std;
using namespace game_server::net;
using namespace dcl::net;
using namespace dcl::tools;
using namespace boost::asio;

int main(int argc, char* argv[]) {
	// Initialization
	Logger logger("Global");
	string loginServer = DEFAULT_LOGIN_SERVER_NAME;
	int loginServerPort = DEFAULT_LOGIN_SERVER_PORT;

	// Missing arguments
	if (argc < 3) {
		cout << "Syntax: GameServer.exe <DataCenter_Name> <port> [LoginServer_IP [LoginServer_Port]]" << endl;
		STOP;
	}

	// Parse arguments
	if (argc > 3) loginServer = argv[3];
	if (argc > 4) loginServerPort = stoi(argv[4]);

	logger.log(StringBuilder("Game server starting..."));

	try {
		io_service service;
		logger.log("Game server is now running!");
		Server::start(service, argv[1], stoi(argv[2]), loginServer, loginServerPort);

		logger.log("Stopping...");
		Server::stop();
	} catch (sql::SQLException& e) {
		logger.log(StringBuilder("SQL Exception:\n (", e.getErrorCode(), ") ", e.what()), LogLevel::fatal);
		STOP;
	} catch (Exception& e) {
		logger.log(e, LogLevel::fatal);
		STOP;
	} catch (boost::system::system_error& e) {
		logger.log(StringBuilder(e.what(), " (", e.code(), ")"));
		STOP;
	} catch (...) {
		logger.log("An unknown error has occured");
		STOP;
	}

	cout << "Goodbye!" << endl;
	return 0;
}


#endif