#include "Database.h"
#include "dcl\tools\Utils.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\Exception.h"

using namespace std;
using namespace game_server::net;
using namespace game_server::game;
using namespace dcl::tools;

Database::Database() : MySQL_DB() {}

Database::~Database() {
	// Deletes all the prepared statements
	for (auto it = _statements.begin(); it != _statements.end(); it++) {
		if (it->second) {
			it->second->close();
			SAFE_DELETE(it->second);
		}
	}
}

void Database::connect(const std::string& url, const std::string& username, const std::string& password) {
	MySQL_DB::connect(url, username, password, "RegionalDB");

	// Initializes all the prepared statements
	_statements[ADD_USER] = getConnection()->prepareStatement("CALL addUser(?);");
	_statements[SET_USER_SESSION] = getConnection()->prepareStatement("CALL setUserSession(?,?);");
	_statements[GET_USER_BY_SESSION] = getConnection()->prepareStatement("CALL getUserBySession(?);");
	_statements[CREATE_CHARACTER_REQ] = getConnection()->prepareStatement("CALL createCharacter(?,?);");
	_statements[GET_CHARACTER] = getConnection()->prepareStatement("CALL getCharacter(?);");
	_statements[GET_CHARACTERS_LIST] = getConnection()->prepareStatement("CALL getCharactersList(?);");
	_statements[IS_CHARACTER_NAME_TAKEN] = getConnection()->prepareStatement("SELECT isCharacterNameTaken(?) AS result;");
}

// This statement locks the mutex for the duration of the function
#define GUARD_MUTEX std::lock_guard<std::recursive_mutex> lock(this->_mtx)

int Database::addUser(const std::string& username) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[ADD_USER];
	stmt->setString(1, username);
	return stmt->executeUpdate();
}

int Database::setUserSession(const std::string& username, const std::string& sessionID) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[SET_USER_SESSION];
	stmt->setString(1, username);
	stmt->setString(2, sessionID);
	return stmt->executeUpdate();
}

string Database::getUserBySession(const string & session) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[GET_USER_BY_SESSION];
	std::unique_ptr<sql::ResultSet> res;
	string user;

	stmt->setString(1, session);
	res.reset(stmt->executeQuery());

	if (!res->next()) return ""; // No resutl found
	user = res->getString("_username");
	res->close();
	while (stmt->getMoreResults()) stmt->getResultSet()->close();
	return user;
}

shared_ptr<Character> Database::createCharacter(const std::string & username, const std::string & characterName) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[CREATE_CHARACTER_REQ];

	stmt->setString(1, username);
	stmt->setString(2, characterName);

	// if no rows were updated
	if (!stmt->executeUpdate()) return nullptr;
	
	while (stmt->getMoreResults()) stmt->getResultSet()->close();
	return make_shared<Character>(username, characterName);
}

shared_ptr<Character> Database::getCharacter(const std::string & characterName) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[GET_CHARACTER];
	std::unique_ptr<sql::ResultSet> rs;
	std::shared_ptr<Character> character;

	stmt->setString(1, characterName);
	rs.reset(stmt->executeQuery());

	if (!rs->next()) return nullptr; // No results found
	character = make_shared<Character>(rs->getString("_username"), rs->getString("_character_name"));
	rs->close();

	while (stmt->getMoreResults()) stmt->getResultSet()->close();
	return character;
}

list<shared_ptr<Character>> Database::getCharactersList(const string & username) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[GET_CHARACTERS_LIST];
	unique_ptr<sql::ResultSet> rs;
	list<shared_ptr<Character>> characters;

	stmt->setString(1, username);
	rs.reset(stmt->executeQuery());
	while (rs->next())
		characters.push_front(make_shared<Character>(rs->getString("_username"), rs->getString("_character_name")));
	rs->close();

	while (stmt->getMoreResults()) stmt->getResultSet()->close();
	return characters;
}

bool Database::isCharacterNameTaken(const std::string & charname) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[IS_CHARACTER_NAME_TAKEN];
	unique_ptr<sql::ResultSet> rs;
	bool result = false;
	
	stmt->setString(1, charname);
	stmt->executeQuery();
	
	rs.reset(stmt->getResultSet());
	if (!rs->next()) return false;
	result = rs->getBoolean("result");
	rs->close();
	
	while (stmt->getMoreResults()) stmt->getResultSet()->close();
	return result;
}

#undef GUARD_MUTEX
