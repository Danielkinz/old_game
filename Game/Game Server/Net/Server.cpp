#include "Server.h"

#include <dcl\tools\StringBuilder.h>
#include "clients\LoginServer.h"
#include <thread>

using namespace std;
using namespace game_server::net;
using namespace game_server::net::clients;
using namespace game_server::game;
using namespace dcl::net;
using namespace dcl::tools;
using namespace dcl::structures;
using namespace boost::asio;
using namespace boost::asio::ip;

/* ---------------------------- DATA ---------------------------- */

Server* __server = nullptr;
io_context* __context = nullptr;

/* ---------------------------- OBJECT ---------------------------- */

Server::Server(boost::asio::io_service& service, ushort server_port, const std::string& dataCenterName, std::string loginServer, ushort loginServerPort) : TCPServer(service, server_port), m_Scheduler(std::bind(&Player::DistributeOnlineFrame)) {
	ServerData::init(server_port);
	Player::init();
	DataCenter::init();

	LoginServer server(service, dataCenterName, loginServer, loginServerPort);
	SERVER_LOGGER.log(StringBuilder("Received datacenter address: ", address_v4(server.GetDataCenterIP()).to_string(), ":", server.GetDataCenterPort()));
	m_DataCenter = make_unique<DataCenter>(service, server.GetDataCenterIP(), server.GetDataCenterPort());

	m_Scheduler.RunTaskTimer(0.02);
}

void Server::acceptHandler(Session* new_session, const boost::system::error_code& error) {
	if (error) {
		delete new_session;
		return;
	}

	Buffer<byte>& payload = new_session->read();
	byte b = payload.Read<byte>();

	if (b == 0) { // Client
		try {
			SERVER_LOGGER.log("New player connected (" + new_session->getSocket().remote_endpoint().address().to_string() + ")");
			auto player = std::make_shared<Player>(*new_session);
			player->setSelf(player);
			_players.insert(player);
			
			Player::DistributePlayerList();

		} catch (dcl::tools::Exception& e) {
			SERVER_LOGGER.log("Player connection error: " + e.what());
		}
	} else { // Error
		Console::writeln("Error while connecting to new peer: Invalid login code (", new_session->getSocket().remote_endpoint().address().to_string(), ")");
		new_session->disconnect();
	}

	delete new_session;
}

Server::~Server() {
	m_Scheduler.Join();

	for (auto player : _players)
		player->disconnect();

	_players.clear();
}

/* ---------------------------- STATIC ---------------------------- */

void Server::start(boost::asio::io_service& service, const string& dataCenterName, ushort port, std::string loginServer, ushort loginServerPort) {
	if (__server) throw Exception("Server already running!");
	__context = &service;
	__server = new Server(service, port, dataCenterName, loginServer, loginServerPort);
	service.run();
}

void Server::stop() {
	static bool stopping = false;
	if (stopping) return;
	stopping = true;

	SAFE_DELETE(__server);
	if (!__context->stopped()) __context->stop();

	stopping = false;
}
