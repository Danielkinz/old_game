#ifndef SERVER_H
#define SERVER_H

#include <dcl\net\TCPServer.h>
#include <dcl\tools\Scheduler.h>
#include <boost\asio.hpp>
#include "ServerData.h"
#include "clients\DataCenter.h"
#include "Net\ServerData.h"

namespace game_server {
	namespace net {
		typedef unsigned short ushort;

		class Server : public dcl::net::TCPServer {
		private:
			Server(boost::asio::io_service&, ushort server_port, const std::string& dataCenterName, std::string loginServer = DEFAULT_LOGIN_SERVER_NAME, ushort loginServerPort = DEFAULT_LOGIN_SERVER_PORT);

			// Event handler for new connections (TCPServer)
			void acceptHandler(dcl::net::Session*, const boost::system::error_code&);

			dcl::tools::Scheduler<std::function<void()>> m_Scheduler;

			std::unique_ptr<clients::DataCenter> m_DataCenter;

		public:
			~Server();

			static void start(boost::asio::io_service&, const std::string& dataCenterName, ushort port, std::string loginServer = DEFAULT_LOGIN_SERVER_NAME, ushort loginServerPort = DEFAULT_LOGIN_SERVER_PORT);
			static void stop();
		};
	}
}

#endif