#include "ServerData.h"

using namespace std;
using namespace boost;
using namespace dcl::tools;
using namespace game_server::net;
using namespace game_server::net::clients;

Database ServerData::database;
unordered_set<std::shared_ptr<Player>> ServerData::players;
Logger ServerData::logger("Server");

unsigned short ServerData::port;

void ServerData::init(unsigned short port) {
	ServerData::database.connect();
	ServerData::port = port;
}