#ifndef SERVER_DATA_H
#define SERVER_DATA_H

#include <boost\unordered_map.hpp>
#include <boost\unordered_set.hpp>
#include "net\clients\Player.h"
#include "net\Database.h"
#include "dcl\tools\Logger.h"

// This file is used to share constants and info between the rest of the objects in the program

namespace game_server {
	namespace net {
		// Class that allows sharing global info between all the other classes
		class ServerData final {
		public:
			static unsigned short port;
			static Database database;
			static dcl::tools::Logger logger;
			static boost::unordered_set<std::shared_ptr<game_server::net::clients::Player>> players;

			static void init(unsigned short port);
		};
	}
}

// Constants that represent the static members of the class

#define _players ServerData::players
#define _database ServerData::database
#define SERVER_LOGGER ServerData::logger
#define SERVER_PORT ServerData::port

// Constants used across the program

#define SESSION_LENGTH 64
#define GAME_SERVER_CERTIFICATE "NTkyNjc1MDViYmRhZjc4ZWFlODkxOTZlMTg5YTBkZjI3OTI0Y2QzZQ"
#define GAME_SERVER_CERTIFICATE_LENGTH strlen(GAME_SERVER_CERTIFICATE)

// Connection constants

#define DEFAULT_LOGIN_SERVER_NAME "127.0.0.1"
#define DEFAULT_LOGIN_SERVER_PORT 5300
#define DEFAULT_SERVER_PORT 5302

#endif