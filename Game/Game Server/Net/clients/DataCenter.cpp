#include "DataCenter.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"
#include "net\ServerData.h"

using namespace std;
using namespace game_server::net;
using namespace game_server::net::clients;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;
using namespace boost::asio;

boost::unordered_map<DataCenter::MessageType, DataCenter::ProtocolHandler> DataCenter::_handlers;

/* ---------------------------- STATIC ---------------------------- */

void DataCenter::init() {
	_handlers[MessageType::VALIDATION] = &DataCenter::ValidationHandler;
	_handlers[MessageType::PORT_REQ] = &DataCenter::portRequestHandler;
	_handlers[MessageType::DISCONNECT] = &DataCenter::DisconnectHandler;
	_handlers[MessageType::PLAYER_COUNT] = &DataCenter::PlayerCountHandler;
}

/* ---------------------------- OBJECT ---------------------------- */

DataCenter::DataCenter(io_service& service, unsigned int host, int port)
	: TCPClient(service, host, port), _logger("Data Center") {

	Buffer<byte> buff;
	buff.Write(1);
	TCPClient::sendData(buff);

	TCPClient::startListening();
}

DataCenter::DataCenter(DataCenter& other)
	: TCPClient(other), _logger(other._logger) {

	TCPClient::startListening();
}

DataCenter& DataCenter::operator=(DataCenter& other) {
	TCPClient::operator=(other);
	_logger = other._logger;
	TCPClient::startListening();
}

void DataCenter::sendData(const MessageType code, const string& payload) {
	Buffer<byte> buff((byte*)&code, 1);
	buff.Write((byte*)payload.data(), payload.length());
	TCPClient::sendData(buff);
}

void DataCenter::sendData(const string& payload) {
	Buffer<byte> buff((byte*)payload.data(), payload.length());
	TCPClient::sendData(buff);
}

/* ---------------------------- Handlers ---------------------------- */

bool DataCenter::handleRead(dcl::structures::Buffer<byte>& buffer, const boost::system::error_code& error) {
	Message msg;

	if (error) {
		if (error.value() == 10009) return false; // Disconnected
		else _logger.log(StringBuilder("Read error: (", error.value(), ") ", error.message()));
		disconnect();
		return false;
	}

	msg = buildReceivedMessage(buffer);
	(this->*(_handlers[(MessageType)msg->getMessageCode()]))(msg);
	return true;
}

DataCenter::Message DataCenter::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::VALIDATION:
	case MessageType::DISCONNECT:
	case MessageType::PORT_REQ:
	case MessageType::PLAYER_COUNT:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void DataCenter::DisconnectHandler(Message& msg) {
	_logger.log("disconnected");
	disconnect();
}

void DataCenter::ValidationHandler(Message& msg) {
	_logger.log("Verification request");
	sendData(GAME_SERVER_CERTIFICATE);
}

void DataCenter::portRequestHandler(Message& msg) {
	_logger.log("Port request");
	sendData(Protocol::ToString<ushort>(SERVER_PORT));
}

void DataCenter::PlayerCountHandler(Message& msg) {
	_logger.log("Player count request");
	Buffer<byte> buff;
	buff.Write<byte>(_players.size());
	sendData(buff);
}
