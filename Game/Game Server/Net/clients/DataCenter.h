#ifndef DATA_CENTER_H
#define DATA_CENTER_H

#include <boost\unordered_map.hpp>
#include "net\Database.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\net\Messages.h"
#include "dcl\net\TCPClient.h"
#include "dcl\tools\Logger.h"

namespace game_server {
	namespace net {
		namespace clients {
			class DataCenter : public dcl::net::TCPClient {
			private: // Types
				typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
				typedef std::shared_ptr<message_t> Message;
				typedef void(DataCenter::*ProtocolHandler)(Message&);

				// Protocol message codes
				enum class MessageType : byte {
					VALIDATION, PORT_REQ, PLAYER_COUNT,
					DISCONNECT = 255
				};

			private: // Static
				// Map of message codes to handler function
				static boost::unordered_map<MessageType, ProtocolHandler> _handlers;

			public: // Static
				static void init();

			private: // Object
				dcl::tools::Logger _logger;

				using TCPClient::sendData;

				void sendData(const std::string& payload);
				void sendData(const MessageType code, const std::string& payload = "");

			public: // Object
				DataCenter(boost::asio::io_service&, unsigned int host, int port);

				DataCenter(DataCenter&);
				DataCenter& operator=(DataCenter&);

			protected: // Handlers
				// Handles Read event from Session
				bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

			private: // Handlers
				// Parses the arguments from the message
				Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

				// Protocol Handler functions
				void PlayerCountHandler(Message&);
				void DisconnectHandler(Message&);
				void ValidationHandler(Message&);
				void portRequestHandler(Message&);
			};
		}
	}
}

#endif