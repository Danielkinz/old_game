#include "LoginServer.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"
#include "net\ServerData.h"

using namespace std;
using namespace game_server::net;
using namespace game_server::net::clients;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;
using namespace boost::asio;

LoginServer::LoginServer(io_service& service, const string& datacenter, const string& host, int port)
	: TCPClient(service, host, port), m_IP(0), m_Port(0) {

	Buffer<byte> buff;
	buff.Write(2);
	sendData(buff);

	buff.clear();
	read(buff);
	if (buff.Read<byte>() != (byte) MessageType::VALIDATION) {
		disconnect();
		throw Exception("Protocol error");
	}

	buff.clear();
	Protocol::WriteStringToBuffer(buff, GAME_SERVER_CERTIFICATE);
	sendData(buff);

	buff.clear();
	read(buff);
	if (buff.Read<byte>() != (byte)MessageType::NAME_REQ) {
		disconnect();
		throw ValidationException();
	}

	buff.clear();
	buff.Write<byte>(datacenter.length());
	Protocol::WriteStringToBuffer(buff, datacenter);
	sendData(buff);

	buff.clear();
	read(buff);
	switch (buff.Read<byte>()) {
	
	case (byte) MessageType::DATA_FAIL:
		throw Exception("Data center not found");
	case (byte)MessageType::DATA_RET:
		break;
	default:
		disconnect();
		throw Exception("Protocol error");
	}

	m_IP = buff.Read<uint>();
	m_Port = buff.Read<ushort>();

	disconnect();
}

uint LoginServer::GetDataCenterIP() {
	return m_IP;
}

ushort LoginServer::GetDataCenterPort() {
	return m_Port;
}
