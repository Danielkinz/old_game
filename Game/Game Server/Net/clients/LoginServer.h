#ifndef LOGIN_SERVER_H
#define LOGIN_SERVER_H

#include <boost\unordered_map.hpp>
#include "net\Database.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\net\Messages.h"
#include "dcl\net\TCPClient.h"
#include "dcl\tools\Logger.h"

namespace game_server {
	namespace net {
		namespace clients {
			typedef unsigned short ushort;
			typedef unsigned int uint;

			class LoginServer : public dcl::net::TCPClient {
			private:
				// The message codes that define the protocol
				enum class MessageType : byte {
					VALIDATION, NAME_REQ, DATA_RET, DATA_FAIL,
					DISCONNECT = 255
				};

				using TCPClient::sendData;

				uint m_IP;
				ushort m_Port;

			public:
				LoginServer() = delete;
				LoginServer(boost::asio::io_service&, const std::string& datacenter, const std::string& host = DEFAULT_LOGIN_SERVER_NAME, int port = DEFAULT_LOGIN_SERVER_PORT);
				LoginServer& operator=(LoginServer&) = delete;
				LoginServer(LoginServer&) = delete;

				uint GetDataCenterIP();
				ushort GetDataCenterPort();
			};
		}
	}
}

#endif