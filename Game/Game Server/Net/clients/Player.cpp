#include "Player.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"
#include <boost\math\constants\constants.hpp>

using namespace std;
using namespace game_server::game;
using namespace game_server::net;
using namespace game_server::net::clients;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;

boost::unordered_map<Player::MessageType, Player::ProtocolHandler> Player::_handlers;
byte Player::NEXT_ID = 0;

/* ---------------------------- STATIC ---------------------------- */

void Player::init() {
	_handlers[MessageType::ENTITY_INFO] = &Player::EntityInfoHandler;
	_handlers[MessageType::DISCONNECT] = &Player::DisconnectHandler;
}

/* ---------------------------- OBJECT ---------------------------- */

Player::Player(dcl::net::Session& session) : Session(session), _logger("Unregistered Player"), _id(NEXT_ID++) {
	Buffer<byte> buffer;
	string session_id;
	string username;
	string characterName;

	// Retrieve the session
	sendData(MessageType::SESSION);
	read(buffer);
	session_id = Protocol::ReadStringFromBuffer(buffer, buffer.Read<byte>());

	// validates sessionID
	if ((username = _database.getUserBySession(session_id)).empty()) {
		disconnect();
		throw dcl::tools::Exception("Invalid SessionID");
	}

	_logger.setName(username).log(StringBuilder("Connected from ", Session::getSocket().remote_endpoint().address().to_string()));

	// Sets the character
	sendData(MessageType::CHARACTER_NAME_REQ);
	read(buffer);

	characterName = Protocol::ReadStringFromBuffer(buffer, buffer.Read<byte>());

	if (!(_character = _database.getCharacter(characterName))) {
		disconnect();
		throw dcl::tools::Exception("Invalid Character Selected");
	}
	_logger.log(StringBuilder("Selected character \"", characterName, "\""));

	Session::startListening();
}

Player::Player(Player& other) : Session(other), _logger(other._logger) {
	this->_character = other._character;
	Session::startListening();
}

Player::~Player() {}

Player& Player::operator=(dcl::net::Session& other) {
	Session::operator=(other);
	Session::startListening();
}

Player& Player::operator=(Player& other) {
	Session::operator=(other);

	this->_character = other._character;
	this->_logger = other._logger;
	Session::startListening();
}

bool Player::operator==(Player& other) {
	return _id == other._id;
}

void Player::sendData(const MessageType code, const string& payload) {
	Buffer<byte> buff((byte*)&code, 1);
	buff.Write((byte*)payload.data(), payload.length());
	Session::sendData(buff);
}

void Player::setSelf(shared_ptr<Player>& self) {
	_self = self;
}

void Player::disconnect() {
	_players.erase(_self);
	Session::disconnect();
}

const string& Player::getUsername() const {
	if (!_character) throw Exception("Username not set");
	return _character->getUsername();
}

Character& Player::getCharacter() const {
	if (_character == nullptr) throw Exception("Character not selected");
	return (*_character);
}

const Player::EntityInfo& Player::GetLocation() const {
	return _location;
}

void Player::SendPlayerList() {
	Buffer<byte> buffer;
	buffer.Write<byte>((byte)MessageType::PLAYER_LIST);
	buffer.Write<byte>(_players.size());
	for (auto player : _players) {
		buffer.Write<byte>(player->_id);
		buffer.Write<byte>(player->_character->getCharacterName().size());
		Protocol::WriteStringToBuffer(buffer, player->_character->getCharacterName());
	}
	Session::sendData(buffer);
}

void Player::DistributeOnlineFrame() {
	Buffer<byte> buffer;
	EntityInfo info;

	buffer.Write<byte>((byte) MessageType::ONLINE_FRAME_MSG);
	buffer.Write<byte>(_players.size());

	for (auto p : _players) {
		buffer.Write<byte>(p->_id);
		buffer.Write<float>(p->_location.X);
		buffer.Write<float>(p->_location.Y);
		buffer.Write<float>(p->_location.Z);
		buffer.Write<byte>(p->_location.Yaw);
	}

	for (auto p : _players) {
		if (p->isConnected())
			reinterpret_pointer_cast<Session>(p)->sendData(buffer);
	}
}

/* ---------------------------- Handlers ---------------------------- */

bool Player::handleRead(dcl::structures::Buffer<byte>& buffer, const boost::system::error_code& error) {
	Message msg;

	if (error) {
		// Logs the error
		if (error.value() == 10009); // Disconnected message already displayed
		else if (error.value() == 2) _logger.log("disconnected");
		else _logger.log(StringBuilder("Read error: (", error.value(), ") ", error.message()));
		return false;
	}
	try {
		// Parses the message and passes it through the handlers
		msg = buildReceivedMessage(buffer);
		(this->*(_handlers[(MessageType)msg->getMessageCode()]))(msg);
		return true;
	} catch (Exception& e) {
		_logger.log(e);
		_logger.log("Disconnecting player");
		return false;
	}
}

Player::Message Player::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();
	EntityInfo info;

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::ENTITY_INFO:
		_location.X = payload.Read<float>();
		_location.Y = payload.Read<float>();
		_location.Z = payload.Read<float>();
		_location.Yaw = payload.Read<byte>();

		//arguments.push_back(EntityInfoObject(info));
		break;
	case MessageType::DISCONNECT:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void Player::EntityInfoHandler(Message& msg) {}

void Player::DisconnectHandler(Player::Message&) {
	_logger.log("disconnected");
	_players.erase(_self);
	Session::disconnect();

	DistributePlayerList();
}

void Player::DistributePlayerList() {
	for (auto it = _players.begin(); it != _players.end(); it++) {
		(*it)->SendPlayerList();
	}
}
