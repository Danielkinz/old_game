#ifndef PLAYER_H
#define PLAYER_H

#include <boost\unordered_map.hpp>
#include "Net\Database.h"
#include "Game\Character.h"
#include "dcl\net\Messages.h"
#include "dcl\net\Session.h"
#include "dcl\structures\Buffer.h"
#include "dcl\tools\Logger.h"
#include <glm\glm.hpp>

namespace game_server {
	namespace net {
		namespace clients {
			typedef unsigned char byte;

			class Player final : public dcl::net::Session {
			public:
				typedef struct entity_info_t {
					float X;
					float Y;
					float Z;
					byte Yaw;
				} EntityInfo;

			private: // Types
				typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
				typedef std::shared_ptr<message_t> Message;
				typedef void(Player::*ProtocolHandler)(Message&);

				// The protocol messages
				enum class MessageType : byte {
					SESSION, ONLINE_FRAME_MSG, ENTITY_INFO,
					ENTITY_LIST, CHARACTER_NAME_REQ, PLAYER_LIST,
					DISCONNECT = 255
				};

			private: // Static
				// List of protocol handlers
				static boost::unordered_map<MessageType, ProtocolHandler> _handlers;
				static byte NEXT_ID;

			public: // Static
				static void init();

			private: // Object
				std::shared_ptr<game::Character> _character;
				dcl::tools::Logger _logger;
				std::shared_ptr<Player> _self;

				byte _id;
				EntityInfo _location;

				void sendData(const MessageType code, const std::string& payload = "");

			public: // Object
				Player(dcl::net::Session&);
				Player(Player&);
				~Player();
				Player& operator=(dcl::net::Session&);
				Player& operator=(Player&);
				bool operator==(Player&);

				void setSelf(std::shared_ptr<Player>&);
				void disconnect();

				// Sends a list of players to the user
				void SendPlayerList();

				// Sends the info about every player's current location
				static void DistributeOnlineFrame();

				// Returns the username or an empty string
				const std::string& getUsername() const;

				// Returns the current character on which the player is playing
				game::Character& getCharacter() const;

				// Returns information about the player's location and orientation
				const EntityInfo& GetLocation() const;

				static void DistributePlayerList();

			protected: // Handlers
				// Handles the Read even from Session
				bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

			private: // Handlers
				// Parses the arguments from the received data
				Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

				// Protocol Handler functions
				void EntityInfoHandler(Message&);
				void DisconnectHandler(Message&);
			};
		}
	}
}

#endif