#include "Engine.h"

#include "EngineSettings.h"
#include "Objects\Hitbox.h"
#include "UI\Font.h"
#include "Storage.h"

#include <dcl\tools\Exception.h>
#include <time.h>

using namespace std;

Engine::Engine(const string& windowTitle, int width, int height) 
	: _window(nullptr), m_CloseRequested(false), m_WindowTitle(windowTitle), m_Width(width), m_Height(height), m_IsStopping(false) {}

Engine::~Engine() {
	Font::End();
	glfwTerminate();
}

void Engine::InitializeInternal() {
	const GLFWvidmode* mode;

	SRANDOM;

	// Init GLFW
	if (glfwInit() != GL_TRUE) {
		throw dcl::tools::Exception("GLFW Initialization failed");
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

	// Create Window
	_window = glfwCreateWindow(m_Width, m_Height, m_WindowTitle.c_str(), NULL, NULL);
	if (_window == NULL) {
		glfwTerminate();
		throw dcl::tools::Exception("Window creation failed");
	}
	glfwMakeContextCurrent(_window);

	// GLFW Setup
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		glfwTerminate();
		throw dcl::tools::Exception("GLEW initialization failed");
	}

	glClearColor(0.282f, 0.820f, 0.8f, 0.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	glfwSetWindowPos(_window, (mode->width - m_Width) / 2, (mode->height - m_Height) / 2);

	// IO Setup
	Window::Initialize(this->_window);
	Mouse::Initialize(this->_window);
	Keyboard::Initinitalize(this->_window);

	Mouse::SetMousePos(Window::GetWidth() / 2, Window::GetHeight() / 2);

	glGetError();

	Renderer.Initialize();
	Texture::LoadDefaultTexture();
	Font::Init();
	//CubeHitbox::SetHitboxModel(Model::LoadModel(EngineSettings::HitboxModelPath));
	EngineStorage::Renderer = &Renderer;

	cout << "Engine initialized successfuly" << endl;

	glGetError();
}

void Engine::Render() {
	glClearColor(0.282f, 0.820f, 0.8f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	Renderer.Render();

	glfwSwapBuffers(_window);
}

bool Engine::ShouldClose() {
	return glfwWindowShouldClose(_window) != 0;
}

void Engine::Run() {
	float now;

	cout << "Initializing..." << endl;

	try { 
		InitializeInternal();
		Initialize();
	} catch (dcl::tools::Exception& e) {
		cout << "(" << e.getFileName() << ":" << e.getLineNumber() << ") " << e.what() << endl;
		getchar();
		return;
	}

	cout << "Initialization successful" << endl;

	do {
		now = (float)glfwGetTime();
		_dt = now - _lastTime;
		_lastTime = now;
		glfwPollEvents();

		EngineStorage::Update(_dt);

		Update();

		Render();
	} while (!glfwWindowShouldClose(_window) && !m_CloseRequested);

	cout << "Stopping..." << endl;

	Stop();
	EngineStorage::DestroyAll();

	cout << "Stopped" << endl;
}

float Engine::GetDT() const {
	return _dt;
}

float Engine::GetTime() const {
	return (float)glfwGetTime();
}

void Engine::Close() {
	m_CloseRequested = true;
}

bool Engine::IsStopping() const {
	return m_IsStopping;
}
