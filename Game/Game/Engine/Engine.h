#ifndef ENGINE_H
#define ENGINE_H

#include <iostream>
#include "GL\glew.h"
#include "GLFW\glfw3.h"
#include "IO\Keyboard.h"
#include "IO\Mouse.h"
#include "IO\Window.h"
#include "Renderer.h"
#include "Storage.h"
#include "StepTimer.h"

	// The main class that handles the graphical side of the game
	// Must not be passed by value!
class Engine {
private:
	GLFWwindow* _window;
	bool m_CloseRequested;
	std::string m_WindowTitle;
	int m_Width;
	int m_Height;
	bool m_IsStopping;

	void Render();
	void InitializeInternal();

	bool ShouldClose();

	float _lastTime;
	float _dt;

protected:
	virtual void Initialize() = 0;
	virtual void Update() = 0;
	virtual void Stop() = 0;

public:
	Engine(const std::string& windowTitle, int width, int height);
	virtual ~Engine();

	void Run();
	void Close();
	
	bool IsStopping() const;

	// Returns the amount of time between the last frame and this one
	float GetDT() const;

	// Returns the amount of time the engine is running
	float GetTime() const;

	/*virtual void OnKeyStateChange();
	virtual void OnKeyPressed();
	virtual void OnMouseClick();
	virtual void OnMouseMove();
	virtual void OnMouseScroll();*/

	SceneRenderer Renderer;
	//StepTimer Timer;
};

#endif
