#ifndef ENGINE_SETTINGS
#define ENGINE_SETTINGS

#include <glm\glm.hpp>

#define SETTING static constexpr

class EngineSettings {
public:
	//SETTING bool HitboxRenderingEnabled = true;
	SETTING const char* HitboxModelPath = "Resources/Hitbox.obj";
	SETTING float DefaultScreenTextScale = 1.0f;

	SETTING glm::vec3 DefaultScreenTextColor = glm::vec3(0.0f, 0.0f, 0.0f);
	SETTING const char* DefaultFontPath = "Resources/Fonts/arial.ttf";
	SETTING float DefaultFontScale = 1;
};

#endif
