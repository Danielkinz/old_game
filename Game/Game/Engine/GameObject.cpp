#include "GameObject.h"
#include "Storage.h"

using namespace std;

GameObject::~GameObject() {}

void* GameObject::operator new(size_t size) {
	void* object = ::operator new(size);
	EngineStorage::RegisterObject(static_cast<GameObject*>(object));
	return object;
}

void GameObject::operator delete(void* ptr) {
	EngineStorage::UnregisterObject(static_cast<GameObject*>(ptr));
	::operator delete(ptr);
}

void GameObject::Update(float dt) {}