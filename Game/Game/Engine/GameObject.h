#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <iostream>
#include "StepTimer.h"

class GameObject {
public:
	virtual ~GameObject();

	virtual void Update(float dt);

	static void* operator new(std::size_t size);
	static void* operator new[](std::size_t size);
	static void operator delete(void* ptr) noexcept;
	static void operator delete[](void* ptr) noexcept;

	template<typename T>
	bool isA() const;
};

template<typename T>
bool GameObject::isA() const {
	return dynamic_cast<T*>(this) != nullptr;
}

#endif