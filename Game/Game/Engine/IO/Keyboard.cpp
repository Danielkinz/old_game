#include "Keyboard.h"

bool Keyboard::m_Keys[GLFW_KEY_LAST] = { 0 };

void Keyboard::Initinitalize(GLFWwindow* window) {
	glfwSetKeyCallback(window, Keyboard::KeyCallback);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
}

void Keyboard::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mode) {
	if (key < 0) return;
	m_Keys[key] = action != GLFW_RELEASE;
}

bool Keyboard::IsKeyDown(int key) {
	return m_Keys[key];
}

bool Keyboard::IsKeyUp(int key) {
	return !m_Keys[key];
}
