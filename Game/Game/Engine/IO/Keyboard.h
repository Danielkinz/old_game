#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "GL\glew.h"
#include "GLFW\glfw3.h"

class Keyboard {
public:
	static void Initinitalize(GLFWwindow* window);
	static bool IsKeyDown(int key);
	static bool IsKeyUp(int key);

private:
	static bool m_Keys[];
	static void KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
};

#endif
