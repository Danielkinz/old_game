#include "Mouse.h"

double Mouse::m_X = 0;
double Mouse::m_Y = 0;
double Mouse::m_ScrollX = 0;
double Mouse::m_ScrollY = 0;
bool Mouse::m_Buttons[GLFW_MOUSE_BUTTON_LAST] = { 0 };
GLFWwindow* Mouse::m_Window = nullptr;

MouseClickEvent::MouseClickEvent(double x, double y, MouseButton button, MouseClickType type) 
	: m_X(x), m_Y(y), m_MouseButton(button), m_ClickType(type) {}

MouseDragEvent::MouseDragEvent(double fromX, double fromY, double toX, double toY, MouseButton button)
	: m_FromX(fromX), m_FromY(fromY), m_ToX(toX), m_ToY(toY), m_MouseButton(button) {}

void Mouse::Initialize(GLFWwindow* window) {
	m_Window = window;
	glfwSetMouseButtonCallback(m_Window, Mouse::MouseButtonCallback);
	glfwSetCursorPosCallback(m_Window, Mouse::MousePosCallback);
	glfwSetScrollCallback(m_Window, Mouse::MouseScrollCallback);
}

void Mouse::MousePosCallback(GLFWwindow* window, double x, double y) {
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);

	m_X = x;
	m_Y = y;
}

void Mouse::MouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
	if (button < 0 || window != m_Window) return;
	m_Buttons[button] = action;
}

void Mouse::MouseScrollCallback(GLFWwindow* window, double xoffset, double yoffset) {
	m_ScrollX = xoffset;
	m_ScrollY = yoffset;
}

double Mouse::GetMouseX() {
	return m_X;
}

double Mouse::GetMouseY() {
	return m_Y;
}

void Mouse::GetMousePos(double& x, double& y) {
	x = m_X;
	y = m_Y;
}

void Mouse::SetMousePos(double x, double y) {
	glfwSetCursorPos(m_Window, x, y);
	m_X = x;
	m_Y = y;
}

bool Mouse::IsButtonDown(int button) {
	return m_Buttons[button];
}

bool Mouse::IsButtonUp(int button) {
	return !m_Buttons[button];
}

void Mouse::SetCursorType(int cursorType) {
	glfwSetInputMode(m_Window, GLFW_CURSOR, cursorType);
}

int Mouse::GetCursorType() {
	return glfwGetInputMode(m_Window, GLFW_CURSOR);
}

double Mouse::GetScrollX(){
	return m_ScrollX;
}

double Mouse::GetScrollY(){
	return m_ScrollY;
}

void Mouse::GetScroll(double& x, double& y){
	x = m_ScrollX;
	y = m_ScrollY;
}

void Mouse::SetScroll(double x, double y) {
	m_ScrollX = x;
	m_ScrollY = y;
}
