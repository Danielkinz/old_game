#ifndef MOUSE_H
#define MOUSE_H

#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include <glm\glm.hpp>

enum class MouseClickType {
	Release = GLFW_RELEASE,
	Press = GLFW_PRESS
};

enum class MouseButton {
	Button1 = GLFW_MOUSE_BUTTON_1,
	Button2 = GLFW_MOUSE_BUTTON_2,
	Button3 = GLFW_MOUSE_BUTTON_3,
	Button4 = GLFW_MOUSE_BUTTON_4,
	Button5 = GLFW_MOUSE_BUTTON_5,
	Button6 = GLFW_MOUSE_BUTTON_6,
	Button7 = GLFW_MOUSE_BUTTON_7,
	Button8 = GLFW_MOUSE_BUTTON_8,
	Left = GLFW_MOUSE_BUTTON_LEFT,
	Middle = GLFW_MOUSE_BUTTON_MIDDLE,
	Right = GLFW_MOUSE_BUTTON_RIGHT
};

class MouseClickEvent {
public:
	MouseClickEvent(double x, double y, MouseButton button, MouseClickType type);
	double GetX() { return m_X; };
	double GetY() { return m_Y; };
	MouseButton GetMouseButton() { return m_MouseButton; };
	MouseClickType GetClickType() { return m_ClickType; };
private:
	double m_X;
	double m_Y;
	MouseButton m_MouseButton;
	MouseClickType m_ClickType;
};

class MouseDragEvent {
public:
	MouseDragEvent(double fromX, double fromY, double toX, double toY, MouseButton button);
	double GetFromX() { return m_FromX; };
	double GetFromY() { return m_FromY; };
	double GetToX() { return m_ToX; };
	double GetToY() { return m_ToY; };
	glm::vec2 GetFrom() { return glm::vec2(m_FromX, m_FromY); };
	glm::vec2 GetTo() { return glm::vec2(m_ToX, m_ToY); };
	MouseButton GetButton() { return m_MouseButton; };

private:
	double m_FromX;
	double m_FromY;
	double m_ToX;
	double m_ToY;
	MouseButton m_MouseButton;
};

class Mouse {
private:
	static double m_X;
	static double m_Y;
	static double m_ScrollX;
	static double m_ScrollY;
	static bool m_Buttons[];
	static GLFWwindow* m_Window;

	static void MousePosCallback(GLFWwindow* window, double x, double y);
	static void MouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
	static void MouseScrollCallback(GLFWwindow* window, double xoffset, double yoffset);
public:
	static void Initialize(GLFWwindow* window);
	static void SetCursorType(int cursorType);
	static int GetCursorType();

	static double GetMouseX();
	static double GetMouseY();
	static void GetMousePos(double& x, double& y);
	static void SetMousePos(double x, double y);

	static double GetScrollX();
	static double GetScrollY();
	static void GetScroll(double& x, double& y);
	static void SetScroll(double x, double y);
	
	static bool IsButtonDown(int button);
	static bool IsButtonUp(int button);
};

#endif
