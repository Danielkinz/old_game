#include "Window.h"

int Window::m_Width = 0;
int Window::m_Height = 0;
bool Window::m_IsFocused = true;

void Window::Initialize(GLFWwindow* window) {
	glfwSetWindowSizeCallback(window, Window::WindowResizeCallback);
	glfwSetWindowFocusCallback(window, Window::WindowFocusCallback);
	glfwGetWindowSize(window, &m_Width, &m_Height);
}

void Window::WindowFocusCallback(GLFWwindow* window, int focused) {
	m_IsFocused = focused;
}

void Window::WindowResizeCallback(GLFWwindow* window, int width, int height) {
	m_Width = width;
	m_Height = height;
	glViewport(0, 0, width, height);
}

int Window::GetWidth() {
	return m_Width;
}

int Window::GetHeight() {
	return m_Height;
}

void Window::GetWindowSize(int& width, int& height) {
	width = m_Width;
	height = m_Height;
}

bool Window::IsSelected() {
	return m_IsFocused;
}
