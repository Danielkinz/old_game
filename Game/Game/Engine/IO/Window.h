#ifndef WINDOW_H
#define WINDOW_H

#include "GL\glew.h"
#include "GLFW\glfw3.h"

class Window {
private:
	static int m_Height;
	static int m_Width;
	static GLFWwindow* m_Window;
	static bool m_IsFocused;

	static void WindowResizeCallback(GLFWwindow* window, int width, int height);
	static void WindowFocusCallback(GLFWwindow* window, int focused);
public:
	static void Initialize(GLFWwindow* window);
	static int GetHeight();
	static int GetWidth();
	static void GetWindowSize(int& width, int& height);
	static bool IsSelected();
};

#endif
