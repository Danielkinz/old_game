#include "Math.h"
#include <limits>
#include <dcl\tools\Exception.h>

using namespace glm;
using namespace math;

mat4 math::scaleTransform(float scale) {
	return glm::scale(mat4(1.0f), vec3(scale));
}

mat4 math::scaleTransform(vec3 scale) {
	return glm::scale(mat4(1.0f), scale);
}

mat4 math::scaleTransform(float scaleX, float scaleY, float scaleZ) {
	return scale(fmat4(1.0f), vec3(scaleX, scaleY, scaleZ));
}

mat4 math::scaleTransform(aiVector3D scale) {
	return glm::scale(mat4(1.0), vec3(scale.x, scale.y, scale.z));
}

mat4 math::rotateTransform(vec3 rotate) {
	return toMat4(quat(rotate));
}

mat4 math::rotateTransform(const aiQuaternion& aiQuat) {
	return toMat4(quat{ aiQuat.x, aiQuat.y, aiQuat.z, aiQuat.w });
}

mat4 math::rotateTransform(float rotateX, float rotateY, float rotateZ) {
	return toMat4(quat(vec3(rotateX, rotateY, rotateZ)));
}

mat4 math::rotateTransform(const quat& angle) {
	return toMat4(angle);
}

mat4 math::translationTransform(vec3 translation) {
	return translate(fmat4(1.0), translation);
}

mat4 math::translationTransform(float x, float y, float z) {
	return translate(fmat4(1.0), fvec3(x, y, z));
}

mat4 math::translationTransform(const aiVector3D translation) {
	return translate(fmat4(1.0), vec3(translation.x, translation.y, translation.z));
}

mat4 math::convertMatrix(const aiMatrix4x4& aiMat) {
	return {
		aiMat.a1, aiMat.b1, aiMat.c1, aiMat.d1,
		aiMat.a2, aiMat.b2, aiMat.c2, aiMat.d2,
		aiMat.a3, aiMat.b3, aiMat.c3, aiMat.d3,
		aiMat.a4, aiMat.b4, aiMat.c4, aiMat.d4
	};
}

mat3 math::convertMatrix(const aiMatrix3x3& aiMat) {
	return {
		aiMat.a1, aiMat.b1, aiMat.c1,
		aiMat.a2, aiMat.b2, aiMat.c2,
		aiMat.a3, aiMat.b3, aiMat.c3
	};
}

quat math::convertQuat(const aiQuaternion& aiQuat) {
	return {
		aiQuat.w,
		aiQuat.x,
		aiQuat.y,
		aiQuat.z
	};
}

quat math::interpolate(const quat& pStart, const quat& pEnd, float pFactor) {
	// calc cosine theta
	float cosom = pStart.x * pEnd.x + pStart.y * pEnd.y + pStart.z * pEnd.z + pStart.w * pEnd.w;

	// adjust signs (if necessary)
	quat end = pEnd;
	if (cosom < 0.0f) {
		cosom = -cosom;
		end.x = -end.x;   // Reverse all signs
		end.y = -end.y;
		end.z = -end.z;
		end.w = -end.w;
	}

	// Calculate coefficients
	float sclp, sclq;
	if (1.0f - cosom > 0.0001f) // 0.0001 -> some epsillon
	{
		// Standard case (slerp)
		float omega, sinom;
		omega = acos(cosom); // extract theta from dot product's cos theta
		sinom = sin(omega);
		sclp = sin((1.0f - pFactor) * omega) / sinom;
		sclq = sin(pFactor * omega) / sinom;
	} else {
		// Very close, do linear interp (because it's faster)
		sclp = 1.0f - pFactor;
		sclq = pFactor;
	}

	return {
		sclp * pStart.w + sclq * end.w,
		sclp * pStart.x + sclq * end.x,
		sclp * pStart.y + sclq * end.y,
		sclp * pStart.z + sclq * end.z
	};
}

glm::mat4 math::worldMatrix(glm::vec3 Location, glm::vec3 Rotation, glm::vec3 Scale) {
	mat4 matrix(1.0);
	mat4 ScaleM = scale(matrix, Scale);
	mat4 RotateM = toMat4(quat(Rotation)) * matrix;
	mat4 TranslateM = translate(matrix, Location);
	return TranslateM * RotateM * ScaleM;
}

glm::mat4 math::worldMatrix(glm::vec3 Location, glm::vec3 Rotation, float Scale) {
	return worldMatrix(Location, Rotation, vec3(Scale, Scale, Scale));
}

bool math::overlaps(float min1, float max1, float min2, float max2) {
	return isBetweenOrdered(min2, min1, max1) || isBetweenOrdered(min1, min2, max2);
}

inline bool math::isBetweenOrdered(float val, float lowerBound, float upperBound) {
	return lowerBound <= val && val <= upperBound;
}

float math::barryCentric(vec3 p1, vec3 p2, vec3 p3, vec2 pos) {
	float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
	float l1 = ((p2.z - p3.z) * (pos.x - p3.x) + (p3.x - p2.x) * (pos.y - p3.z)) / det;
	float l2 = ((p3.z - p1.z) * (pos.x - p3.x) + (p1.x - p3.x) * (pos.y - p3.z)) / det;
	float l3 = 1.0f - l1 - l2;
	return l1 * p1.y + l2 * p2.y + l3 * p3.y;
}