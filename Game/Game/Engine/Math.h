#ifndef MATH_H
#define MATH_H

#define GLM_ENABLE_EXPERIMENTAL
#include <iterator>
#include <vector>
#include <glm\common.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\quaternion.hpp>
#include <glm\gtx\quaternion.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <assimp\postprocess.h>

typedef unsigned int uint;

namespace math {
	glm::mat4 scaleTransform(float scale);
	glm::mat4 scaleTransform(glm::vec3 scale);
	glm::mat4 scaleTransform(aiVector3D scale);
	glm::mat4 scaleTransform(float scaleX, float scaleY, float scaleZ);
	glm::mat4 rotateTransform(glm::vec3 rotate);
	glm::mat4 rotateTransform(float rotateX, float rotateY, float rotateZ);
	glm::mat4 rotateTransform(const glm::quat& quat);
	glm::mat4 rotateTransform(const aiQuaternion& quat);
	glm::mat4 translationTransform(float x, float y, float z);
	glm::mat4 translationTransform(glm::vec3 translation);
	glm::mat4 translationTransform(const aiVector3D translation);

	glm::mat4 convertMatrix(const aiMatrix4x4& aiMat);
	glm::mat3 convertMatrix(const aiMatrix3x3& aiMat);
	glm::quat convertQuat(const aiQuaternion& aiQuat);

	glm::quat interpolate(const glm::quat& pStart, const glm::quat& pEnd, float pFactor);

	glm::mat4 worldMatrix(glm::vec3 Location, glm::vec3 Rotation, glm::vec3 Scale);
	glm::mat4 worldMatrix(glm::vec3 Location, glm::vec3 Rotation, float Scale);

	bool overlaps(float min1, float max1, float min2, float max2);
	bool isBetweenOrdered(float val, float lowerBound, float upperBound);

	float barryCentric(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec2 pos);

	template<class T>
	T sum(T* begin, T* end) {
		T s = 0;
		for (T* t = begin; t != end; t++) s += *t;
		return s;
	}
}

#endif
