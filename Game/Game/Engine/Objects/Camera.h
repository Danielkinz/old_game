#ifndef CAMERA_H
#define CAMERA_H

#include "WorldObject.h"

class Camera : public WorldObject {
public:
	Camera();
	virtual ~Camera();
	virtual glm::mat4 GetViewMatrix() const  = 0;
};

#endif
