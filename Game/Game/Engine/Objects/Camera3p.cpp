#include "Camera3p.h"

using namespace glm;

Camera3p::Camera3p(Entity* target, float distance, vec2 direction) : 
	m_pTarget(target), m_Distance(distance), m_Direction(direction) {}

Camera3p::~Camera3p() {}

Camera3p::Camera3p(Camera3p& other) {
	m_pTarget = other.m_pTarget;
	m_Distance = other.m_Distance;
	m_Direction = other.m_Direction;
}

Camera3p& Camera3p::operator=(const Camera3p& other) {
	m_pTarget = other.m_pTarget;
	m_Distance = other.m_Distance;
	m_Direction = other.m_Direction;
	return *this;
}

Entity* Camera3p::GetTarget() const {
	return this->m_pTarget;
}

void Camera3p::SetTarget(Entity* entity) {
	this->m_pTarget = entity;
}

void Camera3p::SetDistance(float distance) {
	this->m_Distance = distance;
}

void Camera3p::AddDistance(float distance) {
	this->m_Distance = clamp(m_Distance + distance, 10.0f, 60.0f);
}

float Camera3p::GetDistance() const {
	return this->m_Distance;
}

void Camera3p::SetDirection(vec2 direction) {
	this->m_Direction = direction;
}

void Camera3p::AddDirection(vec2 direction) {
	this->m_Direction.x += direction.x;
	this->m_Direction.y = clamp(direction.y + m_Direction.y, -50.0f, 89.0f);
}

void Camera3p::AddDirection(float dx, float dy) {
	this->m_Direction.x += dx;
	this->m_Direction.y = clamp(dy + m_Direction.y, -50.0f, 89.0f);
}

vec2 Camera3p::GetDirection() const {
	return this->m_Direction;
}

mat4 Camera3p::GetViewMatrix() const {
	float height = (m_pTarget->GetModel()->GetMaxCoords().y - m_pTarget->GetModel()->GetMinCoords().y);
	vec3 targetLocation = m_pTarget->GetCenter();
	targetLocation.y += height * 0.20;
	return lookAt(GetLocation() + targetLocation, targetLocation, vec3(0, 1, 0));
}

vec3 Camera3p::GetLocation() const {
	// polar to cartesian
	vec2 direction = vec2(m_Direction.x - 90.0f, m_Direction.y);
	return {
		this->m_Distance * cos(radians(direction.x)) * cos(radians(direction.y)),
		this->m_Distance * sin(radians(direction.y)),
		this->m_Distance * sin(radians(direction.x)) * cos(radians(direction.y))
	};
}

