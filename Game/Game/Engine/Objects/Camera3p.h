#ifndef CAMERA3P_H
#define CAMERA3P_H

#include "Camera.h"
#include "Entity.h"

// 3rd person camera
class Camera3p : public Camera {
private:
	Entity* m_pTarget;
	float m_Distance;
	glm::vec2 m_Direction;

public:
	Camera3p(Entity* target = nullptr, float distance = 50, glm::vec2 direction = glm::vec2(0.0, 30.0));
	Camera3p(Camera3p& other);
	Camera3p& operator=(const Camera3p& other);
	virtual ~Camera3p();

	Entity* GetTarget() const;
	void SetTarget(Entity* ent);

	void SetDistance(float distance);
	void AddDistance(float distance);
	float GetDistance() const;

	void SetDirection(glm::vec2 direction);
	void AddDirection(glm::vec2 direction);
	void AddDirection(float dx, float dy);
	glm::vec2 GetDirection() const;

	virtual glm::vec3 GetLocation() const;

	virtual glm::mat4 GetViewMatrix() const;
};

#endif