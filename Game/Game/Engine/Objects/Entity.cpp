#include "Entity.h"

using namespace glm;

Entity::Entity(Model* pModel)
	: m_Model(pModel), m_Location(0, 0, 0), m_Rotation(0, 0, 0),
	m_Scale(1.0), m_AnimationTime(0), m_RunningAnimationID(0) /*, m_Hitbox(pModel) */ {}

Entity::Entity(Model* pModel, vec3 position, vec3 rotation, float scale)
	: m_Model(pModel), m_Location(position), m_Rotation(rotation), 
	m_Scale(scale), m_AnimationTime(0), m_RunningAnimationID(0) /*, m_Hitbox(pModel) */ {}

Entity::Entity(Model* pModel, float x, float y, float z, glm::vec3 rotation, float scale) 
	: m_Model(pModel), m_Location(x,y,z), m_Rotation(rotation), 
	m_Scale(scale), m_AnimationTime(0), m_RunningAnimationID(0) /*, m_Hitbox(pModel) */ {}

Entity::Entity(Model* pModel, glm::vec3 position, float yaw, float pitch, float roll, float scale) 
	: m_Model(pModel), m_Location(position), m_Rotation(yaw,pitch,roll),
	m_Scale(scale), m_AnimationTime(0), m_RunningAnimationID(0) /*, m_Hitbox(pModel) */ {}

Entity::Entity(Model* pModel, float x, float y, float z, float yaw, float pitch, float roll, float scale)
	: m_Model(pModel), m_Location(x, y, z), m_Rotation(yaw,pitch,roll),
	m_Scale(scale), m_AnimationTime(0), m_RunningAnimationID(0) /*, m_Hitbox(pModel) */ {}


Entity::~Entity() {}

inline mat4 Entity::CalculateWorldMatrix() const {
	return math::worldMatrix(m_Location, m_Rotation, m_Scale);
}

void Entity::SetModel(Model* model) {
	m_Model = model;
	//m_Hitbox = CubeHitbox(model);
}

Model* Entity::GetModel() const {
	return m_Model;
}

vec3 Entity::GetLocation() const {
	return m_Location;
}

vec3& Entity::GetLocation() {
	return m_Location;
}

void Entity::SetLocation(float x, float y, float z) {
	m_Location = vec3(x, y, z);
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0,0,0), m_Scale));
}

void Entity::SetLocation(const glm::vec3& location) {
	m_Location = location;
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

void Entity::AddLocation(float x, float y, float z) {
	m_Location += vec3(x, y, z);
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

void Entity::AddLocation(const glm::vec3& location) {
	m_Location += location;
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

void Entity::SetRotation(vec3 rotation) {
	m_Rotation = rotation;
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

void Entity::SetRotation(float rx, float ry, float rz) {
	m_Rotation = vec3(rx, ry, rz);
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

void Entity::AddRotation(glm::vec3 rotation) {
	m_Rotation += rotation;
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

void Entity::AddRotation(float rx, float ry, float rz) {
	m_Rotation += vec3(rx, ry, rz);
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

glm::vec3& Entity::GetRotation() {
	return m_Rotation;
}

vec3 Entity::GetRotation() const {
	return m_Rotation;
}

void Entity::SetScale(float scale) {
	m_Scale = scale;
	//m_Hitbox.Transform(math::worldMatrix(m_Location, vec3(0, 0, 0), m_Scale));
}

float Entity::GetScale() const {
	return m_Scale;
}

void Entity::SetAcceleration(const glm::vec3& acceleration) {
	m_Acceleration = acceleration;
}

void Entity::SetAcceleration(float x, float y, float z) {
	m_Acceleration = vec3(x, y, z);
}

void Entity::AddAcceleration(const glm::vec3& acceleration) {
	m_Acceleration += acceleration;
}

void Entity::AddAcceleration(float x, float y, float z) {
	m_Acceleration += vec3(x, y, z);
}

glm::vec3 Entity::GetAcceleration() const {
	return m_Acceleration;
}

glm::vec3& Entity::GetAcceleration() {
	return m_Acceleration;
}

void Entity::SetSpeed(const glm::vec3& speed) {
	m_Speed = speed;
}

void Entity::SetSpeed(float x, float y, float z) {
	m_Speed = vec3(x, y, z);
}

void Entity::AddSpeed(const glm::vec3& speed) {
	m_Speed += speed;
}

void Entity::AddSpeed(float x, float y, float z) {
	m_Speed += vec3(x, y, z);
}

glm::vec3 Entity::GetSpeed() const {
	return m_Speed;
}

glm::vec3& Entity::GetSpeed() {
	return m_Speed;
}

vec3 Entity::GetCenter() const {
	vec4 loc = vec4(m_Model->GetCenter(), 1.0) * (CalculateWorldMatrix());
	return m_Location + vec3(loc.x, loc.y, loc.z);
}

float Entity::GetAnimationTime() const {
	return m_AnimationTime;
}

void Entity::SetRunningAnimation(uint id, bool ignoreCurrent) {
	if (!ignoreCurrent && m_RunningAnimationID != 0) return; // Prevents switching animations in the middle
	m_RunningAnimationID = id;
	m_AnimationTime = 0;
}

void Entity::Update(float dt) {
	if (m_Model->HasAnimation()) {
		m_AnimationTime += dt;
		Animation anim = dynamic_cast<AnimatedModel*>(m_Model)->GetAnimation(m_RunningAnimationID);

		// Reset if the animation ended
		if (m_AnimationTime >= anim.Duration / anim.TicksPerSecond) {
			m_RunningAnimationID = 0;
			m_AnimationTime = 0;
		}
	}
}

uint Entity::GetAnimationID() const {
	return m_RunningAnimationID;
}
/*
const CubeHitbox& Entity::GetHitbox() const {
	return m_Hitbox;
}*/
