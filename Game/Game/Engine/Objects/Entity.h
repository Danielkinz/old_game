#ifndef ENTITY_H
#define ENTITY_H

#include "WorldObject.h"
#include "..\Math.h"
#include "Models\StaticModel.h"
#include "Models\AnimatedModel.h"
#include "Hitbox.h"

// Holds data about a single entity
class Entity : public WorldObject {
protected:
	Model* m_Model;
	glm::vec3 m_Rotation;
	float m_Scale;
	float m_AnimationTime;
	uint m_RunningAnimationID;
	glm::vec3 m_Location;
	glm::vec3 m_Speed;
	glm::vec3 m_Acceleration;

	//CubeHitbox m_Hitbox;

public:
	Entity(Model* pModel);
	Entity(Model* pModel, glm::vec3 position, glm::vec3 rotation, float scale = 1);
	Entity(Model* pModel, float x, float y, float z, glm::vec3 rotation, float scale = 1);
	Entity(Model* pModel, glm::vec3 position, float yaw, float pitch, float roll, float scale = 1);
	Entity(Model* pModel, float x, float y, float z, float yaw, float pitch, float roll, float scale = 1);


	virtual ~Entity();

	// utils
	inline glm::mat4 CalculateWorldMatrix() const;

	// Getters and setters
	void SetModel(Model* model);
	Model* GetModel() const;

	virtual glm::vec3 GetLocation() const;
	glm::vec3& GetLocation();
	void SetLocation(float x, float y, float z);
	void SetLocation(const glm::vec3& location);
	void AddLocation(float x, float y, float z);
	void AddLocation(const glm::vec3& location);

	void SetRotation(glm::vec3 rotation);
	void SetRotation(float rx, float ry, float rz);
	void AddRotation(glm::vec3 rotation);
	void AddRotation(float rx, float ry, float rz);
	glm::vec3 GetRotation() const;
	glm::vec3& GetRotation();

	void SetScale(float scale);
	float GetScale() const;

	void SetAcceleration(const glm::vec3& acceleration);
	void SetAcceleration(float x, float y, float z);
	void AddAcceleration(const glm::vec3& acceleration);
	void AddAcceleration(float x, float y, float z);
	glm::vec3 GetAcceleration() const;
	glm::vec3& GetAcceleration();

	void SetSpeed(const glm::vec3& speed);
	void SetSpeed(float x, float y, float z);
	void AddSpeed(const glm::vec3& speed);
	void AddSpeed(float x, float y, float z);
	glm::vec3 GetSpeed() const;
	glm::vec3& GetSpeed();

	glm::vec3 GetCenter() const;

	void SetRunningAnimation(uint id, bool ignoreCurrent = false);
	float GetAnimationTime() const;
	uint GetAnimationID() const;

	virtual void Update(float dt);

	//const CubeHitbox& GetHitbox() const;
};

#endif
