/*
#include "Hitbox.h"
#include "..\EngineSettings.h"
#include "..\Storage.h"
#include <string>
#include "..\IO\Window.h"

using namespace std;
using namespace glm;
using namespace math;

Model* CubeHitbox::HitboxModel = nullptr;

CubeHitbox::CubeHitbox(Model* model) {
	vec3 minV = oMin = model->GetMinCoords();
	vec3 maxV = oMax = model->GetMaxCoords();

	// Convert to 6 faces with 4 vertices each
	m_OriginalBox = { {
		vec3(minV.x, minV.y, minV.z),
		vec3(minV.x, minV.y, maxV.z),
		vec3(maxV.x, minV.y, maxV.z),
		vec3(maxV.x, minV.y, minV.z)
	},{
		vec3(minV.x, minV.y, minV.z),
		vec3(minV.x, minV.y, maxV.z),
		vec3(minV.x, maxV.y, maxV.z),
		vec3(minV.x, maxV.y, minV.z)
	}, {
		vec3(minV.x, minV.y, maxV.z),
		vec3(maxV.x, minV.y, maxV.z),
		vec3(maxV.x, maxV.y, maxV.z),
		vec3(minV.x, maxV.y, maxV.z),
	},{
		vec3(maxV.x, minV.y, minV.z),
		vec3(maxV.x, minV.y, maxV.z),
		vec3(maxV.x, maxV.y, maxV.z),
		vec3(maxV.x, maxV.y, minV.z)
	},{
		vec3(minV.x, minV.y, minV.z),
		vec3(maxV.x, minV.y, minV.z),
		vec3(maxV.x, maxV.y, minV.z),
		vec3(minV.x, maxV.y, minV.z)
	},{
		vec3(minV.x, maxV.y, minV.z),
		vec3(minV.x, maxV.y, maxV.z),
		vec3(maxV.x, maxV.y, maxV.z),
		vec3(maxV.x, maxV.y, minV.z)
	}};

	Transform(mat4(1.0));
}

void CubeHitbox::Transform(mat4& WorldMatrix) {
	m_LastTransformation = WorldMatrix;

	m_MovedHitbox.clear();
	Face modified;
	vec4 tempPos;

	mMin = WorldMatrix * vec4(oMin, 1.0);
	mMax = WorldMatrix * vec4(oMax, 1.0);

	for each (Face face in m_OriginalBox) {
		modified.clear();
		for each (vec3 pos in face) {
			modified.push_back(WorldMatrix * vec4(pos, 1.0));
		}
		m_MovedHitbox.push_back(modified);
	}
}

bool CubeHitbox::Intersects(const CubeHitbox& other) const {
	return (mMin.x <= other.mMax.x && other.mMin.x <= mMax.x) &&
		(mMin.y <= other.mMax.y && other.mMin.y <= mMax.y) &&
		(mMin.z <= other.mMax.z && other.mMin.z <= mMax.z);
}

bool CubeHitbox::Intersects(const CubeHitbox& hitbox1, const CubeHitbox& hitbox2) {
	return hitbox1.Intersects(hitbox2);
}

void CubeHitbox::SetHitboxModel(Model* model) {
	HitboxModel = model;
}

void CubeHitbox::Render() {
	if (HitboxModel) HitboxModel->Render();
}

void CubeHitbox::Print(float x) const {
	float nextY = Window::GetHeight() - 80;

	for each (auto face in m_MovedHitbox) {
		for each (auto vertex in face) {
			float nextX = x;
			nextX = EngineStorage::Renderer->AddText(to_string(vertex.x), nextX, nextY, 0.5f);
			nextX = EngineStorage::Renderer->AddText(", ", nextX, nextY, 0.5f);
			nextX = EngineStorage::Renderer->AddText(to_string(vertex.y), nextX, nextY, 0.5f);
			nextX = EngineStorage::Renderer->AddText(", ", nextX, nextY, 0.5f);
			         EngineStorage::Renderer->AddText(to_string(vertex.z), nextX, nextY, 0.5f);
			nextY -= 30;
		}
		nextY -= 30;
	}
}*/