#include "Light.h"

using namespace glm;

Light::Light() : m_Location(0,0,0), m_Color(0, 0, 0), m_Power(1) {}

Light::Light(vec3 location, vec3 color, float power) 
	: m_Location(location), m_Color(color), m_Power(power) {}

Light::Light(glm::vec3 position, float red, float green, float blue, float power) 
	: m_Location(position), m_Color(red,green,blue), m_Power(power) {}

Light::Light(float x, float y, float z, glm::vec3 color, float power)
	: m_Location(x,y,z), m_Color(color), m_Power(power) {};

Light::Light(float x, float y, float z, float red, float green, float blue, float power) 
	: m_Location(x,y,z), m_Color(red, green, blue), m_Power(power) {}

vec3 Light::GetLocation() const {
	return this->m_Location;
}

void Light::SetLocation(vec3 position) {
	this->m_Location = position;
}

vec3 Light::GetColor() const {
	return this->m_Color;
}

void Light::SetColor(vec3 color) {
	this->m_Color = color;
}

float Light::GetPower() {
	return this->m_Power;
}

void Light::SetPower(float power) {
	this->m_Power = power;
}
