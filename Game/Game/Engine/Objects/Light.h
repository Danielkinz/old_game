#ifndef LIGHT_H
#define LIGHT_H

#include "WorldObject.h"

class Light : public WorldObject {
private:
	glm::vec3 m_Location;
	glm::vec3 m_Color;
	float m_Power;

public:
	Light();
	Light(glm::vec3 position, glm::vec3 color, float power = 1.0);
	Light(glm::vec3 position, float red, float green, float blue, float power = 1.0f);
	Light(float x, float y, float z, glm::vec3 color, float power = 1.0f);
	Light(float x, float y, float z, float red, float green, float blue, float power = 1.0);


	glm::vec3 GetLocation() const;
	void SetLocation(glm::vec3 position);

	glm::vec3 GetColor() const;
	void SetColor(glm::vec3 color);

	float GetPower();
	void SetPower(float power);
};

#endif