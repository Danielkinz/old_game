#include "AnimatedModel.h"
#include <queue>
#include <boost\filesystem.hpp>
#include "..\..\Utils.h"
#include <dcl\tools\Exception.h>

#define POSITION_LOCATION 0
#define NORMAL_LOCATION 1
#define TEX_COORDS_LOCATION 2
#define BONE_ID_LOCATION 3
#define BONE_WEIGHT_LOCATION 4

#define INDEX_BUFFER 0
#define POS_VB 1
#define NORMAL_VB 2
#define TEXCOORDS_VB 3
#define BONES_VB 4

#define GRASS_TEXTURE_UNIT GL_TEXTURE0
#define COLOR_TEXTURE_UNIT_INDEX 0

using namespace glm;
using namespace std;

/*-------------------------------Loading-------------------------------*/

AnimatedModel::Node::Node(const aiNode* pNode) : Name(pNode->mName.data), Transformation(math::convertMatrix(pNode->mTransformation)) {
	Children.resize(pNode->mNumChildren);

	for (uint i = 0; i < pNode->mNumChildren; i++) {
		Children[i] = new Node(pNode->mChildren[i]);
	}
}

AnimatedModel::Node::~Node() {
	for (auto it = Children.begin(); it != Children.end(); it++) {
		delete *it;
	}
}

AnimatedModel::NodeAnim::NodeAnim(const aiNodeAnim* other) {
	uint numKeys = std::max(std::max(other->mNumPositionKeys, other->mNumRotationKeys), other->mNumScalingKeys);

	for (uint i = 0; i < numKeys; i++) {
		if (i < other->mNumPositionKeys) PositionKeys.push_back(other->mPositionKeys[i]);
		if (i < other->mNumRotationKeys) RotationKeys.push_back(other->mRotationKeys[i]);
		if (i < other->mNumScalingKeys) ScalingKeys.push_back(other->mScalingKeys[i]);
	}
}

AnimatedModel::VectorKey::VectorKey(const aiVectorKey key) {
	this->Time = key.mTime;
	this->Value = { key.mValue.x, key.mValue.y, key.mValue.z };
}

AnimatedModel::QuatKey::QuatKey(const aiQuatKey key) {
	this->Time = key.mTime;
	this->Value = math::convertQuat(key.mValue);
}

AnimatedModel::AnimatedModel() : m_VAO(0), m_Reflectivity(0), m_ShineDamper(10) {
	ZERO_MEM(m_Buffers);
}

AnimatedModel::~AnimatedModel() {
	this->Clear();
}

void AnimatedModel::Clear() {
	uint i = 0;

	if (m_Buffers[0]) {
		glDeleteBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);
		ZERO_MEM(m_Buffers);
	}

	for (auto it = m_Textures.begin(); it != m_Textures.end(); it++) {
		if (*it != &Texture::GetDefaultTexture()) SAFE_DELETE(*it);
	}

	if (m_VAO) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}

	delete m_RootNode;
	for (auto it = m_NewNodeMapping.begin(); it != m_NewNodeMapping.end(); it++) {
		for (auto it2 = (*it).begin(); it2 != (*it).end(); it2++) {
			delete it2->second;
		}
	}
}

float AnimatedModel::getShineDamper() const {
	return this->m_ShineDamper;
}

float AnimatedModel::getReflectivity() const {
	return this->m_Reflectivity;
}

void AnimatedModel::setShineDamper(float shineDamper) {
	this->m_ShineDamper = shineDamper;
}

void AnimatedModel::setReflectivity(float reflectivity) {
	this->m_Reflectivity = reflectivity;
}

Animation AnimatedModel::GetAnimation(uint id) {
	return m_Animations[id];
}

bool AnimatedModel::LoadModel(const aiScene* pScene, const std::string& path) {
	bool ret = false;

	Clear();

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);

	m_GlobalInverseTransform = inverse(math::convertMatrix(pScene->mRootNode->mTransformation));
	ret = initFromScene(pScene, m_Path = path);
	m_HasAnimations = true;

	glBindVertexArray(0);

	return ret;
}

bool AnimatedModel::initFromScene(const aiScene* pScene, const std::string& filename) {
	m_Meshes.resize(pScene->mNumMeshes);
	m_Textures.resize(pScene->mNumMaterials);

	vector<vec3> positions;
	vector<vec3> normals;
	vector<vec2> texCoords;
	vector<uint> indices;
	vector<VertexBone> bones;

	uint numVertices = 0;
	uint numIndices = 0;
	uint i = 0;

	// Count number of vertices and indices
	for (; i < m_Meshes.size(); i++) {
		m_Meshes[i].MaterialIndex = pScene->mMeshes[i]->mMaterialIndex;
		m_Meshes[i].NumIndices = pScene->mMeshes[i]->mNumFaces * 3;
		m_Meshes[i].BaseVertex = numVertices;
		m_Meshes[i].BaseIndex = numIndices;

		numVertices += pScene->mMeshes[i]->mNumVertices;
		numIndices += m_Meshes[i].NumIndices;
	}

	// Reserve space
	positions.reserve(numVertices);
	normals.reserve(numVertices);
	texCoords.reserve(numVertices);
	indices.reserve(numIndices);

	// Initializes all the meshes
	for (i = 0; i < m_Meshes.size(); i++)
		initMesh(pScene->mMeshes[i], positions, normals, texCoords, indices);

	if (!initMaterials(pScene)) return false;

	// Generates and populates opengl buffers

	// Position
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[POS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Normals
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normals[0]) * normals.size(), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Texture Coordinates
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[TEXCOORDS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords[0]) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEX_COORDS_LOCATION);
	glVertexAttribPointer(TEX_COORDS_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Buffers[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

	// Loads all the required info for animations
	bones.resize(positions.size());
	for (i = 0; i < m_Meshes.size(); i++)
		loadBones(i, pScene->mMeshes[i], bones);
	mapNodes(pScene);

	// Bone IDs and Weights
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[BONES_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(bones[0]) * bones.size(), &bones[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(BONE_ID_LOCATION);
	glVertexAttribIPointer(BONE_ID_LOCATION, 4, GL_INT, sizeof(VertexBone), (const GLvoid*)0);
	glEnableVertexAttribArray(BONE_WEIGHT_LOCATION);
	glVertexAttribPointer(BONE_WEIGHT_LOCATION, 4, GL_FLOAT, GL_FALSE, sizeof(VertexBone), (const GLvoid*)16);

	GLExceptionIfError();
	return true;
}

void AnimatedModel::initMesh(const aiMesh* mesh, vector<vec3>& positions, vector<vec3>& normals, vector<vec2>& texCoords, vector<uint>& indices) {
	const aiVector3D zero3D(0.0f, 0.0f, 0.0f);
	uint i = 0;

	// Populate the vertex attribute vectors
	for (; i < mesh->mNumVertices; i++) {
		const aiVector3D* pos = &(mesh->mVertices[i]);
		const aiVector3D* normal = &(mesh->mNormals[i]);
		const aiVector3D* texCoord = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][i]) : &zero3D;

		if (pos->x < this->m_MinCoords.x) this->m_MinCoords.x = pos->x;
		if (pos->y < this->m_MinCoords.y) this->m_MinCoords.y = pos->y;
		if (pos->z < this->m_MinCoords.z) this->m_MinCoords.z = pos->z;
		if (pos->x > this->m_MaxCoords.x) this->m_MaxCoords.x = pos->x;
		if (pos->y > this->m_MaxCoords.y) this->m_MaxCoords.y = pos->y;
		if (pos->z > this->m_MaxCoords.z) this->m_MaxCoords.z = pos->z;

		positions.push_back(vec3(pos->x, pos->y, pos->z));
		normals.push_back(vec3(normal->x, normal->y, normal->z));
		texCoords.push_back(vec2(texCoord->x, texCoord->y));
	}

	// Populate the index buffer
	for (i = 0; i < mesh->mNumFaces; i++) {
		const aiFace& face = mesh->mFaces[i];
		assert(face.mNumIndices == 3);
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}
}

bool AnimatedModel::initMaterials(const aiScene* pScene) {
	std::string fullpath;
	bool ret = true;
	uint i = 0;
	aiString path;

	for (; i < pScene->mNumMaterials; i++) {
		const aiMaterial* material = pScene->mMaterials[i];
		m_Textures[i] = nullptr;

		// Gets the texture path
		if (material->GetTextureCount(aiTextureType_DIFFUSE) <= 0) continue;
		if (material->GetTexture(aiTextureType_DIFFUSE, 0, &path, NULL, NULL, NULL, NULL, NULL) != AI_SUCCESS) continue;

		fullpath = "Resources\\textures\\" + boost::filesystem::path(path.C_Str()).filename().string();
		m_Textures[i] = new Texture(GL_TEXTURE_2D, fullpath);

		try {
			m_Textures[i]->Load();
			cout << "Loaded texture \"" << fullpath << "\"" << endl;
		} catch (dcl::tools::Exception& e) {
			cout << e.what() << endl;
			delete m_Textures[i];
			m_Textures[i] = &Texture::GetDefaultTexture();
		}
	}

	return ret;
}

/*-------------------------------Animations-------------------------------*/

void AnimatedModel::PreprocessedVertexBone::addBone(uint id, float weight) {
	_weights[id] = weight;
}

AnimatedModel::VertexBone AnimatedModel::PreprocessedVertexBone::getVertexBone() {
	VertexBone bone;
	float weight;
	uint i = 0;
	priority_queue<pair<float, uint>> q;

	// Sorts the weights
	for (auto it = _weights.begin(); it != _weights.end(); it++)
		q.push(pair<float, uint>(it->second, it->first));

	// Gets the NUM_BONES_PER_VERTEX biggest weights
	for (i = 0; i < NUM_BONES_PER_VERTEX; i++) {
		if (q.empty()) {
			bone._weights[i] = 0;
			bone._ids[i] = 0;
		} else {
			bone._weights[i] = q.top().first;
			bone._ids[i] = q.top().second;
			q.pop();
		}
	}

	// adjusts the remaining bones total weight to be 1
	weight = math::sum(bone._weights, bone._weights + NUM_BONES_PER_VERTEX); //bone._weights[0] + bone._weights[1] + bone._weights[2] + bone._weights[3];
	for (i = 0; i < NUM_BONES_PER_VERTEX; i++) {
		bone._weights[i] /= weight;
	}

	return bone;
}

void AnimatedModel::loadBones(const uint meshIndex, const aiMesh* mesh, vector<VertexBone>& bones) {
	uint i = 0;
	uint j = 0;
	uint numBones = m_Bones.size();
	uint boneIndex = 0;
	map<uint, PreprocessedVertexBone> unprocessedBones;

	if (!mesh->HasBones()) return;

	// Goes through all the applicable bones
	for (i = 0; i < mesh->mNumBones; i++) {
		aiBone* bone = mesh->mBones[i];
		string boneName = bone->mName.data;

		// Adds the bone to the list of bones
		if (m_BoneMapping.find(boneName) == m_BoneMapping.end()) {
			boneIndex = numBones++;
			m_Bones.push_back(BoneInfo());
			m_BoneMapping[boneName] = boneIndex;
			m_Bones[boneIndex]._offset = math::convertMatrix(bone->mOffsetMatrix);
		} else boneIndex = m_BoneMapping[boneName];

		// Adds the weights to the bones vector
		for (j = 0; j < bone->mNumWeights; j++) {
			uint vertexID = m_Meshes[meshIndex].BaseVertex + bone->mWeights[j].mVertexId;
			float weight = bone->mWeights[j].mWeight;
			if (unprocessedBones.find(vertexID) == unprocessedBones.end()) unprocessedBones[vertexID] = PreprocessedVertexBone();
			unprocessedBones[vertexID].addBone(boneIndex, weight);
		}
	}

	// Processes all of the data and returns it
	for (auto it = unprocessedBones.begin(); it != unprocessedBones.end(); it++)
		bones[it->first] = it->second.getVertexBone();
}

void AnimatedModel::BoneTransform(uint animationID, float timeInSeconds, vector<mat4>& transforms) {
	mat4 identity(1.0f);
	uint i = 0;

	// Calculates the animation time in ticks
	float ticksPerSecond = m_Animations[animationID].TicksPerSecond != 0 ? m_Animations[animationID].TicksPerSecond : 25.0f;
	float timeInTicks = timeInSeconds * ticksPerSecond;
	float animationTime = fmod(timeInTicks, m_Animations[animationID].Duration);

	// Moves the bones
	ReadNodeHeirarchy(animationID, animationTime, m_RootNode, identity);

	// Returns the result
	transforms.resize(m_Bones.size());
	for (; i < m_Bones.size(); i++)
		transforms[i] = m_Bones[i]._transform;

}

void AnimatedModel::ReadNodeHeirarchy(uint animationID, float animationTime, const Node* node, const mat4& parentTransformation) {
	string nodeName(node->Name);
	mat4 nodeTransformation = node->Transformation;
	const NodeAnim* nodeAnim = m_NewNodeMapping[animationID][nodeName];

	if (nodeAnim) {
		mat4 scalingM = math::scaleTransform(calcInterpolatedScaling(animationTime, nodeAnim));
		mat4 rotationM = toMat4(calcInterpolatedRotation(animationTime, nodeAnim));
		mat4 translationM = math::translationTransform(calcInterpolatedPosition(animationTime, nodeAnim));

		nodeTransformation = translationM * rotationM * scalingM;
	}

	mat4 globalTransformation = parentTransformation * nodeTransformation;

	if (m_BoneMapping.find(nodeName) != m_BoneMapping.end()) {
		uint boneIndex = m_BoneMapping[nodeName];
		m_Bones[boneIndex]._transform = m_GlobalInverseTransform * globalTransformation * m_Bones[boneIndex]._offset;
	}

	for (uint i = 0; i < node->Children.size(); i++) {
		ReadNodeHeirarchy(animationID, animationTime, node->Children[i], globalTransformation);
	}
}

void AnimatedModel::mapNodes(const aiScene* pScene) {
	Animation a;
	m_Animations.resize(pScene->mNumAnimations);

	for (uint i = 0; i < pScene->mNumAnimations; i++) {
		map<string, NodeAnim*> mapping;

		a.ID = m_Animations.size();
		a.Duration = pScene->mAnimations[i]->mDuration;
		a.TicksPerSecond = pScene->mAnimations[i]->mTicksPerSecond;
		m_Animations[i] = a;

		for (uint j = 0; j < pScene->mAnimations[i]->mNumChannels; j++) {
			mapping[string(pScene->mAnimations[i]->mChannels[j]->mNodeName.data)] = new NodeAnim(pScene->mAnimations[i]->mChannels[j]);
		}
		m_NewNodeMapping.push_back(mapping);
	}

	m_RootNode = new Node(pScene->mRootNode);
}

vec3 AnimatedModel::calcInterpolatedPosition(float AnimationTime, const NodeAnim* pNodeAnim) {
	if (pNodeAnim->PositionKeys.size() == 1) {
		return pNodeAnim->PositionKeys[0].Value;
	}

	uint PositionIndex = findPosition(AnimationTime, pNodeAnim);
	uint NextPositionIndex = (PositionIndex + 1);
	assert(NextPositionIndex < pNodeAnim->PositionKeys.size());
	float DeltaTime = (float)(pNodeAnim->PositionKeys[NextPositionIndex].Time - pNodeAnim->PositionKeys[PositionIndex].Time);
	float Factor = (AnimationTime - (float)pNodeAnim->PositionKeys[PositionIndex].Time) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const vec3 Start = pNodeAnim->PositionKeys[PositionIndex].Value;
	const vec3 End = pNodeAnim->PositionKeys[NextPositionIndex].Value;
	return Start + Factor * (End - Start);
}


quat AnimatedModel::calcInterpolatedRotation(float AnimationTime, const NodeAnim* pNodeAnim) {
	if (pNodeAnim->RotationKeys.size() == 1) {
		return pNodeAnim->RotationKeys[0].Value;
	}

	uint RotationIndex = findRotation(AnimationTime, pNodeAnim);
	uint NextRotationIndex = (RotationIndex + 1);
	assert(NextRotationIndex < pNodeAnim->RotationKeys.size());
	float DeltaTime = (float)(pNodeAnim->RotationKeys[NextRotationIndex].Time - pNodeAnim->RotationKeys[RotationIndex].Time);
	float Factor = (AnimationTime - (float)pNodeAnim->RotationKeys[RotationIndex].Time) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const quat& StartRotationQ = pNodeAnim->RotationKeys[RotationIndex].Value;
	const quat& EndRotationQ = pNodeAnim->RotationKeys[NextRotationIndex].Value;
	return normalize(math::interpolate(StartRotationQ, EndRotationQ, Factor));
}


vec3 AnimatedModel::calcInterpolatedScaling(float AnimationTime, const NodeAnim* pNodeAnim) {
	if (pNodeAnim->ScalingKeys.size() == 1) {
		return pNodeAnim->ScalingKeys[0].Value;
	}

	uint ScalingIndex = findScaling(AnimationTime, pNodeAnim);
	uint NextScalingIndex = (ScalingIndex + 1);
	assert(NextScalingIndex < pNodeAnim->ScalingKeys.size());
	float DeltaTime = (float)(pNodeAnim->ScalingKeys[NextScalingIndex].Time - pNodeAnim->ScalingKeys[ScalingIndex].Time);
	float Factor = (AnimationTime - (float)pNodeAnim->ScalingKeys[ScalingIndex].Time) / DeltaTime;
	assert(Factor >= 0.0f && Factor <= 1.0f);
	const vec3& Start = pNodeAnim->ScalingKeys[ScalingIndex].Value;
	const vec3& End = pNodeAnim->ScalingKeys[NextScalingIndex].Value;
	return Start + Factor * (End - Start);
}

uint AnimatedModel::findPosition(float AnimationTime, const NodeAnim* pNodeAnim) {
	assert(pNodeAnim->RotationKeys.size() > 0);
	for (uint i = 0; i < pNodeAnim->PositionKeys.size() - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->PositionKeys[i + 1].Time) {
			return i;
		}
	}

	assert(0);
	return 0;
}


uint AnimatedModel::findRotation(float AnimationTime, const NodeAnim* pNodeAnim) {
	assert(pNodeAnim->RotationKeys.size() > 0);
	for (uint i = 0; i < pNodeAnim->RotationKeys.size() - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->RotationKeys[i + 1].Time) {
			return i;
		}
	}

	assert(0);
	return 0;
}


uint AnimatedModel::findScaling(float AnimationTime, const NodeAnim* pNodeAnim) {
	assert(pNodeAnim->ScalingKeys.size() > 0);
	for (uint i = 0; i < pNodeAnim->ScalingKeys.size() - 1; i++) {
		if (AnimationTime < (float)pNodeAnim->ScalingKeys[i + 1].Time) {
			return i;
		}
	}
	assert(0);

	return 0;
}

/*-------------------------------Rendering-------------------------------*/

void AnimatedModel::Render() {
	uint materialIndex;

	glBindVertexArray(m_VAO);

	for (uint i = 0; i < m_Meshes.size(); i++) {
		materialIndex = m_Meshes[i].MaterialIndex;
		assert(materialIndex < m_Textures.size());

		// Binds the texture if exists
		if (m_Textures.size() && m_Textures[materialIndex])
			m_Textures[materialIndex]->Bind(GRASS_TEXTURE_UNIT);
		else
			Texture::GetDefaultTexture().Bind(GRASS_TEXTURE_UNIT);

		glDrawElementsBaseVertex(GL_TRIANGLES, m_Meshes[i].NumIndices, GL_UNSIGNED_INT, (void*)(sizeof(uint) * m_Meshes[i].BaseIndex), m_Meshes[i].BaseVertex);
	}

	glBindVertexArray(0);
}
