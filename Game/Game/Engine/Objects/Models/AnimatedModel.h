#ifndef ANIMATED_MODEL_H
#define ANIMATED_MODEL_H

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <vector>
#include <map>

#include "Model.h"
#include "Texture.h"

#define NUM_BONES_PER_VERTEX 4

struct Animation {
	uint ID;
	double TicksPerSecond;
	double Duration;
};

class AnimatedModel : public Model {
public:
	AnimatedModel();
	~AnimatedModel();

	bool LoadModel(const aiScene* pScene, const std::string& path);

	float getShineDamper() const;
	float getReflectivity() const;
	void setShineDamper(float);
	void setReflectivity(float);

	Animation GetAnimation(uint id);

	void Render();

	void BoneTransform(uint animationID, float timeInSeconds, std::vector<glm::mat4>& transforms);
private:

	struct BoneInfo {
		glm::mat4 _offset = glm::mat4(1.0f);
		glm::mat4 _transform = glm::mat4(1.0f);
	};

	struct VertexBone {
		uint _ids[NUM_BONES_PER_VERTEX] = { 0 };
		float _weights[NUM_BONES_PER_VERTEX] = { 0 };
	};

	struct PreprocessedVertexBone {
		std::map<uint, float> _weights;
		void addBone(uint id, float weight);
		VertexBone getVertexBone();
	};

	struct VectorKey {
		VectorKey(const aiVectorKey other);
		double Time;
		glm::vec3 Value;
	};

	struct QuatKey {
		QuatKey(const aiQuatKey other);
		double Time;
		glm::quat Value;
	};

	struct NodeAnim {
		NodeAnim(const aiNodeAnim* other);
		std::vector<VectorKey> ScalingKeys;
		std::vector<QuatKey> RotationKeys;
		std::vector<VectorKey> PositionKeys;
	};

	struct Node {
		Node(const aiNode* pNode);
		~Node();
		std::string Name;
		glm::mat4 Transformation;
		std::vector<Node*> Children;
	};

	// Loading
	bool initFromScene(const aiScene* pScene, const std::string& filename);
	void initMesh(const aiMesh* mesh, std::vector<glm::vec3>& position, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texCoords, std::vector<uint>& indices);
	bool initMaterials(const aiScene* pScene);
	void Clear();
	void loadBones(const uint meshIndex, const aiMesh* mesh, std::vector<VertexBone>& bones);

	// Animation
	void ReadNodeHeirarchy(uint animationID, float animationTime, const Node* node, const glm::mat4& parentTransformation);
	void mapNodes(const aiScene* pScene);

	glm::vec3 calcInterpolatedPosition(float AnimationTime, const NodeAnim* pNodeAnim);
	glm::quat calcInterpolatedRotation(float AnimationTime, const NodeAnim* pNodeAnim);
	glm::vec3 calcInterpolatedScaling(float AnimationTime, const NodeAnim* pNodeAnim);
	uint findPosition(float AnimationTime, const NodeAnim* pNodeAnim);
	uint findRotation(float AnimationTime, const NodeAnim* pNodeAnim);
	uint findScaling(float AnimationTime, const NodeAnim* pNodeAnim);

	std::map<std::string, uint> m_BoneMapping;
	std::vector<BoneInfo> m_Bones;
	glm::mat4 m_GlobalInverseTransform;

	std::vector<Animation> m_Animations;
	std::vector<std::map<std::string, NodeAnim*>> m_NewNodeMapping;
	Node* m_RootNode;

	// Rendering 
	float m_ShineDamper;
	float m_Reflectivity;

	// OpenGL 
	std::vector<Mesh> m_Meshes;
	std::vector<Texture*> m_Textures;
	GLuint m_VAO;
	GLuint m_Buffers[6];
};

#endif
