#include "Map.h"
#include "../../Math.h"
#include "../../Utils.h"
#include <stb/stb_image.h>
#include <vector>

using namespace std;
using namespace glm;

#define POS_VB 0
#define COLOR_VB 1

#define POSITION_LOCATION 0
#define COLOR_LOCATION 1

#define GRASS_TEXTURE_UNIT GL_TEXTURE0
#define SAND_TEXTURE_UNIT GL_TEXTURE1

vec3 GRASS_COLORS[5] = {
	vec3(0    , 0.360, 0.035),
	vec3(0    , 0.407, 0.039),
	vec3(0    , 0.482, 0.047),
	vec3(0.003, 0.556, 0.054),
	vec3(0.003, 0.650, 0.066)
};

float Map::GetHeight(int x, int z) {
	x += width * SIZE_MULTIPLIER / 2;
	z += height * SIZE_MULTIPLIER / 2;

	x /= SIZE_MULTIPLIER;
	z /= SIZE_MULTIPLIER;
	return m_Elevation[x][z] * 2 + 2;
}

Map::Map(const string& heightmap) {
	int channels;
	unsigned char* data = stbi_load(heightmap.c_str(), &width, &height, &channels, 0);

	m_Elevation = new float*[height];

	// Generates the height map using noise
	for (int z = 0; z < height; z++) {
		m_Elevation[z] = new float[width];
		for (int x = 0; x < width; x++) {
			unsigned char* pixelOffset = data + (x + width * z) * channels;
			m_Elevation[z][x] = pixelOffset[0] - 100;
		}
	}

	stbi_image_free(data);

	// Converts the height map to vertices
	vector<vec3> positions;
	vector<vec3> colors;

	grass = new Texture(GL_TEXTURE_2D, "Resources/textures/grass.jpg");
	grass->Load();
	sand = new Texture(GL_TEXTURE_2D, "Resources/textures/sand.jpg");
	sand->Load();

	mat4 world = math::worldMatrix(vec3(0.0f, 0.0f, 0.0f), vec3(0.0f, 0.0f, 0.0f), vec3(SIZE_MULTIPLIER, 2.0f, SIZE_MULTIPLIER));
	vec3 color;

	float highest;
	for (uint i = 0; i < height - 1; i++) {
		for (uint j = 0; j < width - 1; j++) {
			positions.push_back(world * vec4(i, m_Elevation[i][j], j, 0));
			positions.push_back(world * vec4(i + 1, m_Elevation[i + 1][j], j, 0));
			positions.push_back(world * vec4(i, m_Elevation[i][j + 1], j + 1, 0));

			positions.push_back(world * vec4(i + 1, m_Elevation[i + 1][j + 1], j + 1, 0));
			positions.push_back(world * vec4(i + 1, m_Elevation[i + 1][j], j, 0));
			positions.push_back(world * vec4(i, m_Elevation[i][j + 1], j + 1, 0));

			highest = fmax(positions[positions.size() - 1].y, positions[positions.size() - 2].y);
			for (uint k = 3; k < 7; k++) {
				highest = fmax(highest, positions[positions.size()-k].y);
			}

			if (highest >= 0 && highest <= 40) {
				color = vec3(1.0, (182 + 58 * (highest / 40.0))/255.0, 0.09);
			} else if (highest >= 40 && highest <= 320) {
				color = GRASS_COLORS[int(highest*2) % 5];
				//color = vec3((37 + 126 * ((int(highest)%20)/20.0)) /255.0, 1, 0.0117);
			} else {
				color = vec3(0, 0, 1);
			}

			colors.push_back(color);
			colors.push_back(color);
			colors.push_back(color);
			colors.push_back(color);
			colors.push_back(color);
			colors.push_back(color);
		}
	}
	
	for (uint i = 0; i < positions.size(); i++) {
		positions[i].x -= width * SIZE_MULTIPLIER / 2;
		positions[i].z -= height * SIZE_MULTIPLIER / 2;
	}

	m_NumVertices = positions.size();

	// Creates the OpenGL buffers
	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);

	// Positions
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[POS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Positions
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[COLOR_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colors[0]) * colors.size(), &colors[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(COLOR_LOCATION);
	glVertexAttribPointer(COLOR_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	GLExceptionIfError();
}

Map::~Map() {
	Clear();
}

void Map::Clear() {
	SAFE_DELETE(grass);
	SAFE_DELETE(sand);

	if (m_Buffers[0]) {
		glDeleteBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);
		ZERO_MEM(m_Buffers);
	}

	if (m_VAO) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}

	for (uint i = 0; i < height; i++) {
		delete[] m_Elevation[i];
	}
	delete[] m_Elevation;
}

void Map::Render() {
	glBindVertexArray(m_VAO);

	grass->Bind(GRASS_TEXTURE_UNIT);
	sand->Bind(SAND_TEXTURE_UNIT);
	glDrawArrays(GL_TRIANGLES, 0, m_NumVertices);

	glBindVertexArray(0);
}
