#ifndef MAP_H
#define MAP_H

#include "Model.h"

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <vector>
#include "Texture.h"

#define SIZE_MULTIPLIER 10

class Map : public Model {
public:
	Map(const std::string& heightmap);
	~Map();

	void Render();

	template<typename Number1, typename Number2>
	float GetHeight(Number1 x, Number2 z);

private:
	void Clear();
	float GetHeight(int x, int z);

	int width;
	int height;

	GLuint m_VAO;
	GLuint m_Buffers[2];
	float** m_Elevation;
	int m_NumVertices;
	Texture* grass;
	Texture* sand;
};

template<typename Number1, typename Number2>
float Map::GetHeight(Number1 nx, Number2 nz) {
	float x1 = nx - (int)nx;
	float z1 = nz - (int)nz;

	float answer;

	if (x1 <= (1 - z1)) {
		answer = math::barryCentric(
			vec3(0, GetHeight(int(nx), int(nz)), 0),
			vec3(1, GetHeight(int(nx) + 1, int(nz)), 0),
			vec3(0, GetHeight(int(nx), int(nz) + 1), 1),
			vec2(x1, z1));
	} else {
		answer = math::barryCentric(
			vec3(1, GetHeight(int(nx) + 1, int(nz)), 0),
			vec3(1, GetHeight(int(nx) + 1, int(nz) + 1), 1),
			vec3(0, GetHeight(int(nx), int(nz) + 1), 1),
			vec2(x1, z1));
	}

	return answer;
}



#endif