#include "Model.h"
#include "StaticModel.h"
#include "AnimatedModel.h"
#include "..\..\Storage.h"
#include "..\..\Utils.h"

using namespace glm;
using namespace std;

/*-------------------------------Loading-------------------------------*/
Model::Model() {}

Model::~Model() {
	EngineStorage::UnregisterModel(this);
}

Mesh::Mesh() : NumIndices(0), BaseIndex(0), BaseVertex(0), MaterialIndex((unsigned int)INVALID_MATERIAL) {}

/*-------------------------------Getters and Setters-------------------------------*/

vec3 Model::GetMinCoords() const {
	return this->m_MinCoords;
}

vec3 Model::GetMaxCoords() const {
	return this->m_MaxCoords;
}

vec3 Model::GetCenter() const {
	vec3 pos;
	pos.x = (this->m_MinCoords.x + this->m_MaxCoords.x) / 2;
	pos.y = (this->m_MinCoords.y + this->m_MaxCoords.y) / 2;
	pos.z = (this->m_MinCoords.z + this->m_MaxCoords.z) / 2;
	return pos;
}

vec3 Model::GetCenterBottom() const {
	vec3 pos;
	pos.x = (this->m_MinCoords.x + this->m_MaxCoords.x) / 2;
	pos.y = this->m_MinCoords.y;
	pos.z = (this->m_MinCoords.z + this->m_MaxCoords.z) / 2;
	return pos;
}

const string& Model::GetPath() const {
	return this->m_Path;
}

bool Model::HasAnimation() const {
	return this->m_HasAnimations;
}

Model* Model::LoadModel(const string& path, bool loadAnimations) {
	Model* model = nullptr;
	Assimp::Importer importer;
	const aiScene* pScene;

	if (EngineStorage::IsModelLoaded(path))
		return EngineStorage::GetModel(path);

	cout << "Loading model: \"" << path << "\"" << endl;

	pScene = importer.ReadFile(path.c_str(), ASSIMP_LOAD_FLAGS);

	if (!pScene) {
		cout << "Error parsing: '" << path << "': " << importer.GetErrorString() << endl;
		return nullptr;
	}

	if (pScene->HasAnimations() && loadAnimations) {
		model = new AnimatedModel();
		if (!static_cast<AnimatedModel*>(model)->LoadModel(pScene, path)) {
			cout << "Error while loading model '" << path << "'" << endl;
			delete model;
			return nullptr;
		}
	} else {
		model = new StaticModel();
		if (!static_cast<StaticModel*>(model)->LoadModel(pScene, path)) {
			cout << "Error while loading model '" << path << "'" << endl;
			delete model;
			return nullptr;
		}
	}

	EngineStorage::RegisterModel(model);
	return model;
}