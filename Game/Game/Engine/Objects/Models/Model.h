#ifndef MODEL_H
#define MODEL_H

#include "..\..\GameObject.h"
#include "..\..\Math.h"
#include <glm\glm.hpp>
#include <string>

#define INVALID_MATERIAL 0xffffffff

struct Mesh {
	Mesh();
	unsigned int NumIndices;
	unsigned int BaseVertex;
	unsigned int BaseIndex;
	unsigned int MaterialIndex;
};

class Model : public GameObject {
public:
	Model();
	virtual ~Model();

	// Getters / Setters
	glm::vec3 GetMinCoords() const;
	glm::vec3 GetMaxCoords() const;
	glm::vec3 GetCenter() const;
	glm::vec3 GetCenterBottom() const;

	const std::string& GetPath() const;

	bool HasAnimation() const;

	virtual void Render() = 0;
	static Model* LoadModel(const std::string& path, bool loadAnimations = true);

protected:
	// Model 
	glm::vec3 m_MinCoords;
	glm::vec3 m_MaxCoords;
	std::string m_Path;
	bool m_HasAnimations;
};

#endif
