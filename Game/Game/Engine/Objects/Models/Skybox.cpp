#include "Skybox.h"
#include "..\..\Utils.h"
#include <dcl\tools\Exception.h>
#include "..\..\Storage.h"

using namespace std;
using namespace glm;

float skyboxVertices[] = {
	// positions          
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f,  1.0f
};

Skybox::Skybox() : m_VAO(0), m_Texture(0), m_PositionBuffer(0) {
	m_MinCoords = vec3(-1.0f, -1.0f, -1.0f);
	m_MaxCoords = vec3(1.0f, 1.0f, 1.0f);
	m_HasAnimations = false;
}

Skybox::~Skybox() {
	Clear();
}

void Skybox::Clear() {
	if (m_Texture) {
		glDeleteTextures(1, &m_Texture);
		m_Texture = 0;
	}

	if (m_PositionBuffer) {
		glDeleteBuffers(1, &m_PositionBuffer);
		m_PositionBuffer = 0;
	}

	if (m_VAO) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}
}

Skybox* Skybox::LoadSkybox(const std::vector<std::string>& textures) {
	if (EngineStorage::IsModelLoaded("Cubemap: " + textures[0]))
		return static_cast<Skybox*>(EngineStorage::GetModel("Cubemap: " + textures[0]));

	Skybox* skybox = new Skybox();

	if (textures.size() != 6) return nullptr;

	try {
		skybox->Load(textures);
	} catch (dcl::tools::Exception& e) {
		cout << e.what() << endl;
	}

	return skybox;
}

void Skybox::Load(const vector<string>& faces) {
	Clear();
	m_Path = "Cubemap: " + faces[0];

	glGenTextures(1, &m_Texture);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_Texture);

	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++) {
		unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data) {
			cout << "Loaded cubemap face '" << faces[i] << "'" << endl;
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		} else {
			stbi_image_free(data);
			throw dcl::tools::Exception("Cubemap texture failed to load at path: \"", faces[i], "\"\n");
		}
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(1, &m_PositionBuffer);

	glBindBuffer(GL_ARRAY_BUFFER, m_PositionBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);

	glBindVertexArray(0);
	GLExceptionIfError();
}

void Skybox::Render() {
	GLint OldCullFaceMode;
	GLint OldDepthFuncMode;
	glGetIntegerv(GL_CULL_FACE_MODE, &OldCullFaceMode);
	glGetIntegerv(GL_DEPTH_FUNC, &OldDepthFuncMode);

	glCullFace(GL_FRONT);
	glDepthFunc(GL_LEQUAL);

	glBindVertexArray(m_VAO);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_Texture);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);

	glCullFace(OldCullFaceMode);
	glDepthFunc(OldDepthFuncMode);
}
