#ifndef SKYBOX_H
#define SKYBOX_H

#include <vector>
#include "Texture.h"
#include "Model.h"

class Skybox : public Model {
private:
	GLuint m_Texture;
	GLuint m_VAO;
	GLuint m_PositionBuffer;

	Skybox();
	void Load(const std::vector<std::string>& faces);

public:
	~Skybox();

	void Clear();
	void Render();
	static Skybox* LoadSkybox(const std::vector<std::string> &faces);
};

#endif