#include "StaticModel.h"
#include <boost\filesystem.hpp>
#include "..\..\Utils.h"

#define POSITION_LOCATION 0
#define NORMAL_LOCATION 1
#define TEX_COORDS_LOCATION 2
#define MVP_LOCATION 3
#define MODEL_LOCATION 7

#define INDEX_BUFFER 0
#define POS_VB 1
#define NORMAL_VB 2
#define TEXCOORDS_VB 3
#define MVP_MAT_VB 4
#define MODEL_MAT_VB 5

#define GRASS_TEXTURE_UNIT GL_TEXTURE0
#define COLOR_TEXTURE_UNIT_INDEX 0

using namespace std;
using namespace glm;

/*-------------------------------Loading-------------------------------*/

StaticModel::StaticModel() : m_VAO(0), m_Reflectivity(0), m_ShineDamper(10) {
	ZERO_MEM(m_Buffers);
}

StaticModel::~StaticModel() {
	this->Clear();
}

void StaticModel::Clear() {
	uint i = 0;
	for (; i < m_Textures.size(); i++)
		if (m_Textures[i] != &Texture::GetDefaultTexture())
			SAFE_DELETE(m_Textures[i]);

	if (m_Buffers[0]) {
		glDeleteBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);
		ZERO_MEM(m_Buffers);
	}

	if (m_VAO) {
		glDeleteVertexArrays(1, &m_VAO);
		m_VAO = 0;
	}
}

float StaticModel::getShineDamper() const {
	return this->m_ShineDamper;
}

float StaticModel::getReflectivity() const {
	return this->m_Reflectivity;
}

void StaticModel::setShineDamper(float shineDamper) {
	this->m_ShineDamper = shineDamper;
}

void StaticModel::setReflectivity(float reflectivity) {
	this->m_Reflectivity = reflectivity;
}

bool StaticModel::LoadModel(const aiScene* pScene, const string& path) {
	bool ret = false;

	Clear();

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);

	ret = InitFromScene(pScene, m_Path = path);

	glBindVertexArray(0);

	return ret;
}

bool StaticModel::InitFromScene(const aiScene* pScene, const string& filename) {
	m_Meshes.resize(pScene->mNumMeshes);
	m_Textures.resize(pScene->mNumMaterials);

	vector<vec3> positions;
	vector<vec3> normals;
	vector<vec2> texCoords;
	vector<uint> indices;

	uint numVertices = 0;
	uint numIndices = 0;
	uint i = 0;

	// Count number of vertices and indices
	for (; i < m_Meshes.size(); i++) {
		m_Meshes[i].MaterialIndex = pScene->mMeshes[i]->mMaterialIndex;
		m_Meshes[i].NumIndices = pScene->mMeshes[i]->mNumFaces * 3;
		m_Meshes[i].BaseVertex = numVertices;
		m_Meshes[i].BaseIndex = numIndices;

		numVertices += pScene->mMeshes[i]->mNumVertices;
		numIndices += m_Meshes[i].NumIndices;
	}

	// Reserve space
	positions.reserve(numVertices);
	normals.reserve(numVertices);
	texCoords.reserve(numVertices);
	indices.reserve(numIndices);

	// Initializes all the meshes
	for (i = 0; i < m_Meshes.size(); i++)
		InitMesh(pScene->mMeshes[i], positions, normals, texCoords, indices);

	if (!InitMaterials(pScene)) return false;

	// Generates and populates opengl buffers

	// Positions
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[POS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Normals
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[NORMAL_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(normals[0]) * normals.size(), &normals[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(NORMAL_LOCATION);
	glVertexAttribPointer(NORMAL_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// Texture Coordinates
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[TEXCOORDS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texCoords[0]) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(TEX_COORDS_LOCATION);
	glVertexAttribPointer(TEX_COORDS_LOCATION, 2, GL_FLOAT, GL_FALSE, 0, 0);

	// Indices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Buffers[INDEX_BUFFER]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * indices.size(), &indices[0], GL_STATIC_DRAW);

	// MVP buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[MVP_MAT_VB]);
	for (i = 0; i < 4; i++) {
		glEnableVertexAttribArray(MVP_LOCATION + i);
		glVertexAttribPointer(MVP_LOCATION + i, 4, GL_FLOAT, GL_FALSE, sizeof(mat4),
			(const GLvoid*)(sizeof(GLfloat) * i * 4));
		glVertexAttribDivisor(MVP_LOCATION + i, 1);
	}

	// StaticModel buffer
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[MODEL_MAT_VB]);
	for (i = 0; i < 4; i++) {
		glEnableVertexAttribArray(MODEL_LOCATION + i);
		glVertexAttribPointer(MODEL_LOCATION + i, 4, GL_FLOAT, GL_FALSE, sizeof(mat4),
			(const GLvoid*)(sizeof(GLfloat) * i * 4));
		glVertexAttribDivisor(MODEL_LOCATION + i, 1);
	}

	GLExceptionIfError();
	return true;
}

void StaticModel::InitMesh(const aiMesh* mesh, vector<vec3>& positions, vector<vec3>& normals, vector<vec2>& texCoords, vector<uint>& indices) {
	const aiVector3D zero3D(0.0f, 0.0f, 0.0f);
	uint i = 0;

	// Populate the vertex attribute vectors
	for (; i < mesh->mNumVertices; i++) {
		const aiVector3D* pos = &(mesh->mVertices[i]);
		const aiVector3D* normal = &(mesh->mNormals[i]);
		const aiVector3D* texCoord = mesh->HasTextureCoords(0) ? &(mesh->mTextureCoords[0][i]) : &zero3D;

		if (pos->x < this->m_MinCoords.x) this->m_MinCoords.x = pos->x;
		if (pos->y < this->m_MinCoords.y) this->m_MinCoords.y = pos->y;
		if (pos->z < this->m_MinCoords.z) this->m_MinCoords.z = pos->z;
		if (pos->x > this->m_MaxCoords.x) this->m_MaxCoords.x = pos->x;
		if (pos->y > this->m_MaxCoords.y) this->m_MaxCoords.y = pos->y;
		if (pos->z > this->m_MaxCoords.z) this->m_MaxCoords.z = pos->z;

		positions.push_back(vec3(pos->x, pos->y, pos->z));
		normals.push_back(vec3(normal->x, normal->y, normal->z));
		texCoords.push_back(vec2(texCoord->x, texCoord->y));
	}

	// Populate the index buffer
	for (i = 0; i < mesh->mNumFaces; i++) {
		const aiFace& face = mesh->mFaces[i];
		assert(face.mNumIndices == 3);
		indices.push_back(face.mIndices[0]);
		indices.push_back(face.mIndices[1]);
		indices.push_back(face.mIndices[2]);
	}
}

bool StaticModel::InitMaterials(const aiScene* pScene) {
	string fullpath;
	bool ret = true;
	uint i = 0;
	aiString path;

	for (; i < pScene->mNumMaterials; i++) {
		const aiMaterial* material = pScene->mMaterials[i];
		m_Textures[i] = nullptr;

		// Gets the texture path
		if (material->GetTextureCount(aiTextureType_DIFFUSE) <= 0) continue;
		if (material->GetTexture(aiTextureType_DIFFUSE, 0, &path, NULL, NULL, NULL, NULL, NULL) != AI_SUCCESS) continue;

		fullpath = "Resources\\textures\\" + boost::filesystem::path(path.C_Str()).filename().string();
		m_Textures[i] = new Texture(GL_TEXTURE_2D, fullpath);

		try {
			m_Textures[i]->Load();
			cout << "Loaded texture \"" << fullpath << "\"" << endl;
		} catch (dcl::tools::Exception& e) {
			cout << e.what() << endl;
			delete m_Textures[i];
			m_Textures[i] = &Texture::GetDefaultTexture();
		}
	}

	return ret;
}

/*-------------------------------Rendering-------------------------------*/

void StaticModel::Render() {
	glBindVertexArray(m_VAO);

	for (uint i = 0; i < m_Meshes.size(); i++) {
		const uint materialIndex = m_Meshes[i].MaterialIndex;

		assert(materialIndex < m_Textures.size());

		if (m_Textures[materialIndex]) {
			m_Textures[materialIndex]->Bind(GRASS_TEXTURE_UNIT);
		}

		glDrawElementsBaseVertex(GL_TRIANGLES,
			m_Meshes[i].NumIndices,
			GL_UNSIGNED_INT,
			(void*)(sizeof(uint) * m_Meshes[i].BaseIndex),
			m_Meshes[i].BaseVertex);
	}

	// Make sure the VAO is not changed from the outside    
	glBindVertexArray(0);
}

void StaticModel::Render(const vector<mat4>& worldMatrices, const vector<mat4>& WVPs) {
	uint i = 0;
	uint materialIndex = 0;

	assert(worldMatrices.size() == WVPs.size());

	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[MVP_MAT_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(mat4) * worldMatrices.size(), &WVPs[0], GL_DYNAMIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[MODEL_MAT_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(mat4) * worldMatrices.size(), &worldMatrices[0], GL_DYNAMIC_DRAW);

	glBindVertexArray(m_VAO);

	for (; i < m_Meshes.size(); i++) {
		materialIndex = m_Meshes[i].MaterialIndex;

		assert(materialIndex < m_Textures.size());

		// Binds the texture if exists
		if (m_Textures.size() && m_Textures[materialIndex])
			m_Textures[materialIndex]->Bind(GRASS_TEXTURE_UNIT);
		else
			Texture::GetDefaultTexture().Bind(GRASS_TEXTURE_UNIT);

		//glDrawElementsBaseVertex(GL_TRIANGLES, _meshes[i]._numIndices, GL_UNSIGNED_INT, (void*)(sizeof(uint) * _meshes[i]._baseIndex), _meshes[i]._baseVertex);
		glDrawElementsInstancedBaseVertex(GL_TRIANGLES,
			m_Meshes[i].NumIndices,
			GL_UNSIGNED_INT,
			(void*)(sizeof(uint) * m_Meshes[i].BaseIndex),
			worldMatrices.size(),
			m_Meshes[i].BaseVertex);
	}

	glBindVertexArray(0);
}
