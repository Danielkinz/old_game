#ifndef STATIC_MODEL_H
#define STATIC_MODEL_H

#include "Model.h"

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include <vector>
#include "Texture.h"

class StaticModel : public Model {
public:
	StaticModel();
	~StaticModel();

	bool LoadModel(const aiScene* pScene, const std::string& path);

	float getShineDamper() const;
	float getReflectivity() const;
	void setShineDamper(float);
	void setReflectivity(float);

	void Render();
	void Render(const std::vector<glm::mat4> & worldMatrices, const std::vector<glm::mat4>& WVPs);

private:
	// Loading
	bool InitFromScene(const aiScene* pScene, const std::string& filename);
	void InitMesh(const aiMesh* mesh, std::vector<glm::vec3>& position, std::vector<glm::vec3>& normals, std::vector<glm::vec2>& texCoords, std::vector<uint>& indices);
	bool InitMaterials(const aiScene* pScene);
	void Clear();

	// Rendering 
	float m_ShineDamper;
	float m_Reflectivity;

	// OpenGL 
	std::vector<Mesh> m_Meshes;
	std::vector<Texture*> m_Textures;
	GLuint m_VAO;
	GLuint m_Buffers[6];
};

#endif
