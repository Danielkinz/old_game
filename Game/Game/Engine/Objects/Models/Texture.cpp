#include "Texture.h"
#include <dcl\Tools\Exception.h>

using namespace std;

Texture Texture::defTexture;

void Texture::LoadDefaultTexture(const string& filename) {
	defTexture = Texture(GL_TEXTURE_2D, filename);
	defTexture.Load();
}

Texture::Texture() : Texture(GL_TEXTURE_2D, "Resources/White.png") {}

Texture::Texture(GLenum textureTarget, const string& filename) {
	_textureTarget = textureTarget;
	_filename = filename;
	_loaded = false;
}

Texture::~Texture() {
	glDeleteTextures(1, &_textureObject);
}

void Texture::Load() {
	int width, height, channels;

	if (_loaded) return throw ;
	_loaded = true;

	// Creates the data and checks that its valid
	unsigned char* data = stbi_load(_filename.c_str(), &width, &height, &channels, 0);
	if (!data) throw dcl::tools::Exception("Failed to load texture '", _filename, "'");

	// Creates the object
	glGenTextures(1, &_textureObject);
	glBindTexture(_textureTarget, _textureObject);
	glTexImage2D(_textureTarget, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	glTexParameterf(_textureTarget, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(_textureTarget, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(_textureTarget);

	// Cleaning
	stbi_image_free(data);
	glBindTexture(_textureTarget, 0);
}

void Texture::Bind(GLenum TextureUnit) const {
	glActiveTexture(TextureUnit);
	glBindTexture(_textureTarget, _textureObject);
}

Texture& Texture::GetDefaultTexture() {
	return defTexture;
}

GLuint Texture::GetTextureObject() {
	return _textureObject;
}

