#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/glew.h>
#include <iostream>
#include <stb/stb_image.h>

// A class that holds data about a single texture
class Texture {
private:
	Texture();
	std::string _filename;
	GLenum _textureTarget;
	GLuint _textureObject;
	bool _loaded;
	static Texture defTexture;
public:
	Texture(GLenum textureTarget, const std::string& filename);
	~Texture();

	// Loads the texture from the file
	void Load();

	// Binds the texture to the currently active unit
	void Bind(GLenum textureUnit) const;

	GLuint GetTextureObject();

	static Texture& GetDefaultTexture();
	static void LoadDefaultTexture(const std::string& filename = "Resources/textures/White.png");
};

#endif
