#include "Water.h"

#include "../../Utils.h"
#include <vector>

using namespace std;
using namespace glm;

#define POS_VB 0
#define POSITION_LOCATION 0

Water::Water(glm::vec2 min, glm::vec2 max, float height) {
	vector<vec3> positions;
	positions.push_back(vec3(min.x, height, min.y));
	positions.push_back(vec3(max.x, height, min.y));
	positions.push_back(vec3(min.x, height, max.y));

	positions.push_back(vec3(max.x, height, max.y));
	positions.push_back(vec3(max.x, height, min.y));
	positions.push_back(vec3(min.x, height, max.y));

	glGenVertexArrays(1, &m_VAO);
	glBindVertexArray(m_VAO);

	glGenBuffers(ARRAY_SIZE_IN_ELEMENTS(m_Buffers), m_Buffers);

	// Positions
	glBindBuffer(GL_ARRAY_BUFFER, m_Buffers[POS_VB]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(positions[0]) * positions.size(), &positions[0], GL_STATIC_DRAW);
	glEnableVertexAttribArray(POSITION_LOCATION);
	glVertexAttribPointer(POSITION_LOCATION, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glBindVertexArray(0);
	GLExceptionIfError();
}

void Water::Render() {
	glBindVertexArray(m_VAO);
	
	glDrawArrays(GL_TRIANGLES, 0, 6);

	glBindVertexArray(0);
}
