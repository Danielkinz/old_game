#ifndef WATER_H
#define WATER_H

#include <GL/glew.h>
#include "Model.h"

class Water : public Model {
public:
	Water(glm::vec2 min, glm::vec2 max, float height);
	void Render();

private:
	GLuint m_VAO;
	GLuint m_Buffers[1];
};


#endif