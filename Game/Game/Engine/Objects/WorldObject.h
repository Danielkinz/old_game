#ifndef WORLD_OBJECT_H
#define WORLD_OBJECT_H

#include "..\GameObject.h"
#include <glm\glm.hpp>

class WorldObject : public GameObject {
public:
	WorldObject();
	virtual ~WorldObject();

	virtual glm::vec3 GetLocation() const = 0;
};

#endif