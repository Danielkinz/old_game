#include "Renderer.h"
#include <boost\type_traits.hpp>
#include <dcl\tools\Exception.h>
#include "IO\Window.h"

using namespace std;
using namespace glm;

SceneRenderer::SceneRenderer() : m_Skybox(nullptr), m_Light(nullptr), m_Camera(nullptr), m_Map(nullptr) {}

SceneRenderer::ScreenTextInfo::ScreenTextInfo(const string& text, float x, float y, glm::vec3 color, float scale)
	: Text(text), X(x), Y(y), Color(color), Scale(scale) {}

void SceneRenderer::Initialize() {
	if (!m_AnimatedModelEffect.Init()) {
		throw dcl::tools::Exception("Animated model technique initialization failed");
	}

	if (!m_StaticModelEffect.Init()) {
		throw dcl::tools::Exception("Static model technique initialization failed");
	}

	if (!m_SkyboxEffect.Init()) {
		throw dcl::tools::Exception("Skybox technique initialization failed");
	}

	if (!m_ScreenTextEffect.Init()) {
		throw dcl::tools::Exception("Screen text technique initialization failed");
	}

	if (!m_HitboxEffect.Init()) {
		throw dcl::tools::Exception("Hitbox technique initialization failed");
	}

	if (!m_MapEffect.Init()) {
		throw dcl::tools::Exception("Map technique initialization failed");
	}

	if (!m_WaterEffect.Init()) {
		throw dcl::tools::Exception("Water technique initialization failed");
	}
}

void SceneRenderer::SetLight(const Light const * light) {
	m_Light = light;
}

void SceneRenderer::SetCamera(const Camera const * camera) {
	m_Camera = camera;
}

void SceneRenderer::SetMap(Map* map) {
	m_Map = map;
}

void SceneRenderer::SetSkybox(Skybox* skybox) {
	m_Skybox = skybox;
}

void SceneRenderer::Add(const Entity* entity) {
	m_Entities[entity->GetModel()].push_back(entity);
}

void SceneRenderer::Remove(const Entity* entity) {
	vector<const Entity*>& ents = m_Entities[entity->GetModel()];
	ents.erase(find(ents.begin(), ents.end(), entity));
}

void SceneRenderer::Clear() {
	m_Entities.clear();
}

void SceneRenderer::AddWater(const Water* water) {
	m_Water.push_back((Water*) water);
}

void SceneRenderer::RemoveWater(const Water* water) {
	m_Water.erase(std::find(m_Water.begin(), m_Water.end(), water));
}

void SceneRenderer::Render() {
	mat4 projectionMatrix = perspective((float)radians(75.0f), (float) Window::GetWidth() / (float) Window::GetHeight(), 0.1f, 10000.0f);
	mat4 viewMatrix = m_Camera->GetViewMatrix();
	vector<mat4> WVPs;
	vector<mat4> WorldMatrices;
	vector<mat4> boneTransforms;
	mat4 worldMatrix;

	// For each model
	for (auto mit = m_Entities.begin(); mit != m_Entities.end(); mit++) {
		Model* model = mit->first;
		if (dynamic_cast<StaticModel*>(model)) {
			m_StaticModelEffect.Enable();
			m_StaticModelEffect.SetLight(*m_Light);
			m_StaticModelEffect.SetView(viewMatrix);
			m_StaticModelEffect.SetShineDamper(dynamic_cast<StaticModel*>(model)->getShineDamper());
			m_StaticModelEffect.SetReflectivity(dynamic_cast<StaticModel*>(model)->getReflectivity());

			WVPs.clear();
			WorldMatrices.clear();
			for (auto eit = mit->second.begin(); eit != mit->second.end(); eit++) {
				worldMatrix = (*eit)->CalculateWorldMatrix();
				WorldMatrices.push_back(worldMatrix);
				WVPs.push_back(projectionMatrix * viewMatrix * worldMatrix);
			}

			((StaticModel*)model)->Render(WorldMatrices, WVPs);

		} else if (dynamic_cast<AnimatedModel*>(model)) {
			m_AnimatedModelEffect.Enable();
			m_AnimatedModelEffect.SetLight(*m_Light);
			m_AnimatedModelEffect.SetView(viewMatrix);
			m_AnimatedModelEffect.SetShineDamper(dynamic_cast<AnimatedModel*>(model)->getShineDamper());
			m_AnimatedModelEffect.SetReflectivity(dynamic_cast<AnimatedModel*>(model)->getReflectivity());

			// Animated model rendering
			for (auto eit = mit->second.begin(); eit != mit->second.end(); eit++) {
				static_cast<AnimatedModel*>(model)->BoneTransform((*eit)->GetAnimationID(), (*eit)->GetAnimationTime(), boneTransforms);
				m_AnimatedModelEffect.SetBones(boneTransforms);
				m_AnimatedModelEffect.SetWorld(worldMatrix = (*eit)->CalculateWorldMatrix());
				m_AnimatedModelEffect.SetWVP(projectionMatrix * viewMatrix * worldMatrix);

				model->Render();
			}
		}
	}

	if (m_HitboxesEnabled) {
		vec3 Scale;
		m_HitboxEffect.Enable();

		for each (auto p in m_Entities) {
			Scale = {
				(p.first->GetMaxCoords().x - p.first->GetMinCoords().x) / 2,
				(p.first->GetMaxCoords().y - p.first->GetMinCoords().y) / 2,
				(p.first->GetMaxCoords().z - p.first->GetMinCoords().z) / 2
			};

			for each (const Entity* entity in p.second) {
				m_HitboxEffect.SetWVP(projectionMatrix * viewMatrix * math::worldMatrix(entity->GetCenter(), vec3(0, 0, 0), Scale * entity->GetScale()));
				//CubeHitbox::Render();
			}
		}
	}

	if (m_Map) {
		m_MapEffect.Enable();
		//m_MapEffect.SetLight(*m_Light);
		//m_MapEffect.SetView(viewMatrix);
		//m_MapEffect.SetShineDamper(m_Map->getShineDamper());
		//m_MapEffect.SetReflectivity(m_Map->getReflectivity());
		m_MapEffect.SetVP(projectionMatrix* viewMatrix);
		m_Map->Render();
	}

	
	m_WaterEffect.Enable();
	m_WaterEffect.SetVP(projectionMatrix* viewMatrix);
	for each (Water* water in m_Water) {
		water->Render();
	}

	if (m_Skybox) {
		fmat4 worldMatrix(1.0);
		worldMatrix = scale(worldMatrix, vec3(5000.0f, 5000.0f, 5000.0f));
		worldMatrix = translate(worldMatrix, vec3(0.0f, 0.0f, 0.0f));
		worldMatrix = toMat4(quat(vec3(0, 0, 0))) * worldMatrix;

		m_SkyboxEffect.Enable();
		m_SkyboxEffect.SetWVP(projectionMatrix * viewMatrix * worldMatrix);
		m_Skybox->Render();
	}

	if (!m_ScreenText.empty()) {
		m_ScreenTextEffect.Enable();
		m_ScreenTextEffect.SetProjection(ortho(0.0f, static_cast<GLfloat>(Window::GetWidth()), 0.0f, static_cast<GLfloat>(Window::GetHeight())));
		for each (auto p in m_ScreenText) {
			p.first->BeginRender();
			for each (ScreenTextInfo text in p.second) {
				m_ScreenTextEffect.SetTextColor(text.Color);
				float x = text.X;
				for each (char c in text.Text) {
					x += p.first->Render(c, x, text.Y, text.Scale);
				}
			}
			p.first->EndRender();
		}
		m_ScreenText.clear();
	}
}

float SceneRenderer::AddText(const std::string& Text, float X, float Y, glm::vec3 Color, Font* font) {
	m_ScreenText[font].push_back(ScreenTextInfo(Text, X, Y, Color, EngineSettings::DefaultScreenTextScale));
	return font->GetLength(Text, EngineSettings::DefaultScreenTextScale) + X;
}

float SceneRenderer::AddText(const string& Text, float X, float Y, float Scale, Font* font) {
	m_ScreenText[font].push_back(ScreenTextInfo(Text, X, Y, EngineSettings::DefaultScreenTextColor, Scale));
	return font->GetLength(Text, Scale) + X;
}

float SceneRenderer::AddText(const string& Text, float X, float Y, glm::vec3 Color, float Scale, Font* font) {
	m_ScreenText[font].push_back(ScreenTextInfo(Text, X, Y, Color, Scale));
	return font->GetLength(Text, Scale) + X;
}

void SceneRenderer::EnableHitboxes() {
	m_HitboxesEnabled = true;
}

void SceneRenderer::DisableHitboxes() {
	m_HitboxesEnabled = false;
}

void SceneRenderer::ToggleHitboxes() {
	m_HitboxesEnabled = !m_HitboxesEnabled;
}
