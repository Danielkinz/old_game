#ifndef RENDERER_H
#define RENDERER_H

#include "Techniques/AnimatedModelTechnique.h"
#include "Techniques/StaticModelTechnique.h"
#include "Techniques/SkyboxTechnique.h"
#include "Techniques/HitboxTechnique.h"
#include "Techniques/ScreenTextTechnique.h"
#include "Techniques/MapTechnique.h"
#include "Techniques/WaterTechnique.h"
#include "Objects/Entity.h"
#include "Objects/Light.h"
#include "Objects/Camera3p.h"
#include "Objects/Models/Skybox.h"
#include "Objects/Models/Map.h"
#include "Objects/Models/Water.h"
#include "UI/Font.h"
#include "EngineSettings.h"
#include <map>

#define DEFAULT_SCREEN_TEXT_COLOR glm::vec3(0.0,0.0,0.0)

class SceneRenderer {
public:
	SceneRenderer();

	void Initialize();

	void SetMap(Map* map);
	void SetSkybox(Skybox* skybox);
	void SetLight(const Light const * light);
	void SetCamera(const Camera const * camera);

	void Add(const Entity* entity);
	void Remove(const Entity* entity);

	void AddWater(const Water* water);
	void RemoveWater(const Water* water);

	void Clear();
	void Render();
	
	float AddText(const std::string& Text, float X, float Y, float Scale = EngineSettings::DefaultFontScale, Font* font = Font::GetDefaultFont());
	float AddText(const std::string& Text, float X, float Y, glm::vec3 Color, Font* font = Font::GetDefaultFont());
	float AddText(const std::string& Text, float X, float Y, glm::vec3 Color, float Scale, Font* font = Font::GetDefaultFont());

	void EnableHitboxes();
	void DisableHitboxes();
	void ToggleHitboxes();

private:
	AnimatedModelTechnique m_AnimatedModelEffect;
	StaticModelTechnique m_StaticModelEffect;
	SkyboxTechnique m_SkyboxEffect;
	HitboxTechnique m_HitboxEffect;
	ScreenTextTechnique m_ScreenTextEffect;
	MapTechnique m_MapEffect;
	WaterTechnique m_WaterEffect;

	bool m_HitboxesEnabled = false;

	struct ScreenTextInfo {
		ScreenTextInfo(const std::string& text, float x, float y, glm::vec3 color, float scale);
		std::string Text;
		float X;
		float Y;
		float Scale;
		glm::vec3 Color;
	};

	std::map<Font*, std::vector<ScreenTextInfo>> m_ScreenText;
	std::map<Model*, std::vector<const Entity*>> m_Entities;
	std::vector<Water*> m_Water;
	Skybox* m_Skybox;
	Map* m_Map;

	Light const * m_Light;
	Camera const * m_Camera;
};

#endif
