#include "StepTimer.h"
/*
// Integer format represents time using 10,000,000 ticks per second.
const unsigned long long StepTimer::TicksPerSecond = 10000000;

StepTimer::StepTimer() :
	m_elapsedTicks(0),
	m_totalTicks(0),
	m_leftOverTicks(0),
	m_frameCount(0),
	m_framesPerSecond(0),
	m_framesThisSecond(0),
	m_qpcSecondCounter(0),
	m_isFixedTimeStep(false),
	m_targetElapsedTicks(TicksPerSecond / 60) {
	if (!QueryPerformanceFrequency(&m_qpcFrequency)) {
		throw dcl::tools::Exception("Performance frequency query failed!");
	}

	if (!QueryPerformanceCounter(&m_qpcLastTime)) {
		throw dcl::tools::Exception("Performance counter query failed!");
	}

	// Initialize max delta to 1/10 of a second.
	m_qpcMaxDelta = m_qpcFrequency.QuadPart / 10;
}

// Get elapsed time since the previous Update call.
unsigned long long StepTimer::GetElapsedTicks() const { 
	return m_elapsedTicks; 
}

double StepTimer::GetElapsedSeconds() const { 
	return TicksToSeconds(m_elapsedTicks); 
}

// Get total time since the start of the program.
unsigned long long StepTimer::GetTotalTicks() const { 
	return m_totalTicks; 
}

double StepTimer::GetTotalSeconds() const { 
	return TicksToSeconds(m_totalTicks); 
}

// Get total number of updates since start of the program.
unsigned int StepTimer::GetFrameCount() const { 
	return m_frameCount; 
}

// Get the current framerate.
unsigned int StepTimer::GetFramesPerSecond() const { 
	return m_framesPerSecond; 
}

// Set whether to use fixed or variable timestep mode.
void StepTimer::SetFixedTimeStep(bool isFixedTimestep) {
	m_isFixedTimeStep = isFixedTimestep; 
}

// Set how often to call Update when in fixed timestep mode.
void StepTimer::SetTargetElapsedTicks(unsigned long long targetElapsed) {
	m_targetElapsedTicks = targetElapsed; 
}

void StepTimer::SetTargetElapsedSeconds(double targetElapsed) { 
	m_targetElapsedTicks = SecondsToTicks(targetElapsed); 
}

double StepTimer::TicksToSeconds(unsigned long long ticks) { 
	return static_cast<double>(ticks) / TicksPerSecond; 
}

unsigned long long StepTimer::SecondsToTicks(double seconds) { 
	return static_cast<unsigned long long>(seconds * TicksPerSecond); 
}

// After an intentional timing discontinuity (for instance a blocking IO operation)
// call this to avoid having the fixed timestep logic attempt a set of catch-up 
// Update calls.

void StepTimer::ResetElapsedTime() {
	if (!QueryPerformanceCounter(&m_qpcLastTime)) {
		throw dcl::tools::Exception("Performance counter query failed!");
	}

	m_leftOverTicks = 0;
	m_framesPerSecond = 0;
	m_framesThisSecond = 0;
	m_qpcSecondCounter = 0;
}
*/