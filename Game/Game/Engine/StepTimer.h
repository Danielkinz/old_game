#ifndef STEP_TIMER_H
#define STEP_TIMER_H
/*
//#include <wrl.h>
#include <dcl\tools\Exception.h>

// Helper class for animation and simulation timing.
class StepTimer {
public:
	StepTimer();

	// Get elapsed time since the previous Update call.
	unsigned long long GetElapsedTicks() const;
	double GetElapsedSeconds() const;

	// Get total time since the start of the program.
	unsigned long long GetTotalTicks() const;
	double GetTotalSeconds() const;

	// Get total number of updates since start of the program.
	unsigned int GetFrameCount() const;

	// Get the current framerate.
	unsigned int GetFramesPerSecond() const;

	// Set whether to use fixed or variable timestep mode.
	void SetFixedTimeStep(bool isFixedTimestep);

	// Set how often to call Update when in fixed timestep mode.
	void SetTargetElapsedTicks(unsigned long long targetElapsed);
	void SetTargetElapsedSeconds(double targetElapsed);

	static double TicksToSeconds(unsigned long long ticks);
	static unsigned long long SecondsToTicks(double seconds);

	// After an intentional timing discontinuity (for instance a blocking IO operation)
	// call this to avoid having the fixed timestep logic attempt a set of catch-up 
	// Update calls.

	void ResetElapsedTime();

	// Update timer state, calling the specified Update function the appropriate number of times.
	template<typename TUpdate>
	void Tick(const TUpdate& update);

	// Integer format represents time using 10,000,000 ticks per second.
	static const unsigned long long TicksPerSecond;

private:
	// Source timing data uses QPC units.
	//LARGE_INTEGER m_qpcFrequency;
	//LARGE_INTEGER m_qpcLastTime;
	unsigned long long m_qpcMaxDelta;

	// Derived timing data uses a canonical tick format.
	unsigned long long m_elapsedTicks;
	unsigned long long m_totalTicks;
	unsigned long long m_leftOverTicks;

	// Members for tracking the framerate.
	unsigned int m_frameCount;
	unsigned int m_framesPerSecond;
	unsigned int m_framesThisSecond;
	unsigned long long m_qpcSecondCounter;

	// Members for configuring fixed timestep mode.
	bool m_isFixedTimeStep;
	unsigned long long m_targetElapsedTicks;
};

template<typename TUpdate>
void StepTimer::Tick(const TUpdate& update) {
	// Query the current time.
	LARGE_INTEGER currentTime;

	if (!QueryPerformanceCounter(&currentTime)) {
		throw dcl::tools::Exception("Performance counter query failed");
	}

	unsigned long long  timeDelta = currentTime.QuadPart - m_qpcLastTime.QuadPart;

	m_qpcLastTime = currentTime;
	m_qpcSecondCounter += timeDelta;

	// Clamp excessively large time deltas (e.g. after paused in the debugger).
	if (timeDelta > m_qpcMaxDelta) {
		timeDelta = m_qpcMaxDelta;
	}

	// Convert QPC units into a canonical tick format. This cannot overflow due to the previous clamp.
	timeDelta *= TicksPerSecond;
	timeDelta /= m_qpcFrequency.QuadPart;

	unsigned int  lastFrameCount = m_frameCount;

	if (m_isFixedTimeStep) {
		// Fixed timestep update logic

		// If the app is running very close to the target elapsed time (within 1/4 of a millisecond) just clamp
		// the clock to exactly match the target value. This prevents tiny and irrelevant errors
		// from accumulating over time. Without this clamping, a game that requested a 60 fps
		// fixed update, running with vsync enabled on a 59.94 NTSC display, would eventually
		// accumulate enough tiny errors that it would drop a frame. It is better to just round 
		// small deviations down to zero to leave things running smoothly.

		if (abs(static_cast<long long>(timeDelta - m_targetElapsedTicks)) < TicksPerSecond / 4000) {
			timeDelta = m_targetElapsedTicks;
		}

		m_leftOverTicks += timeDelta;

		while (m_leftOverTicks >= m_targetElapsedTicks) {
			m_elapsedTicks = m_targetElapsedTicks;
			m_totalTicks += m_targetElapsedTicks;
			m_leftOverTicks -= m_targetElapsedTicks;
			m_frameCount++;

			update();
		}
	} else {
		// Variable timestep update logic.
		m_elapsedTicks = timeDelta;
		m_totalTicks += timeDelta;
		m_leftOverTicks = 0;
		m_frameCount++;

		update();
	}

	// Track the current framerate.
	if (m_frameCount != lastFrameCount) {
		m_framesThisSecond++;
	}

	if (m_qpcSecondCounter >= static_cast<long long>(m_qpcFrequency.QuadPart)) {
		m_framesPerSecond = m_framesThisSecond;
		m_framesThisSecond = 0;
		m_qpcSecondCounter %= m_qpcFrequency.QuadPart;
	}
}
*/
#endif