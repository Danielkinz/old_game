#include "Storage.h"
#include <dcl\tools\Exception.h>
#include "Utils.h"

using namespace std;
using namespace glm;

SceneRenderer* EngineStorage::Renderer = nullptr;

std::deque<GameObject*> EngineStorage::Objects;
std::list<Model*> EngineStorage::Models;

void EngineStorage::Init() {
	DestroyAll();
}

void EngineStorage::RegisterObject(GameObject* object) {
	if (object == nullptr) throw dcl::tools::Exception("Invalid object registered");

	Objects.push_back(object);
}

void EngineStorage::UnregisterObject(GameObject* object) {
	if (object == nullptr) return;

	for (auto it = Objects.begin(); it != Objects.end(); it++) {
		if (*it == object) {
			Objects.erase(it);
			return;
		}
	}
}

Model* EngineStorage::GetModel(const std::string& path) {
	for (auto it = Models.begin(); it != Models.end(); it++) {
		if ((*it)->GetPath() == path) return *it;
	}
	return nullptr;
}

bool EngineStorage::IsModelLoaded(const string& path) {
	for (auto it = Models.begin(); it != Models.end(); it++) {
		if ((*it)->GetPath() == path) return true;
	}
	return false;
}

void EngineStorage::RegisterModel(Model* model) {
	if (model == nullptr) return;
	Models.push_front(model);
}

void EngineStorage::UnregisterModel(Model* model) {
	if (model == nullptr) return;

	for (auto it = Models.begin(); it != Models.end(); it++) {
		if ((*it)->GetPath() == model->GetPath()) {
			Models.erase(it);
		}
	}
}

void EngineStorage::Update(float dt) {
	for each (auto object in Objects) {
		object->Update(dt);
	}
}

void EngineStorage::DestroyAll() {
	while (!Objects.empty()) {
		delete Objects.front();
	}

	Objects.clear();
	Models.clear();
}
