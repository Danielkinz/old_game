#ifndef STORAGE_H
#define STORAGE_H

#include <list>
#include <deque>
#include <algorithm>
#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>
#include "Objects\Models\StaticModel.h"
#include "Objects\Models\AnimatedModel.h"
#include "Objects\Models\Skybox.h"
#include "Objects\Light.h"
#include "GameObject.h"
#include "StepTimer.h"
#include "Renderer.h"

// A class used to safely create game objects
class EngineStorage {
private:
	static std::deque<GameObject*> Objects;
	static std::list<Model*> Models;

public:
	static void Init();

	static void RegisterObject(GameObject* object);
	static void UnregisterObject(GameObject* object);

	static Model* GetModel(const std::string& path);
	static bool IsModelLoaded(const std::string& path);
	static void RegisterModel(Model* model);
	static void UnregisterModel(Model* model);

	static void Update(float dt);
	static void DestroyAll();

	static SceneRenderer* Renderer;
};

#endif