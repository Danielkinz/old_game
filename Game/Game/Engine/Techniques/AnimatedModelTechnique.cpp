#include "AnimatedModelTechnique.h"
#include "ShaderResources.h"

bool AnimatedModelTechnique::Init() {
	if (!Technique::Init()) return false;
	if (!AddShader(GL_VERTEX_SHADER, ANIMATED_MODEL_VERTEX)) return false;
	if (!AddShader(GL_FRAGMENT_SHADER, ANIMATED_MODEL_FRAGMENT)) return false;
	if (!Finalize()) return false;

	m_WVPLocation = GetUniformLocation("WVP");
	m_WorldLocation = GetUniformLocation("World");
	m_ViewLocation = GetUniformLocation("View");
	m_LightLocation = GetUniformLocation("lightPosition_worldspace");
	m_LightColorLocation = GetUniformLocation("lightColor");
	m_BonesLocation = GetUniformLocation("bones");
	m_ShineDamperLocation = GetUniformLocation("shineDamper");
	m_ReflectivityLocation = GetUniformLocation("reflectivity");

	if (m_WVPLocation == INVALID_UNIFORM_LOCATION ||
		m_WorldLocation == INVALID_UNIFORM_LOCATION ||
		m_ViewLocation == INVALID_UNIFORM_LOCATION ||
		m_LightLocation == INVALID_UNIFORM_LOCATION ||
		m_LightColorLocation == INVALID_UNIFORM_LOCATION ||
		m_BonesLocation == INVALID_UNIFORM_LOCATION ||
		m_ShineDamperLocation == INVALID_UNIFORM_LOCATION ||
		m_ReflectivityLocation == INVALID_UNIFORM_LOCATION) {
		return false;
	}

	return true;
}

void AnimatedModelTechnique::SetLight(const Light& light) {
	glUniform3fv(m_LightLocation, 1, &light.GetLocation()[0]);
	glUniform3fv(m_LightColorLocation, 1, &light.GetColor()[0]);
}

void AnimatedModelTechnique::SetView(const glm::mat4& value) {
	glUniformMatrix4fv(m_ViewLocation, 1, GL_FALSE, &value[0][0]);
}

void AnimatedModelTechnique::SetWorld(const glm::mat4& value) {
	glUniformMatrix4fv(m_WorldLocation, 1, GL_FALSE, &value[0][0]);
}

void AnimatedModelTechnique::SetWVP(const glm::mat4& value) {
	glUniformMatrix4fv(m_WVPLocation, 1, GL_FALSE, &value[0][0]);
}

void AnimatedModelTechnique::SetShineDamper(float value) {
	glUniform1f(m_ShineDamperLocation, value);
}

void AnimatedModelTechnique::SetReflectivity(float value) {
	glUniform1f(m_ReflectivityLocation, value);
}

void AnimatedModelTechnique::SetBones(const std::vector<glm::mat4>& bones) {
	glUniformMatrix4fv(m_BonesLocation, bones.size(), GL_FALSE, &bones[0][0][0]);
}
