#ifndef ANIMATED_MODEL_TECHNIQUE_H
#define ANIMATED_MODEL_TECHNIQUE_H

#include <vector>

#include "Technique.h"
#include "..\Objects\Light.h"

class AnimatedModelTechnique : public Technique {
public:
	virtual bool Init();

	void SetLight(const Light& light);
	void SetView(const glm::mat4& value);
	void SetWorld(const glm::mat4& value);
	void SetWVP(const glm::mat4& value);
	void SetShineDamper(float value);
	void SetReflectivity(float value);
	void SetBones(const std::vector<glm::mat4>& bones);

private:
	GLuint m_WVPLocation;
	GLuint m_WorldLocation;
	GLuint m_ViewLocation;
	GLuint m_LightLocation;
	GLuint m_LightColorLocation;
	GLuint m_BonesLocation;
	GLuint m_ShineDamperLocation;
	GLuint m_ReflectivityLocation;
};

#endif