#ifndef HITBOX_TECHNIQUE_H
#define HITBOX_TECHNIQUE_H

#include "Technique.h"

class HitboxTechnique : public Technique {
public:
	virtual bool Init();

	void SetWVP(const glm::mat4& value);

private:
	GLuint m_WVPLocation;
};

#endif