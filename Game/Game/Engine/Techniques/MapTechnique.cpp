#include "MapTechnique.h"
#include "ShaderResources.h"

bool MapTechnique::Init() {
	if (!Technique::Init()) return false;
	if (!AddShader(GL_VERTEX_SHADER, MAP_VERTEX)) return false;
	if (!AddShader(GL_FRAGMENT_SHADER, MAP_FRAGMENT)) return false;
	if (!Finalize()) return false;

	/*m_ViewLocation = GetUniformLocation("View");
	m_LightLocation = GetUniformLocation("lightPosition_worldspace");
	m_LightColorLocation = GetUniformLocation("lightColor");
	m_ShineDamperLocation = GetUniformLocation("shineDamper");
	m_ReflectivityLocation = GetUniformLocation("reflectivity");*/
	m_VPLocation = GetUniformLocation("VP");
	//glUniform1i(GetUniformLocation("grass"), 0);
	//glUniform1i(GetUniformLocation("sand"), 1);

	if (/*m_ViewLocation == INVALID_UNIFORM_LOCATION ||
		m_LightLocation == INVALID_UNIFORM_LOCATION ||
		m_LightColorLocation == INVALID_UNIFORM_LOCATION ||
		m_ShineDamperLocation == INVALID_UNIFORM_LOCATION ||
		m_ReflectivityLocation == INVALID_UNIFORM_LOCATION ||*/
		m_VPLocation == INVALID_UNIFORM_LOCATION) {
		return false;
	}

	return true;
}

void MapTechnique::SetView(const glm::mat4& value) {
	//glUniformMatrix4fv(m_ViewLocation, 1, GL_FALSE, &value[0][0]);
}

void MapTechnique::SetLight(const Light& light) {
	//glUniform3fv(m_LightLocation, 1, &light.GetLocation()[0]);
	//glUniform3fv(m_LightColorLocation, 1, &light.GetColor()[0]);
}

void MapTechnique::SetShineDamper(float value) {
	//glUniform1f(m_ShineDamperLocation, value);
}

void MapTechnique::SetReflectivity(float value) {
	//glUniform1f(m_ReflectivityLocation, value);
}

void MapTechnique::SetVP(const glm::mat4& value) {
	glUniformMatrix4fv(m_VPLocation, 1, GL_FALSE, &value[0][0]);
}
