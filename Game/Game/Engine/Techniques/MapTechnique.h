#ifndef MAP_TECHNIQUE_H
#define MAP_TECHNIQUE_H

#include "Technique.h"
#include "..\Objects\Light.h"

class MapTechnique : public Technique {
public:
	virtual bool Init();

	void SetView(const glm::mat4& value);
	void SetLight(const Light& light);
	void SetShineDamper(float value);
	void SetReflectivity(float value);
	void SetVP(const glm::mat4& value);

private:
	GLuint m_ViewLocation;
	GLuint m_LightLocation;
	GLuint m_LightColorLocation;
	GLuint m_ShineDamperLocation;
	GLuint m_ReflectivityLocation;
	GLuint m_VPLocation;
};

#endif