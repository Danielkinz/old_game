#include "ScreenTextTechnique.h"
#include "ShaderResources.h"

using namespace glm;

bool ScreenTextTechnique::Init() {
	if (!Technique::Init()) return false;
	if (!AddShader(GL_VERTEX_SHADER, SCREEN_TEXT_VERTEX)) return false;
	if (!AddShader(GL_FRAGMENT_SHADER, SCREEN_TEXT_FRAGMENT)) return false;
	if (!Finalize()) return false;

	m_ProjectionLocation = GetUniformLocation("Projection");
	m_TextColorLocation = GetUniformLocation("TextColor");

	if (m_ProjectionLocation == INVALID_UNIFORM_LOCATION ||
		m_TextColorLocation == INVALID_UNIFORM_LOCATION) return false;

	return true;
}

void ScreenTextTechnique::SetProjection(const mat4& value) {
	glUniformMatrix4fv(m_ProjectionLocation, 1, GL_FALSE, &value[0][0]);
}

void ScreenTextTechnique::SetTextColor(const vec3& value) {
	glUniform3fv(m_TextColorLocation, 1, &value[0]);
}
