#ifndef SCREEN_TEXT_TECHNIQUE_H
#define SCREEN_TEXT_TECHNIQUE_H

#include "Technique.h"

class ScreenTextTechnique : public Technique {
public:
	virtual bool Init();

	void SetProjection(const glm::mat4& value);
	void SetTextColor(const glm::vec3& color);

private:
	GLuint m_ProjectionLocation;
	GLuint m_TextColorLocation;
};

#endif