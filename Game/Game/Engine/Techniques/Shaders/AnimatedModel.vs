#version 330 core

const int MAX_BONES = 100;

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal_modelspace;
layout(location = 2) in vec2 texture_coords;
layout(location = 3) in ivec4 boneIDs;
layout(location = 4) in vec4 weights;

out vec3 surfaceNormal;
out vec3 toLightVector;
out vec3 toCameraVector;
out vec2 texture_coords_pass;

uniform mat4 WVP;
uniform mat4 World;
uniform mat4 View;
uniform vec3 lightPosition_worldspace;
uniform mat4 bones[MAX_BONES];

void main() {
	mat4 boneTransform = bones[boneIDs[0]] * weights[0];
	boneTransform += bones[boneIDs[1]] * weights[1];
	boneTransform += bones[boneIDs[2]] * weights[2];
	boneTransform += bones[boneIDs[3]] * weights[3];

    gl_Position = WVP * boneTransform * vec4(vertexPosition_modelspace, 1);

	surfaceNormal = (World * vec4(vertexNormal_modelspace, 0.0)).xyz;
	toLightVector = lightPosition_worldspace - (World * vec4(vertexPosition_modelspace, 1.0)).xyz;
	toCameraVector = (inverse(View) * vec4(0.0,0.0,0.0,1.0)).xyz - (World * vec4(vertexPosition_modelspace, 1)).xyz;

	texture_coords_pass = texture_coords;
}

