#version 330 core

layout(location = 0) in vec3 vertexPosition_modelSpace;

uniform mat4 WVP;

void main() {
	gl_Position = WVP * vec4(vertexPosition_modelSpace, 1); 
}