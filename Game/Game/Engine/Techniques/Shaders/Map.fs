#version 330 core

out vec4 FragColor;

uniform sampler2D grass;
uniform sampler2D sand;

in vec3 color_pass;
in vec3 texture_pass;

void main() {
	if (texture_pass.y < 40) {
		FragColor = 1 * vec4(color_pass, 0) + 0 * texture(sand, vec2(texture_pass.x/50, texture_pass.z/50));
	} else {
		FragColor = 1 * vec4(color_pass, 0) + 0 * texture(grass, vec2(texture_pass.x/50, texture_pass.z/50));
	}
}