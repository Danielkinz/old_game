#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 color_in;

uniform mat4 VP;

out vec3 texture_pass;
out vec3 color_pass;

void main() {
	color_pass = color_in;
	texture_pass = vertexPosition_modelspace;
    gl_Position = VP * vec4(vertexPosition_modelspace, 1);
}

