#version 330 core

layout (location = 0) in vec3 Pos;

uniform mat4 WVP;

out vec3 TexCoords;

void main()
{
    TexCoords = Pos;
    vec4 pos = WVP * vec4(Pos, 1.0);
    gl_Position = pos.xyww;
}  