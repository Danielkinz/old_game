#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal_modelspace;
layout(location = 2) in vec2 texture_coords;
layout(location = 3) in mat4 WVP;
layout(location = 7) in mat4 World;

out vec3 surfaceNormal;
out vec3 toLightVector;
out vec3 toCameraVector;
out vec2 texture_coords_pass;

uniform mat4 View;
uniform vec3 lightPosition_worldspace;

void main() {
    gl_Position = WVP * vec4(vertexPosition_modelspace, 1);

	surfaceNormal = (World * vec4(vertexNormal_modelspace, 0.0)).xyz;
	toLightVector = lightPosition_worldspace - (World * vec4(vertexPosition_modelspace, 1.0)).xyz;
	toCameraVector = (inverse(View) * vec4(0.0,0.0,0.0,1.0)).xyz - (World * vec4(vertexPosition_modelspace, 1)).xyz;

	texture_coords_pass = texture_coords;
}

