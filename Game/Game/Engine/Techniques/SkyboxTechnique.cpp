#include "SkyBoxTechnique.h"
#include "ShaderResources.h"

bool SkyboxTechnique::Init() {
	if (!Technique::Init()) return false;
	if (!AddShader(GL_VERTEX_SHADER, SKYBOX_VERTEX)) return false;
	if (!AddShader(GL_FRAGMENT_SHADER, SKYBOX_FRAGMENT)) return false;
	if (!Finalize()) return false;

	m_WVPLocation = GetUniformLocation("WVP");

	if (m_WVPLocation == INVALID_UNIFORM_LOCATION) return false;

	return true;
}

void SkyboxTechnique::SetWVP(const glm::mat4& value) {
	glUniformMatrix4fv(m_WVPLocation, 1, GL_FALSE, &value[0][0]);
}