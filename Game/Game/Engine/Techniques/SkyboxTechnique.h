#ifndef SKYBOX_TECHNIQUE_H
#define SKYBOX_TECHNIQUE_H

#include "Technique.h"

class SkyboxTechnique : public Technique {
public:
	virtual bool Init();

	void SetWVP(const glm::mat4& value);

private:
	GLuint m_WVPLocation;
};

#endif