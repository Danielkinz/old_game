#include "StaticModelTechnique.h"
#include "ShaderResources.h"

bool StaticModelTechnique::Init() {
	if (!Technique::Init()) return false;
	if (!AddShader(GL_VERTEX_SHADER, STATIC_MODEL_VERTEX)) return false;
	if (!AddShader(GL_FRAGMENT_SHADER, STATIC_MODEL_FRAGMENT)) return false;
	if (!Finalize()) return false;

	m_ViewLocation = GetUniformLocation("View");
	m_LightLocation = GetUniformLocation("lightPosition_worldspace");
	m_LightColorLocation = GetUniformLocation("lightColor");
	m_ShineDamperLocation = GetUniformLocation("shineDamper");
	m_ReflectivityLocation = GetUniformLocation("reflectivity");

	if (m_ViewLocation == INVALID_UNIFORM_LOCATION ||
		m_LightLocation == INVALID_UNIFORM_LOCATION ||
		m_LightColorLocation == INVALID_UNIFORM_LOCATION ||
		m_ShineDamperLocation == INVALID_UNIFORM_LOCATION ||
		m_ReflectivityLocation == INVALID_UNIFORM_LOCATION) {
		return false;
	}

	return true;
}

void StaticModelTechnique::SetView(const glm::mat4& value) {
	glUniformMatrix4fv(m_ViewLocation, 1, GL_FALSE, &value[0][0]);
}

void StaticModelTechnique::SetLight(const Light& light) {
	glUniform3fv(m_LightLocation, 1, &light.GetLocation()[0]);
	glUniform3fv(m_LightColorLocation, 1, &light.GetColor()[0]);
}

void StaticModelTechnique::SetShineDamper(float value) {
	glUniform1f(m_ShineDamperLocation, value);
}

void StaticModelTechnique::SetReflectivity(float value) {
	glUniform1f(m_ReflectivityLocation, value);
}
