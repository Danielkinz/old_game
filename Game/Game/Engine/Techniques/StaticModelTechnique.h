#ifndef STATIC_MODEL_TECHNIQUE_H
#define STATIC_MODEL_TECHNIQUE_H

#include "Technique.h"
#include "..\Objects\Light.h"

class StaticModelTechnique : public Technique {
public:
	virtual bool Init();

	void SetView(const glm::mat4& value);
	void SetLight(const Light& light);
	void SetShineDamper(float value);
	void SetReflectivity(float value);

private:
	GLuint m_ViewLocation;
	GLuint m_LightLocation;
	GLuint m_LightColorLocation;
	GLuint m_ShineDamperLocation;
	GLuint m_ReflectivityLocation;
};

#endif