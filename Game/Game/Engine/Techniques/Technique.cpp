#include <stdio.h>
#include <string.h>
#include "..\Utils.h"

#include "technique.h"

using namespace std;
using namespace glm;

Technique::Technique() {
	m_shaderProg = 0;
}

Technique::~Technique() {
	// deletes unlinked shaders
	for (auto it = m_shaderObjList.begin(); it != m_shaderObjList.end(); it++) {
		glDeleteShader(*it);
	}

	// delete the program
	if (m_shaderProg) {
		glDeleteProgram(m_shaderProg);
		m_shaderProg = 0;
	}
}

void Technique::Enable() {
	glUseProgram(m_shaderProg);
}

bool Technique::Init() {
	m_shaderProg = glCreateProgram();

	if (m_shaderProg == 0) {
		fprintf(stderr, "Error creating shader program\n");
		return false;
	}

	return true;
}

bool Technique::AddShader(GLenum ShaderType, const std::string& script) {
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
		return false;
	}

	// Save the shader object - will be deleted in the destructor
	m_shaderObjList.push_back(ShaderObj);

	const GLchar* p[1];
	p[0] = script.c_str();
	GLint Lengths[1] = { (GLint) script.size() };

	glShaderSource(ShaderObj, 1, p, Lengths);

	glCompileShader(ShaderObj);

	GLint success;
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);

	if (!success) {
		GLchar InfoLog[1024];
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader script: '%s'\n", InfoLog);
		return false;
	}

	glAttachShader(m_shaderProg, ShaderObj);

	return true;
}

bool Technique::AddShader(GLenum ShaderType, const char* pFilename) {
	string s;

	if (!dcl::tools::ReadFile(pFilename, s)) {
		return false;
	}

	return AddShader(ShaderType, s);
}

#ifdef _WIN32
#include <Windows.h>
#include "ShaderResources.h"
bool Technique::AddShader(GLenum ShaderType, int resource) {
	HMODULE handle = ::GetModuleHandle(NULL);
	HRSRC rc = ::FindResource(handle, MAKEINTRESOURCE(resource), MAKEINTRESOURCE(SHADER));
	HGLOBAL rcData = ::LoadResource(handle, rc);
	DWORD size = ::SizeofResource(handle, rc);
	const char* data = static_cast<const char*>(::LockResource(rcData));
	return AddShader(ShaderType, string(data, data + size));
}
#endif

// After all the shaders have been added to the program call this function
// to link and validate the program.
bool Technique::Finalize() {
	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	glLinkProgram(m_shaderProg);

	glGetProgramiv(m_shaderProg, GL_LINK_STATUS, &Success);
	if (Success == 0) {
		glGetProgramInfoLog(m_shaderProg, sizeof(ErrorLog), NULL, ErrorLog);
		fprintf(stderr, "Error linking shader program: '%s'\n", ErrorLog);
		return false;
	}

	glValidateProgram(m_shaderProg);
	glGetProgramiv(m_shaderProg, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(m_shaderProg, sizeof(ErrorLog), NULL, ErrorLog);
		fprintf(stderr, "Invalid shader program: '%s'\n", ErrorLog);
		//   return false;
	}

	// Delete the intermediate shader objects that have been added to the program
	for (auto it = m_shaderObjList.begin(); it != m_shaderObjList.end(); it++) {
		glDeleteShader(*it);
	}

	m_shaderObjList.clear();

	GLExceptionIfError();
	return true;
}

inline void Technique::SetUniform(const string & name, const float& value) const {
	glUniform1f(glGetUniformLocation(m_shaderProg, name.c_str()), value);
}

inline void Technique::SetUniform(const string & name, const int& value) const {
	glUniform1i(glGetUniformLocation(m_shaderProg, name.c_str()), value);
}

inline void Technique::SetUniform(const string & name, const bool& value) const {
	glUniform1i(glGetUniformLocation(m_shaderProg, name.c_str()), value ? 1 : 0);
}

inline void Technique::SetUniform(const string & name, const vec2& value) const {
	glUniform2fv(glGetUniformLocation(m_shaderProg, name.c_str()), 1, &value[0]);
}

inline void Technique::SetUniform(const string & name, const vec3& value) const {
	glUniform3fv(glGetUniformLocation(m_shaderProg, name.c_str()), 1, &value[0]);
}

inline void Technique::SetUniform(const string & name, const vec4& value) const {
	glUniform4fv(glGetUniformLocation(m_shaderProg, name.c_str()), 1, &value[0]);
}

inline void Technique::SetUniform(const string & name, const mat4& value) const {
	glUniformMatrix4fv(glGetUniformLocation(m_shaderProg, name.c_str()), 1, GL_FALSE, &value[0][0]);
}

inline void Technique::SetUniform(const string& name, const GLuint& value) const {
	glUniform1ui(glGetUniformLocation(m_shaderProg, name.c_str()), value);
}

inline void Technique::SetUniform(const string& name, const mat4* values, uint amount) const {
	glUniformMatrix4fv(glGetUniformLocation(m_shaderProg, name.c_str()), amount, GL_FALSE, &values[0][0][0]);
}

GLint Technique::GetUniformLocation(const char* pUniformName) {
	GLuint Location = glGetUniformLocation(m_shaderProg, pUniformName);

	if (Location == INVALID_UNIFORM_LOCATION) {
		fprintf(stderr, "Warning! Unable to get the location of uniform '%s'\n", pUniformName);
	}

	return Location;
}

GLint Technique::GetProgramParam(GLint param) {
	GLint ret;
	glGetProgramiv(m_shaderProg, param, &ret);
	return ret;
}
