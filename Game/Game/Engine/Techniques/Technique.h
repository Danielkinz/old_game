#ifndef TECHNIQUE_H
#define TECHNIQUE_H

#include <list>
#include <GL\glew.h>
#include <glm\glm.hpp>
#include "..\Math.h"

#define INVALID_UNIFORM_LOCATION 0xFFFFFFFF

class Technique {
public:
	virtual ~Technique();
	void Enable();

protected:
	Technique();
	virtual bool Init();

	bool AddShader(GLenum ShaderType, const std::string& script);
	bool AddShader(GLenum ShaderType, const char* pFilename);

#ifdef _WIN32
	bool AddShader(GLenum ShaderType, int resource);
#endif

	bool Finalize();

	inline void SetUniform(const std::string& name, const float& value) const;
	inline void SetUniform(const std::string& name, const int& value) const;
	inline void SetUniform(const std::string& name, const bool& value) const;
	inline void SetUniform(const std::string& name, const glm::vec2& value) const;
	inline void SetUniform(const std::string& name, const glm::vec3& value) const;
	inline void SetUniform(const std::string& name, const glm::vec4& value) const;
	inline void SetUniform(const std::string& name, const glm::mat4& value) const;
	inline void SetUniform(const std::string& name, const GLuint& value) const;
	inline void SetUniform(const std::string& name, const glm::mat4* values, uint amount) const;

	GLint GetUniformLocation(const char* pUniformName);
	GLint GetProgramParam(GLint param);
	GLuint m_shaderProg;

private:
	std::list<GLuint> m_shaderObjList;
};

#endif