#include "WaterTechnique.h"
#include "ShaderResources.h"

bool WaterTechnique::Init() {
	if (!Technique::Init()) return false;
	if (!AddShader(GL_VERTEX_SHADER, WATER_VERTEX)) return false;
	if (!AddShader(GL_FRAGMENT_SHADER, WATER_FRAGMENT)) return false;
	if (!Finalize()) return false;

	m_VPLocation = GetUniformLocation("VP");

	if (m_VPLocation == INVALID_UNIFORM_LOCATION) {
		return false;
	}

	return true;
}

void WaterTechnique::SetVP(const glm::mat4& value) {
	glUniformMatrix4fv(m_VPLocation, 1, GL_FALSE, &value[0][0]);
}
