#ifndef WATER_TECHNIQUE_H
#define WATER_TECHNIQUE_H

#include "Technique.h"
#include "..\Objects\Light.h"

class WaterTechnique : public Technique {
public:
	virtual bool Init();

	void SetVP(const glm::mat4& value);

private:
	GLuint m_VPLocation;
};

#endif