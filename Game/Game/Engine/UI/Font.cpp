#include "Font.h"
#include <ft2build.h>
#include FT_FREETYPE_H
#include <dcl\tools\Exception.h>
#include "..\EngineSettings.h"

#define CHARACTERS_AMOUNT 128

using namespace std;
using namespace glm;

Font* Font::DefaultFont = nullptr;
GLuint Font::m_VAO = 0;
GLuint Font::m_VBO = 0;

Font* Font::GetDefaultFont() {
	return DefaultFont;
}

void Font::Init() {
	glGenVertexArrays(1, &m_VAO);
	glGenBuffers(1, &m_VBO);
	glBindVertexArray(m_VBO);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	DefaultFont = new Font();
	DefaultFont->Load(EngineSettings::DefaultFontPath);
}

void Font::End() {
	delete DefaultFont;
}

Font::~Font() {
	for each (Character c in m_Characters) {
		glDeleteTextures(1, &c.TextureID);
	}
}

void Font::Load(const string& filename) {
	FT_Library ft;
	if (FT_Init_FreeType(&ft))
		throw dcl::tools::Exception("Could not init freetype library");

	FT_Face face;
	if (FT_New_Face(ft, filename.c_str(), 0, &face)) {
		FT_Done_FreeType(ft);
		throw dcl::tools::Exception("Could not load font '", filename, "'");
	}

	FT_Set_Pixel_Sizes(face, 0, 48);

	m_Characters.resize(CHARACTERS_AMOUNT);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1); // Disable byte-alignment restriction

	for (GLubyte c = 0; c < CHARACTERS_AMOUNT; c++) {
		// Load character glyph 
		if (FT_Load_Char(face, c, FT_LOAD_RENDER))
			continue;

		// Generate texture
		GLuint texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(
			GL_TEXTURE_2D,
			0,
			GL_RED,
			face->glyph->bitmap.width,
			face->glyph->bitmap.rows,
			0,
			GL_RED,
			GL_UNSIGNED_BYTE,
			face->glyph->bitmap.buffer
		);
		// Set texture options
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		// Now store character for later use
		Character character = {
			texture,
			glm::ivec2(face->glyph->bitmap.width, face->glyph->bitmap.rows),
			glm::ivec2(face->glyph->bitmap_left, face->glyph->bitmap_top),
			face->glyph->advance.x
		};
		m_Characters[c] = character;
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);
}

void Font::BeginRender() {
	glBindVertexArray(m_VAO);
	glActiveTexture(GL_TEXTURE0);

	m_CullFacesState = glIsEnabled(GL_CULL_FACE);
	m_BlendState = glIsEnabled(GL_BLEND);
	glGetIntegerv(GL_BLEND_SRC, &m_BlendSrc);
	glGetIntegerv(GL_BLEND_DST, &m_BlendDst);

	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

float Font::Render(char character, float x, float y, float Scale) {
	Character ch = m_Characters[character];

	GLfloat xpos = x + ch.Bearing.x * Scale;
	GLfloat ypos = y - (ch.Size.y - ch.Bearing.y) * Scale;

	GLfloat w = ch.Size.x * Scale;
	GLfloat h = ch.Size.y * Scale;
	// Update VBO for each character
	GLfloat vertices[6][4] = {
		{ xpos,     ypos + h,   0.0, 0.0 },
		{ xpos,     ypos,       0.0, 1.0 },
		{ xpos + w, ypos,       1.0, 1.0 },

		{ xpos,     ypos + h,   0.0, 0.0 },
		{ xpos + w, ypos,       1.0, 1.0 },
		{ xpos + w, ypos + h,   1.0, 0.0 }
	};

	glBindTexture(GL_TEXTURE_2D, ch.TextureID);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices); // Be sure to use glBufferSubData and not glBufferData

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	return (ch.Advance >> 6) * Scale; // Bitshift by 6 to get value in pixels (2^6 = 64 (divide amount of 1/64th pixels by 64 to get amount of pixels))
}

void Font::EndRender() {
	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	if (m_BlendState) glEnable(GL_BLEND); else glDisable(GL_BLEND);
	if (m_CullFacesState) glEnable(GL_CULL_FACE); else glDisable(GL_CULL_FACE);
	glBlendFunc(m_BlendSrc, m_BlendDst);
}

float Font::GetLength(const std::string& Str, float Scale) {
	float advance = 0;
	for each (char c in Str) {
		advance += (m_Characters[c].Advance >> 6) * Scale;
	}
	return advance;
}
