#ifndef FONT_H
#define FONT_H

#include <GL\glew.h>
#include <glm\glm.hpp>
#include <vector>

class Font {
public:
	~Font();
	void Load(const std::string& filename);

	void BeginRender();
	float Render(char character, float x, float y, float Scale = 1.0);
	void EndRender();

	float GetLength(const std::string& Str, float Scale = 1.0);

	static Font* GetDefaultFont();
	static void Init();
	static void End();

private:
	struct Character {
		GLuint TextureID = 0;
		glm::ivec2 Size;
		glm::ivec2 Bearing;
		GLuint Advance;
	};

	std::vector<Character> m_Characters;

	static Font* DefaultFont;

	GLboolean m_CullFacesState = GL_FALSE;
	GLboolean m_BlendState = GL_FALSE;
	GLint m_BlendSrc;
	GLint m_BlendDst;

	static GLuint m_VAO;
	static GLuint m_VBO;
};

#endif