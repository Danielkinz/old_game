#ifndef UTILS_H
#define UTILS_H
#include <dcl\Tools.h>

using namespace std;

#define INVALID_OGL_VALUE 0xFFFFFFFF

#define GLErrored() (glGetError() != GL_NO_ERROR)
#define GLExceptionIfError() \
	GLuint _error = glGetError(); \
	if (_error != GL_NO_ERROR) { \
		throw dcl::tools::Exception("OpenGL error has occured: ", _error); \
	}

#define GLExitIfError                                                          \
{                                                                               \
    GLenum Error = glGetError();                                                \
                                                                                \
    if (Error != GL_NO_ERROR) {                                                 \
        printf("OpenGL error in %s:%d: 0x%x\n", __FILE__, __LINE__, Error);     \
        exit(0);                                                                \
    }                                                                           \
} 

#define ASSIMP_LOAD_FLAGS (aiProcess_Triangulate | aiProcess_GenSmoothNormals /*| aiProcess_FlipUVs*/)

#endif
