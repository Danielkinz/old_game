#include "Game.h"
#include "GameData.h"
#include <boost\math\constants\constants.hpp>

using namespace std;
using namespace glm;

#define ONLINE_FRAME_DURATION 0.02
#define RENDER_PLAYER_FROM_SERVER false
#define GRAVITY_ACCELERATION -8
#define SPEED_MULTIPLIER_RUNNING 70
#define SPEED_MULTIPLIER_BACK 20
#define SPEED_MULTIPLIER_SIDE 20

Game::Game(int argc, char* argv[], const std::string& windowTitle, int width, int height)
	: Engine(windowTitle, width, height), server(nullptr)
	, m_LocationSendScheduler(std::bind(&Game::SendUpdate, this))
	, m_UpdateFrameScheduler(std::bind(&Game::UpdateOnlineFrame, this)) {

	uint host = 2130706433; // 127.0.0.1
	unsigned short port = 5302;

	if (argc < 5) throw dcl::tools::Exception("Syntax: Game <SessionID> <Character_Name> <host> <port>");

	SESSION_ID = argv[1];
	CHARACTER_NAME = argv[2];
	host = stoul(argv[3]);
	port = stoul(argv[4]);

	if (SESSION_ID.length() != SESSION_LENGTH) throw dcl::tools::Exception("Invalid session id!");

	GameServer::init();
	server = new GameServer(service, host, port);
	ServerThread = new thread(&Game::ServerThreadFunction, this);
}

void Game::ServerThreadFunction() {
	service.run();
}

void Game::Initialize() {
	vector<string> skyboxFaces = {
		"Resources\\Skybox\\Daylight_Box_Right.bmp",
		"Resources\\Skybox\\Daylight_Box_Left.bmp",
		"Resources\\Skybox\\Daylight_Box_Top.bmp",
		"Resources\\Skybox\\Daylight_Box_Bottom.bmp",
		"Resources\\Skybox\\Daylight_Box_Front.bmp",
		"Resources\\Skybox\\Daylight_Box_Back.bmp"
	};

	knightModel = Model::LoadModel("Resources/knight_animated.fbx");
	mapModel = new Map("Resources/heightmap.png");

	character = new Entity(knightModel, 0, 0, 0, 0, 0, 0, 1);
	water = new Water(vec2(-5000, -5000), vec2(5000, 5000), 0.0f);

	character->SetRunningAnimation(0);

	camera = new Camera3p(character);
	light = new Light(vec3(1000, 2000, 1000), vec3(1, 1, 1), 50.0f);
	skybox = Skybox::LoadSkybox(skyboxFaces);

	CameraDirection = camera->GetDirection();

	Renderer.SetCamera(camera);
	Renderer.SetLight(light);
	Renderer.AddWater(water);
	Renderer.Add(dragon);
	Renderer.Add(character);
	Renderer.SetSkybox(skybox);
	Renderer.SetMap(mapModel);

	CurrentOnlineFrame.startTime = GetTime();
	NextOnlineFrame.startTime = GetTime();

	m_LocationSendScheduler.RunTaskTimer(ONLINE_FRAME_DURATION);
	m_UpdateFrameScheduler.RunTaskTimer(ONLINE_FRAME_DURATION);
	GameData::Running = true;
}

void Game::DrawPlayers() {
	vec3 location;
	float yaw;
	EntityInfo current;
	EntityInfo next;
	float delta = (GetTime() - CurrentOnlineFrame.startTime) / (NextOnlineFrame.startTime - CurrentOnlineFrame.startTime);
	if (delta > 1) delta = 1;
	if (delta < 0) delta = 0;

	// If the player list was updated - delete the current entities and create new ones
	if (GameData::PlayerListUpdated) {
		GameData::PlayersMutex.lock();

		// Remove old entities
		for (auto a : playerEntities) {
			if (a.second.first.GetName() != GameData::CharacterName || RENDER_PLAYER_FROM_SERVER)
				Renderer.Remove(a.second.second);
		}

		// Insert new entities
		for (auto a : PLAYERS) {
			playerEntities.insert(
				pair<byte, pair<Player, Entity*>>(
					a.first,
					pair<Player, Entity*>(a.second, new Entity(knightModel))
					)
			);

			if (a.second.GetName() != GameData::CharacterName || RENDER_PLAYER_FROM_SERVER)
				Renderer.Add(playerEntities[a.first].second);
		}

		GameData::PlayerListUpdated = false;
		GameData::PlayersMutex.unlock();
	}

	// update the players positions
	for (auto a : playerEntities) {
		current = CurrentOnlineFrame.players[a.first];
		next = NextOnlineFrame.players[a.first];

		// Calculate interpolation
		location = vec3(current.X, current.Y, current.Z) + vec3(next.X - current.X, next.Y - current.Y, next.Z - current.Z)*delta;
		yaw = ((2 * boost::math::constants::pi<float>() * current.Yaw) / (255)) +
			((((2 * boost::math::constants::pi<float>() * next.Yaw) / (255)) -
			((2 * boost::math::constants::pi<float>() * current.Yaw) / (255)))
				* delta);

		// Update entity
		playerEntities[a.first].second->SetLocation(location);
		playerEntities[a.first].second->SetRotation(vec3(0, yaw, 0));
	}
}

void Game::Update() {
	float nextX;
	MoveCharacter();
	DrawPlayers();

	/*if (Keyboard::IsKeyDown(GLFW_KEY_F1)) {
		Renderer.EnableHitboxes();
	} else {
		Renderer.DisableHitboxes();
	}*/

	if (Keyboard::IsKeyDown(GLFW_KEY_F2)) {
		nextX = Renderer.AddText("FPS: ", 20, Window::GetHeight() - 50);
		Renderer.AddText(to_string(1.0 / GetDT()), nextX, Window::GetHeight() - 50);

		nextX = Renderer.AddText("X: ", 20, Window::GetHeight() - 100);
		Renderer.AddText(to_string(character->GetLocation().x), nextX, Window::GetHeight() - 100);

		nextX = Renderer.AddText("Y: ", 20, Window::GetHeight() - 150);
		Renderer.AddText(to_string(character->GetLocation().y), nextX, Window::GetHeight() - 150);

		nextX = Renderer.AddText("Z: ", 20, Window::GetHeight() - 200);
		Renderer.AddText(to_string(character->GetLocation().z), nextX, Window::GetHeight() - 200);
	}

	if (Keyboard::IsKeyDown(GLFW_KEY_ESCAPE)) Close();
}

void Game::Stop() {
	server->disconnect();
	if (!service.stopped()) service.stop();
	if (ServerThread->joinable()) ServerThread->join();
	m_LocationSendScheduler.Join();
	SAFE_DELETE(ServerThread);
	SAFE_DELETE(server);
}

Game::~Game() {}

void Game::MoveCharacter() {
	static bool IsRunning = false;
	static bool IsInAir = false;
	static long CanMoveAfter = -1;

	if (!Window::IsSelected()) return;

	double dt = GetDT();

	if (Keyboard::IsKeyDown(GLFW_KEY_LEFT_ALT)) {
		if (Mouse::GetCursorType() != GLFW_CURSOR_NORMAL) {
			CameraDirection = camera->GetDirection();
			PreviousCursorLocation = vec2(Window::GetWidth() / 2, Window::GetHeight() / 2);
		}
		Mouse::SetCursorType(GLFW_CURSOR_NORMAL);

	} else {
		if (Mouse::GetCursorType() != GLFW_CURSOR_HIDDEN) {
			camera->SetDirection(CameraDirection);
			Mouse::SetMousePos(Window::GetWidth() / 2, Window::GetHeight() / 2);
		}
		Mouse::SetCursorType(GLFW_CURSOR_HIDDEN);

		// Camera adjustments
		Camera3p nextCamera = *camera;
		nextCamera.AddDirection(
			float((Window::GetWidth() / 2 - Mouse::GetMouseX()) * dt * -5),
			float((Window::GetHeight() / 2 - Mouse::GetMouseY()) * dt * -5)
		);

		nextCamera.AddDistance((float)-Mouse::GetScrollY());

		if (CanMoveAfter < GetTime()) {
			// Horizontal character movement
			character->GetSpeed().x = 0;
			character->GetSpeed().z = 0;
			float yaw = character->GetRotation().y;
			if (Keyboard::IsKeyDown(GLFW_KEY_W) && !Keyboard::IsKeyDown(GLFW_KEY_S)) {
				character->AddSpeed(sin(yaw) * dt *  SPEED_MULTIPLIER_RUNNING, 0, cos(yaw) * dt * SPEED_MULTIPLIER_RUNNING);
				IsRunning = true;
				if (!IsInAir) character->SetRunningAnimation(1);
			} else {
				IsRunning = false;
				if (Keyboard::IsKeyDown(GLFW_KEY_S) && !Keyboard::IsKeyDown(GLFW_KEY_W)) character->AddSpeed(sin(yaw) * dt * -SPEED_MULTIPLIER_BACK, 0, cos(yaw) * dt * -SPEED_MULTIPLIER_BACK);
			}

			yaw += radians(90.0f);
			if (Keyboard::IsKeyDown(GLFW_KEY_A) && !Keyboard::IsKeyDown(GLFW_KEY_D)) character->AddSpeed(sin(yaw) * dt *  SPEED_MULTIPLIER_SIDE, 0, cos(yaw) * dt *  SPEED_MULTIPLIER_SIDE);
			else if (Keyboard::IsKeyDown(GLFW_KEY_D) && !Keyboard::IsKeyDown(GLFW_KEY_A)) character->AddSpeed(sin(yaw) * dt * -SPEED_MULTIPLIER_SIDE, 0, cos(yaw) * dt * -SPEED_MULTIPLIER_SIDE);

			// Vertical character location correction
			if (character->GetLocation().y <= mapModel->GetHeight(character->GetLocation().x, character->GetLocation().z)) {
				character->GetAcceleration().y = 0;
				character->GetSpeed().y = 0;
				character->GetLocation().y = mapModel->GetHeight(character->GetLocation().x, character->GetLocation().z);

				if (IsInAir) {
					IsInAir = false;
					character->SetRunningAnimation(4, true);
					CanMoveAfter = GetTime() + 0.5;
				}
			} else {
				character->GetAcceleration().y = GRAVITY_ACCELERATION;
			}


			// Vertical character movement
			if (character->GetLocation().y <= mapModel->GetHeight(character->GetLocation().x, character->GetLocation().z) && Keyboard::IsKeyDown(GLFW_KEY_SPACE)) {
				character->GetSpeed().y = 3;
				character->SetRunningAnimation(5, true);
				CanMoveAfter = GetTime() + 0.5;
				IsInAir = true;
			}
		
			// Finalize character movement
			character->AddSpeed(character->GetAcceleration() * float(dt));
			vec3 targetLocation = character->GetLocation() + character->GetSpeed();
			if (mapModel->GetHeight(targetLocation.x, targetLocation.z) <= 0) {
				character->GetSpeed().x = 0;
				character->GetSpeed().z = 0;
			}
			character->AddLocation(character->GetSpeed());
		}

		// Camera clipping and sliding
		vec3 nextCameraLocation = nextCamera.GetLocation() + nextCamera.GetTarget()->GetLocation();
		while (nextCameraLocation.y < mapModel->GetHeight(nextCameraLocation.x, nextCameraLocation.z)) {
			nextCamera.AddDirection(0, 0.01);
			nextCameraLocation = nextCamera.GetLocation() + nextCamera.GetTarget()->GetLocation();
		}
		character->AddRotation(0, radians((Window::GetWidth() / 2.0 - Mouse::GetMouseX()) * dt * 5), 0);
		*camera = nextCamera;

		// Reset mouse to middle
		Mouse::SetMousePos(Window::GetWidth() / 2, Window::GetHeight() / 2);
		Mouse::SetScroll(0, 0);

		// Temporary animation testing
		for (int i = 0; i < 8; i++) {
			if (Keyboard::IsKeyDown(GLFW_KEY_1 + i)) character->SetRunningAnimation(i, true);
		}
	}
}

void Game::SendUpdate() {
	server->SendCharacterPosition(character->GetLocation(), character->GetRotation().y);
}

void Game::UpdateOnlineFrame() {
	try {
		this->CurrentOnlineFrame = this->NextOnlineFrame;

		this->NextOnlineFrame = GameData::GetNextOnlineFrame();
		this->NextOnlineFrame.startTime = this->CurrentOnlineFrame.startTime + ONLINE_FRAME_DURATION;
	} catch (...) {}
}
