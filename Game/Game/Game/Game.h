#ifndef GAME_H
#define GAME_H

#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include <dcl/tools/Scheduler.h>
#include <boost/asio.hpp>
#include "Net/GameServer.h"
#include "Engine/Engine.h"
#include "Engine/Objects/Models/Water.h"
#include "Engine/Objects/Models/Map.h"
#include "Net/OnlineFrame.h"
#include <thread>

class Game : public Engine {
private:
	Model* knightModel;
	Map* mapModel;
	Entity* character;
	Water* water;
	Entity* dragon;
	Light* light;
	Skybox* skybox;
	Camera3p* camera;

	std::map<byte, std::pair<Player, Entity*>> playerEntities;

	double lastFrameSend;

	dcl::tools::Scheduler<std::function<void()>> m_LocationSendScheduler;
	dcl::tools::Scheduler<std::function<void()>> m_UpdateFrameScheduler;

	boost::asio::io_service service;
	GameServer* server;

	glm::vec2 CameraDirection;
	glm::vec2 PreviousCursorLocation;

	std::thread* ServerThread;

	OnlineFrame CurrentOnlineFrame;
	OnlineFrame NextOnlineFrame;

	void DrawPlayers();
	void MoveCharacter();
	void ServerThreadFunction();
	void SendUpdate();
	void UpdateOnlineFrame();

protected:
	virtual void Initialize();
	virtual void Update();
	virtual void Stop();

public:
	Game(int argc, char* argv[], const std::string& windowTitle, int width, int height);
	virtual ~Game();
};

#endif