#include "GameData.h"

using namespace std;
using namespace boost;
using namespace dcl::tools;

bool GameData::Running = false;

Logger GameData::logger("General");

string GameData::SessionID;
string GameData::CharacterName;

bool GameData::PlayerListUpdated = false;
std::mutex GameData::PlayersMutex;
std::map<const byte, Player> GameData::Players;

std::queue<OnlineFrame> GameData::Frames;
std::mutex GameData::FrameMutex;

OnlineFrame GameData::GetNextOnlineFrame() {
	if (Frames.empty()) throw dcl::tools::Exception("Online Frame Empty");
	OnlineFrame frame = Frames.front();
	Frames.pop();
	return frame;
}

void GameData::InsertOnlineFrame(OnlineFrame& frame) {
	Frames.push(frame);
}

