#ifndef GAMEDATA_H
#define GAMEDATA_H

#include <boost\unordered_map.hpp>
#include <boost\unordered_set.hpp>
#include <dcl\tools\Logger.h>
#include <map>
#include <mutex>
#include <queue>
#include "Net\OnlineFrame.h"
#include "Player.h"
#include "Engine/Objects/Models/Model.h"

// This file is used to share constants and info between the rest of the objects in the program

// Class that allows sharing global info between all the other classes
class GameData final {
public:
	static bool Running;

	static dcl::tools::Logger logger;

	static std::string SessionID;
	static std::string CharacterName;

	static std::mutex PlayersMutex;
	static std::map<const byte, Player> Players;
	static bool PlayerListUpdated;

	static OnlineFrame GetNextOnlineFrame();
	static void InsertOnlineFrame(OnlineFrame& frame);

private:
	static std::queue<OnlineFrame> Frames;
	static std::mutex FrameMutex;
};

// Constants that represent the static members of the class

#define GLOBAL_LOGGER GameData::logger
#define SESSION_ID GameData::SessionID
#define CHARACTER_NAME GameData::CharacterName
#define PLAYERS GameData::Players

// Constants used across the program

#define SESSION_LENGTH 64

#endif