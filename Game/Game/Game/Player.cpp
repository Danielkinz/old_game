#include "Player.h"

Player::Player() : m_Name(""), m_ID(0xff) {}
Player::Player(const std::string& name, byte id) : m_Name(name), m_ID(id) {}

std::string& Player::GetName() {
	return m_Name;
}

byte Player::GetID() {
	return m_ID;
}
