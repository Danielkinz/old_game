#ifndef PLAYER_H
#define PLAYER_H

#include <string>

typedef unsigned char byte;

class Player {
private:
	std::string m_Name;
	byte m_ID;

public:
	Player();
	Player(const std::string& name, byte id);
	std::string& GetName();
	byte GetID();
};

#endif