#include "Game\Game.h"

// Creates the implementation of stb_image
#define STB_IMAGE_IMPLEMENTATION
#include <stb\stb_image.h>
#undef STB_IMAGE_IMPLEMENTATION

using namespace std;
using namespace boost::asio;

int main(int argc, char** argv) {
	try {
		Game(argc, argv, "Game", 1280, 720).Run();
	} catch (dcl::tools::Exception& e) {
		cout << e.what() << endl;
		getchar();
	} catch (...) {
		cout << "An unknown error has occured" << endl;
		getchar();
	}

	return 0;
}
