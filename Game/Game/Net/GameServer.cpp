#include "GameServer.h"
#include "dcl\structures\Buffer.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"
#include "Game\GameData.h"
#include <boost\math\constants\constants.hpp>
#include "Game\Player.h"

using namespace std;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;
using namespace boost::asio;

/* ---------------------------- Types ---------------------------- */

boost::unordered_map<GameServer::MessageType, GameServer::ProtocolHandler> GameServer::m_Handlers;

/* ---------------------------- STATIC ---------------------------- */

void GameServer::init() {
	m_Handlers[MessageType::SESSION] = &GameServer::SessionHandler;
	m_Handlers[MessageType::ONLINE_FRAME_MSG] = &GameServer::OnlineFrameHandler;
	m_Handlers[MessageType::PLAYER_LIST] = &GameServer::PlayerListHandler;
	m_Handlers[MessageType::CHARACTER_NAME_REQ] = &GameServer::CharacterNameHandler;
	m_Handlers[MessageType::DISCONNECT] = &GameServer::DisconnectHandler;
}

/* ---------------------------- OBJECT ---------------------------- */

GameServer::GameServer(io_service& service, unsigned int host, unsigned short port)
	: TCPClient(service, host, port), m_Logger("GameServer") {

	Buffer<byte> buff;
	buff.Write(0);
	TCPClient::sendData(buff);

	TCPClient::startListening();
}

GameServer::GameServer(GameServer& other)
	: TCPClient(other), m_Logger(other.m_Logger) {

	TCPClient::startListening();
}

GameServer& GameServer::operator=(GameServer& other) {
	TCPClient::operator=(other);
	m_Logger = other.m_Logger;
	TCPClient::startListening();
	return *this;
}

void GameServer::sendData(const MessageType code, const string& payload) {
	Buffer<byte> buff((byte*)&code, 1);
	buff.Write((byte*)payload.data(), payload.length());
	TCPClient::sendData(buff);
}

void GameServer::sendData(const string& payload) {
	Buffer<byte> buff((byte*)payload.data(), payload.length());
	TCPClient::sendData(buff);
}

void GameServer::disconnect() {
	sendData(MessageType::DISCONNECT);

	TCPClient::disconnect();
}

/* ---------------------------- Handlers ---------------------------- */

bool GameServer::handleRead(dcl::structures::Buffer<byte>& buffer, const boost::system::error_code& error) {
	Message msg;

	if (error) {
		if (error.value() == 10009 || error.value() == 995) return false; // Disconnected
		else m_Logger.log(StringBuilder("Read error: (", error.value(), ") ", error.message()));
		disconnect();
		return false;
	}

	msg = buildReceivedMessage(buffer);
	(this->*(m_Handlers[(MessageType)msg->getMessageCode()]))(msg);
	return true;
}

GameServer::Message GameServer::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();
	byte length;
	byte id;
	OnlineFrame frame;
	EntityInfo info;
	string name = "";

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::ONLINE_FRAME_MSG:
		// We don't want to process online frames while the engine isn't running
		if (GameData::Running) {
			length = payload.Read<byte>();
			for (uint i = 0; i < length; i++) {
				id = payload.Read<byte>();

				info.X = payload.Read<float>();
				info.Y = payload.Read<float>();
				info.Z = payload.Read<float>();
				info.Yaw = payload.Read<byte>();

				frame.players[id] = info;
			}
			GameData::InsertOnlineFrame(frame);
		}
		break;

	case MessageType::PLAYER_LIST:
		length = payload.Read<byte>();

		// Updates the list in GameData
		GameData::PlayersMutex.lock();
		PLAYERS.clear();
		for (uint i = 0; i < length; i++) {
			id = payload.Read<byte>();
			name = Protocol::ReadStringFromBuffer(payload, payload.Read<byte>());
			PLAYERS.insert(std::pair<const byte, Player>(id, Player(name, id)));
		}
		GameData::PlayerListUpdated = true;
		GameData::PlayersMutex.unlock();

		break;
	case MessageType::CHARACTER_NAME_REQ:
	case MessageType::SESSION:
	case MessageType::DISCONNECT:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void GameServer::SessionHandler(Message& msg) {
	Buffer<byte> buff;
	buff.Write<byte>((byte) SESSION_ID.size());
	buff.Write((byte*)SESSION_ID.data(), SESSION_ID.length());
	sendData(buff);
}

void GameServer::PlayerListHandler(Message& msg) {}
void GameServer::OnlineFrameHandler(Message& msg) {}

void GameServer::DisconnectHandler(Message& msg) {
	m_Logger.log("disconnected");
	disconnect();
}

void GameServer::CharacterNameHandler(Message& msg) {
	Buffer<byte> buff;
	buff.Write<byte>(CHARACTER_NAME.size());
	Protocol::WriteStringToBuffer(buff, CHARACTER_NAME);
	sendData(buff);
}

void GameServer::SendCharacterPosition(float x, float y, float z, float yaw) {
	Buffer<byte> buffer;
	buffer.Write<MessageType>(MessageType::ENTITY_INFO);
	buffer.Write<float>(x)
		.Write<float>(y)
		.Write<float>(z)
		.Write<byte>((255 * yaw) / (2 * boost::math::constants::pi<float>()));
	sendData(buffer);
}

void GameServer::SendCharacterPosition(glm::vec3 location, float yaw) {
	SendCharacterPosition(location.x, location.y, location.z, yaw);
}
