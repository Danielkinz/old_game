#ifndef CONNECTION_H
#define CONNECTION_H

#include <glm\glm.hpp>
#include <queue>
#include <boost\asio.hpp>
#include "Game\Player.h"
#include "dcl\net\Protocol.h"
#include "dcl\net\Messages.h"
#include "dcl\net\TCPClient.h"
#include "dcl\tools\Logger.h"
#include "OnlineFrame.h"

typedef unsigned char byte;

class GameServer : public dcl::net::TCPClient {
private:
	typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
	typedef std::shared_ptr<message_t> Message;
	typedef void(GameServer::*ProtocolHandler)(Message&);

	// Protocol message codes
	enum class MessageType : byte {
		SESSION, ONLINE_FRAME_MSG, ENTITY_INFO,
		ENTITY_LIST, CHARACTER_NAME_REQ, PLAYER_LIST,
		DISCONNECT = 255
	};

	dcl::tools::Logger m_Logger;

	void sendData(const std::string& payload);
	void sendData(const MessageType code, const std::string& payload = "");

	template<size_t size>
	void sendData(const MessageType code, dcl::structures::Buffer<byte, size>&);

	// Parses the arguments from the message
	Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

	// Map of message codes to handler function
	static boost::unordered_map<MessageType, ProtocolHandler> m_Handlers;

	// Protocol Handler functions
	void OnlineFrameHandler(Message&);
	void SessionHandler(Message&);
	void PlayerListHandler(Message&);
	void CharacterNameHandler(Message&);
	void DisconnectHandler(Message&);

	using TCPClient::sendData;

protected:
	// Handles read event from Session
	bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

public:
	static void init();

	GameServer(boost::asio::io_service&, unsigned int host, unsigned short port);

	GameServer(GameServer&);
	GameServer& operator=(GameServer&);

	void disconnect();

	void SendCharacterPosition(float x, float y, float z, float yaw);
	void SendCharacterPosition(glm::vec3 location, float yaw);
};

template<size_t size>
void GameServer::sendData(const MessageType code, dcl::structures::Buffer<byte, size>&) {
	Buffer<byte, size> buff((byte*)&code, 1);
	buff.Write(buff.data(), buff.length());
	TCPClient::sendData(buffer);
}

#endif