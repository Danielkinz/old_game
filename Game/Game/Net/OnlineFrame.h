#ifndef ONLINE_FRAME
#define ONLINE_FRAME

#include <boost\unordered_map.hpp>

typedef unsigned char byte;

struct EntityInfo {
	float X;
	float Y;
	float Z;
	byte Yaw;
};

struct OnlineFrame {
	double startTime;
	boost::unordered_map<byte, EntityInfo> players;
};

#endif