﻿namespace Client {
	partial class AddCharacterControl {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.label1 = new System.Windows.Forms.Label();
			this.CharacterNameTextBox = new System.Windows.Forms.TextBox();
			this.CreateCharacterButton = new System.Windows.Forms.Button();
			this.errorLabel = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(3, 6);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(247, 29);
			this.label1.TabIndex = 0;
			this.label1.Text = "Enter character name:";
			// 
			// CharacterNameTextBox
			// 
			this.CharacterNameTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.CharacterNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.CharacterNameTextBox.Font = new System.Drawing.Font("Arial Narrow", 18F, System.Drawing.FontStyle.Bold);
			this.CharacterNameTextBox.ForeColor = System.Drawing.Color.White;
			this.CharacterNameTextBox.Location = new System.Drawing.Point(3, 41);
			this.CharacterNameTextBox.Name = "CharacterNameTextBox";
			this.CharacterNameTextBox.Size = new System.Drawing.Size(287, 35);
			this.CharacterNameTextBox.TabIndex = 29;
			// 
			// CreateCharacterButton
			// 
			this.CreateCharacterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(75)))));
			this.CreateCharacterButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
			this.CreateCharacterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
			this.CreateCharacterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
			this.CreateCharacterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.CreateCharacterButton.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CreateCharacterButton.ForeColor = System.Drawing.Color.White;
			this.CreateCharacterButton.Location = new System.Drawing.Point(3, 82);
			this.CreateCharacterButton.Name = "CreateCharacterButton";
			this.CreateCharacterButton.Size = new System.Drawing.Size(287, 37);
			this.CreateCharacterButton.TabIndex = 31;
			this.CreateCharacterButton.Text = "Create Character";
			this.CreateCharacterButton.UseVisualStyleBackColor = false;
			this.CreateCharacterButton.Click += new System.EventHandler(this.CreateCharacterButton_Click);
			// 
			// errorLabel
			// 
			this.errorLabel.AutoSize = true;
			this.errorLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
			this.errorLabel.ForeColor = System.Drawing.Color.Yellow;
			this.errorLabel.Location = new System.Drawing.Point(142, 121);
			this.errorLabel.Name = "errorLabel";
			this.errorLabel.Size = new System.Drawing.Size(0, 25);
			this.errorLabel.TabIndex = 32;
			// 
			// AddCharacterControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.Controls.Add(this.errorLabel);
			this.Controls.Add(this.CreateCharacterButton);
			this.Controls.Add(this.CharacterNameTextBox);
			this.Controls.Add(this.label1);
			this.Name = "AddCharacterControl";
			this.Size = new System.Drawing.Size(293, 163);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox CharacterNameTextBox;
		private System.Windows.Forms.Button CreateCharacterButton;
		private System.Windows.Forms.Label errorLabel;
	}
}
