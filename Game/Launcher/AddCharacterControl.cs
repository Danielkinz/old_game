﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client {
	// A control that creates new characters
	public partial class AddCharacterControl : UserControl {
		public AddCharacterControl() {
			InitializeComponent();
		}

		private void CreateCharacterButton_Click(object sender, EventArgs e) {
			if (this.CharacterNameTextBox.Text.Length == 0) return;
			Connection.sendMessage((byte) Connection.DataCenterMessageType.CREATE_CHARACTER_REQ, 
				Connection.getByteAsString((byte) this.CharacterNameTextBox.Text.Length) + this.CharacterNameTextBox.Text);
		}

		public void setError(string error) {
			Invoke((MethodInvoker) delegate {
				this.errorLabel.Text = error;
				this.errorLabel.Location = new Point(this.Size.Width / 2 - this.errorLabel.Size.Width / 2, this.errorLabel.Location.Y);
			});
		}
	}
}
