﻿using System.Collections.Generic;

namespace Client {
	// Stores the information about a character
	public class Character {
		private string name;
		public string Name { get { return name; } }

		public Character(string name) {
			this.name = name;
		}

		public Character(List<byte> payload) {
			this.name = Connection.getStringFromBuffer(payload, Connection.getByteFromBuffer(payload));
		}
	}
}
