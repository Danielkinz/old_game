﻿namespace Client {
	partial class CharacterSelect {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.splitLine = new System.Windows.Forms.FlowLayoutPanel();
			this.NewCharacterButton = new System.Windows.Forms.Button();
			this.ContentPanel = new System.Windows.Forms.Panel();
			this.customLayout1 = new Client.CustomLayout();
			this.SuspendLayout();
			// 
			// splitLine
			// 
			this.splitLine.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.splitLine.Location = new System.Drawing.Point(172, 38);
			this.splitLine.Name = "splitLine";
			this.splitLine.Size = new System.Drawing.Size(5, 400);
			this.splitLine.TabIndex = 0;
			// 
			// NewCharacterButton
			// 
			this.NewCharacterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(75)))));
			this.NewCharacterButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
			this.NewCharacterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
			this.NewCharacterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
			this.NewCharacterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.NewCharacterButton.Font = new System.Drawing.Font("Arial Narrow", 16F, System.Drawing.FontStyle.Bold);
			this.NewCharacterButton.ForeColor = System.Drawing.Color.White;
			this.NewCharacterButton.Location = new System.Drawing.Point(10, 398);
			this.NewCharacterButton.Name = "NewCharacterButton";
			this.NewCharacterButton.Size = new System.Drawing.Size(152, 40);
			this.NewCharacterButton.TabIndex = 31;
			this.NewCharacterButton.Text = "New Character";
			this.NewCharacterButton.UseVisualStyleBackColor = false;
			this.NewCharacterButton.Click += new System.EventHandler(this.NewCharacterButton_Click);
			// 
			// ContentPanel
			// 
			this.ContentPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.ContentPanel.Location = new System.Drawing.Point(188, 38);
			this.ContentPanel.Name = "ContentPanel";
			this.ContentPanel.Size = new System.Drawing.Size(350, 400);
			this.ContentPanel.TabIndex = 32;
			// 
			// customLayout1
			// 
			this.customLayout1.AutoSize = true;
			this.customLayout1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.customLayout1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.customLayout1.Location = new System.Drawing.Point(0, 0);
			this.customLayout1.Name = "customLayout1";
			this.customLayout1.Size = new System.Drawing.Size(550, 450);
			this.customLayout1.TabIndex = 0;
			// 
			// CharacterSelect
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(550, 450);
			this.Controls.Add(this.ContentPanel);
			this.Controls.Add(this.NewCharacterButton);
			this.Controls.Add(this.splitLine);
			this.Controls.Add(this.customLayout1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "CharacterSelect";
			this.Text = "CharacterSelect";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private CustomLayout customLayout1;
		private System.Windows.Forms.FlowLayoutPanel splitLine;
		private System.Windows.Forms.Button NewCharacterButton;
		private System.Windows.Forms.Panel ContentPanel;
	}
}