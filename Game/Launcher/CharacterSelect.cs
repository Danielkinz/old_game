﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Client {
	// The character selection menu
	public partial class CharacterSelect : Form {
		private class CharacterButton : Button {
			private Character _character;
			public Character Character { get { return _character; } set { _character = value; } }
			public CharacterButton(Character c) {
				this.Name = c.Name;
				this.Text = c.Name;
				this._character = c;
				this.BackColor = Color.FromArgb(((int) (((byte) (70)))), ((int) (((byte) (70)))), ((int) (((byte) (75)))));
				this.FlatAppearance.BorderColor = Color.FromArgb(((int) (((byte) (50)))), ((int) (((byte) (50)))), ((int) (((byte) (50)))));
				this.FlatAppearance.MouseDownBackColor = Color.FromArgb(((int) (((byte) (55)))), ((int) (((byte) (55)))), ((int) (((byte) (55)))));
				this.FlatAppearance.MouseOverBackColor = Color.FromArgb(((int) (((byte) (60)))), ((int) (((byte) (60)))), ((int) (((byte) (60)))));
				this.FlatStyle = FlatStyle.Flat;
				this.Font = new Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
				this.ForeColor = Color.White;
				this.TabIndex = 0;
				this.UseVisualStyleBackColor = false;
				this.TabIndex = 0;
				this.Click += new System.EventHandler(this.CharacterButton_Click);
			}

			private void CharacterButton_Click(object sender, EventArgs e) {
				Connection.sendMessage((byte) Connection.DataCenterMessageType.SERVER_GET, 
					Connection.getByteAsString((byte) _character.Name.Length) + _character.Name);
				Program.MainForm.CharacterName = _character.Name;
			}
		}

		private CharacterButton[] _characterButtons = new CharacterButton[0];
		private AddCharacterControl _addCharacterContorl = new AddCharacterControl();

		public CharacterSelect() {
			InitializeComponent();

			_addCharacterContorl.Location = new Point(this.ContentPanel.Size.Width / 2 - _addCharacterContorl.Size.Width / 2,
				this.ContentPanel.Size.Height / 2 - _addCharacterContorl.Size.Height / 2);
		}

		// Displays the list of characters on the screen
		public void DisplayCharactrs(Character[] characters) {
			// Deletes the old character list
			foreach (CharacterButton cb in _characterButtons) {
				this.Controls.Remove(cb);
				cb.Dispose();
			}
			
			// Shows the new character list
			_characterButtons = new CharacterButton[characters.Length];
			for (int i = 0; i < characters.Length; i++) {
				_characterButtons[i] = new CharacterButton(characters[i]);
				_characterButtons[i].Size = this.NewCharacterButton.Size;
				_characterButtons[i].Location = new Point(this.NewCharacterButton.Location.X,
					this.splitLine.Location.Y + i * (this.NewCharacterButton.Size.Height + 5));
				this.Controls.Add(_characterButtons[i]);
				_characterButtons[i].BringToFront();
				_characterButtons[i].Parent = this;
			}
		}

		// Reports back to the user about the result of the character creation
		public void DisplayCharacterCreateResult(CharacterCreateResult result) {
			if (result == CharacterCreateResult.NAME_TAKEN)
				this._addCharacterContorl.setError("Character Name Taken");
			else
				this._addCharacterContorl.setError("Character created successfuly");
		}

		private void NewCharacterButton_Click(object sender, EventArgs e) {
			clearPanel();
			this.ContentPanel.Controls.Add(_addCharacterContorl);
		}

		private void clearPanel() {
			this.ContentPanel.Controls.Clear();
		}

		public enum CharacterCreateResult : byte {
			SUCCESS, NAME_TAKEN
		}
	}
}
