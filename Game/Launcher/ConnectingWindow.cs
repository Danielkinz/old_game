﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Net;

namespace Client {

	// A window that appears whenever connecting 
	public partial class ConnectingWindow : Form {

		// Imports functions and defines data to allow moving the form without the title bar
		private const int WM_NCLBUTTONDOWN = 0xA1;
		private const int HT_CAPTION = 0x2;
		[DllImportAttribute("user32.dll")]
		private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
		[DllImportAttribute("user32.dll")]
		private static extern bool ReleaseCapture();

		private class Address {
			public string ip;
			public ushort port;
			public Address(string ip, ushort port) { this.ip = ip; this.port = port; }
		}
		
		public ConnectingWindow() {
			InitializeComponent();

			// Binds the event that allows dragging the window to all the sub-components
			foreach (Control c in this.Controls) {
				if (!(c is Button))
					c.MouseDown += ConnectingWindow_MouseDown;
			}

			Thread t = new Thread(connectToLoginServer);
			t.IsBackground = true;
			t.Start();
		}

		public ConnectingWindow(string ip, ushort port) {
			InitializeComponent();

			// Binds the event that allows dragging the window to all the sub-components
			foreach (Control c in this.Controls) {
				if (!(c is Button))
					c.MouseDown += ConnectingWindow_MouseDown;
			}

			Thread t = new Thread(connectToDataCenter);
			t.IsBackground = true;
			t.Start(new Address(ip, port));
		}

		// Allows drag and drop
		private void ConnectingWindow_MouseDown(object sender, MouseEventArgs e) {
			if (e.Button == MouseButtons.Left) {
				ReleaseCapture();
				SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
			}
		}

		private void ExitButton_MouseHover(object sender, EventArgs e) {
			this.ExitButton.ForeColor = Color.FromArgb(((int) (((byte) (100)))), ((int) (((byte) (100)))), ((int) (((byte) (100)))));
		}

		private void ExitButton_MouseLeave(object sender, EventArgs e) {
			this.ExitButton.ForeColor = Color.FromArgb(((int) (((byte) (80)))), ((int) (((byte) (80)))), ((int) (((byte) (80)))));
		}

		private void ExitButton_Click(object sender, EventArgs e) {
			Application.Exit();
		}

		private void connectToLoginServer() {
			Connection.ConnectToLoginServer();

			Invoke((MethodInvoker) delegate {
				this.Close();
			});
		}

		private void connectToDataCenter(object address) {
			Address add = (Address) address;

			Connection.ConnectToDataCenter(add.ip, add.port);

			Invoke((MethodInvoker) delegate {
				this.Close();
			});
		}
	}
}
