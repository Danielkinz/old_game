﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;

namespace Client {
	public class Connection {

		private static TcpClient client = null;
		private static NetworkStream clientStream = null;
		private static Thread messageHandler = null;

		private delegate void Handler(List<byte> payload);
		private static Dictionary<byte, Handler> handlers = new Dictionary<byte, Handler>();

		public static void ConnectToLoginServer() {
			// Maps the handlers to their message codes
			handlers.Add((byte) LoginServerMessageType.SIGN_IN_RET, Program.MainForm.handleSignIn);
			handlers.Add((byte) LoginServerMessageType.REGISTER_RET, Program.MainForm.handleRegister);
			handlers.Add((byte) LoginServerMessageType.DATA_CENTER_LIST_RET, Program.MainForm.handleDataCenterList);
			handlers.Add((byte) LoginServerMessageType.DATA_CENTER_SELECT_RET, Program.MainForm.handleDataCenterSelect);

			// This loop will exit only if the connection to the server is successful
			while (true) {
				// Connect to the server
				try {
					client = new TcpClient();
					IPEndPoint serverEndPoint = new IPEndPoint(Program.IP, Program.Port);
					client.Connect(serverEndPoint);
					clientStream = client.GetStream();
					messageHandler = new Thread(HandleMessages);
					messageHandler.IsBackground = true;
					messageHandler.Start();
					break;
				} catch (Exception) {
					if (clientStream != null)
						clientStream.Close();
					if (client != null)
						client.Close();
				}
			}

			sendMessage(getByteAsString(0));
		}

		public static void ConnectToDataCenter(string ip, ushort port) {
			sendMessage((byte) LoginServerMessageType.DISCONNECT);
			Disconnect();

			// Maps the message handlers
			handlers.Clear();
			handlers.Add((byte) DataCenterMessageType.SESSION, Program.MainForm.handleSessionReqeust);
			handlers.Add((byte) DataCenterMessageType.CHARACTER_LIST, Program.MainForm.handleCharacterList);
			handlers.Add((byte) DataCenterMessageType.CREATE_CHARACTER_RET, Program.MainForm.handleCharacterCreateResult);
			handlers.Add((byte) DataCenterMessageType.SERVER_RET, Program.MainForm.handleServerRet);

			// This loop will exit only if the connection to the server is successful
			while (true) {
				try {
					client = new TcpClient();
					IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(ip), port);
					client.Connect(serverEndPoint);
					clientStream = client.GetStream();
					messageHandler = new Thread(HandleMessages);
					messageHandler.IsBackground = true;
					messageHandler.Start();
					break;
				} catch (Exception) {
					if (clientStream != null)
						clientStream.Close();
					if (client != null)
						client.Close();
				}
			}

			sendMessage(getByteAsString(0));
		}

		public static void Disconnect() {
			if (clientStream != null) clientStream.Close();
			if (client != null) client.Close();
			if (messageHandler != null) messageHandler.Abort();
		}

		// Waits for messages and activates their handlers
		public static void HandleMessages() {
			// Runs until disconnected or terminated
			while (true) {
				int bytesReceived = 0;
				List<byte> payload = new List<byte>();
				byte[] buffer = new Byte[512];
				byte msg;

				// Reads a new message
				try {
					while ((bytesReceived = clientStream.Read(buffer, 0, 512)) == 512) {
						for (int i = 0; i < 512; i++) payload.Add(buffer[i]);
					}
					for (int i = 0; i < bytesReceived; i++) payload.Add(buffer[i]);
				} catch (System.IO.IOException) {
					Program.IsRunning = false;
					MessageBox.Show("Connection to server lost");
					Application.Exit();
					return;
				}

				// Handles the message
				msg = getByteFromBuffer(payload);
				try {
					handlers[msg](payload);
				} catch (KeyNotFoundException) {
					if (!Program.IsRunning) {
						return;
					}
				}
			}
		}
	
		public static void sendMessage(byte msg, string data="") {
			sendMessage(getByteAsString(msg) + data);
		}

		public static void sendMessage(string data) {
			byte[] buffer = new ASCIIEncoding().GetBytes(data);
			try {
				// Sends the buffer data in packets of 512 bytes
				int i = 0;
				for (i = 0; i < buffer.Length / 512; i++) {
					clientStream.Write(buffer, i * 512, 512);
				}
				if (buffer.Length % 512 != 0) clientStream.Write(buffer, i * 512, buffer.Length % 512);
				else {
					buffer[0] = 0;
					clientStream.Write(buffer, 0, 1);
				}
				clientStream.Flush();
			} catch (Exception) {
				if (Program.IsRunning) {
					Application.OpenForms[0].Invoke((MethodInvoker) delegate {
						MessageBox.Show("An error has occured while sending message to server");
					});
				}
			}
		}

		// Converts 2 bytes into an unsigned short
		public static ushort getUShortFromBuffer(List<Byte> buffer) {
			ushort num = buffer[1];
			num *= 256;
			num += buffer[0];
			buffer.RemoveRange(0, 2);
			return num;
		}

		// Converts 4 byte into an unsigned int
		public static uint getUIntFromBuffer(List<Byte> buffer) {
			uint num = buffer[3];
			num *= 256;
			num += buffer[2];
			num *= 256;
			num += buffer[1];
			num *= 256;
			num += buffer[0];
			buffer.RemoveRange(0, 4);
			return num;
		}

		// Gets 1 byte from the buffer
		public static byte getByteFromBuffer(List<Byte> buffer) {
			byte b = buffer[0];
			buffer.RemoveAt(0);
			return b;
		}

		// Reads a string from the buffer of length @length
		public static string getStringFromBuffer(List<Byte> buffer, int length) {
			byte[] data = new byte[length];
			for (int i = 0; i < length; i++) data[i] = getByteFromBuffer(buffer);
			return new ASCIIEncoding().GetString(data);
		}

		// Reads a plain-text int from the buffer
		public static int getIntFromBuffer(List<Byte> buffer, int bytesNum) {
			return int.Parse(getStringFromBuffer(buffer, bytesNum));
		}

		// Converts the byte @b to a 1-byte string
		public static string getByteAsString(byte b) {
			byte[] bytes = new byte[] { b };
			return Encoding.ASCII.GetString(bytes);
		}

		// returns a string containing the number @num 0-padded to be of length @size 
		public static string getPaddedNumber(int num, int size) {
			string s = num.ToString();
			while (s.Length < size) s = "0" + s;
			return s;
		}
		
		// Message code in the Login Server communication protocol
		public enum LoginServerMessageType : byte {
			SIGN_IN_REQ, SIGN_IN_RET,
			REGISTER_REQ, REGISTER_RET,
			CHANGE_PASS_REQ, CHANGE_PASS_RET,
			CHANGE_EMAIL_REQ, CHANGE_EMAIL_RET,
			DATA_CENTER_LIST_REQ, DATA_CENTER_LIST_RET,
			DATA_CENTER_SELECT_REQ, DATA_CENTER_SELECT_RET,
			DISCONNECT = 255
		};

		// Message codes in the DataCenter communication protocol
		public enum DataCenterMessageType : byte {
			SESSION, CHARACTER_LIST,
			CREATE_CHARACTER_REQ, CREATE_CHARACTER_RET,
			SERVER_GET, SERVER_RET,
			DISCONNECT = 255
		}
	}
}
