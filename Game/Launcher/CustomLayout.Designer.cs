﻿namespace Client {
	partial class CustomLayout {
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.TitlePanel = new System.Windows.Forms.Panel();
			this.Minimize = new System.Windows.Forms.Button();
			this.ExitButton = new System.Windows.Forms.Button();
			this.GameName = new System.Windows.Forms.Label();
			this.TitlePanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// TitlePanel
			// 
			this.TitlePanel.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (40)))), ((int) (((byte) (40)))), ((int) (((byte) (45)))));
			this.TitlePanel.Controls.Add(this.GameName);
			this.TitlePanel.Controls.Add(this.ExitButton);
			this.TitlePanel.Controls.Add(this.Minimize);
			this.TitlePanel.Location = new System.Drawing.Point(0, 0);
			this.TitlePanel.Name = "TitlePanel";
			this.TitlePanel.Size = new System.Drawing.Size(300, 25);
			this.TitlePanel.TabIndex = 1;
			this.TitlePanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Drag_MouseDown);
			// 
			// Minimize
			// 
			this.Minimize.BackColor = System.Drawing.Color.Transparent;
			this.Minimize.FlatAppearance.BorderSize = 0;
			this.Minimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.Minimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.Minimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.Minimize.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Bold);
			this.Minimize.ForeColor = System.Drawing.Color.FromArgb(((int) (((byte) (80)))), ((int) (((byte) (80)))), ((int) (((byte) (80)))));
			this.Minimize.Location = new System.Drawing.Point(252, 0);
			this.Minimize.Name = "Minimize";
			this.Minimize.Size = new System.Drawing.Size(25, 25);
			this.Minimize.TabIndex = 6;
			this.Minimize.TabStop = false;
			this.Minimize.Text = "-";
			this.Minimize.UseVisualStyleBackColor = false;
			this.Minimize.Click += new System.EventHandler(this.Minimize_Click);
			this.Minimize.MouseLeave += new System.EventHandler(this.button_MouseLeave);
			this.Minimize.MouseHover += new System.EventHandler(this.button_MouseHover);
			// 
			// ExitButton
			// 
			this.ExitButton.BackColor = System.Drawing.Color.Transparent;
			this.ExitButton.FlatAppearance.BorderSize = 0;
			this.ExitButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
			this.ExitButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
			this.ExitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.ExitButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
			this.ExitButton.ForeColor = System.Drawing.Color.FromArgb(((int) (((byte) (80)))), ((int) (((byte) (80)))), ((int) (((byte) (80)))));
			this.ExitButton.Location = new System.Drawing.Point(274, 0);
			this.ExitButton.Name = "ExitButton";
			this.ExitButton.Size = new System.Drawing.Size(25, 25);
			this.ExitButton.TabIndex = 5;
			this.ExitButton.TabStop = false;
			this.ExitButton.Text = "X";
			this.ExitButton.UseVisualStyleBackColor = false;
			this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
			this.ExitButton.MouseLeave += new System.EventHandler(this.button_MouseLeave);
			this.ExitButton.MouseHover += new System.EventHandler(this.button_MouseHover);
			// 
			// GameName
			// 
			this.GameName.AutoSize = true;
			this.GameName.Font = new System.Drawing.Font("Showcard Gothic", 12F);
			this.GameName.ForeColor = System.Drawing.Color.FromArgb(((int) (((byte) (80)))), ((int) (((byte) (80)))), ((int) (((byte) (80)))));
			this.GameName.Location = new System.Drawing.Point(3, 1);
			this.GameName.Name = "GameName";
			this.GameName.Size = new System.Drawing.Size(54, 20);
			this.GameName.TabIndex = 7;
			this.GameName.Text = "Game";
			this.GameName.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Drag_MouseDown);
			// 
			// CustomLayout
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int) (((byte) (50)))), ((int) (((byte) (50)))), ((int) (((byte) (55)))));
			this.Controls.Add(this.TitlePanel);
			this.Name = "CustomLayout";
			this.Size = new System.Drawing.Size(302, 302);
			this.Load += new System.EventHandler(this.LayoutTitle_Load);
			this.TitlePanel.ResumeLayout(false);
			this.TitlePanel.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		internal System.Windows.Forms.Panel TitlePanel;
		private System.Windows.Forms.Label GameName;
		internal System.Windows.Forms.Button ExitButton;
		internal System.Windows.Forms.Button Minimize;
	}
}
