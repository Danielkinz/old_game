﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace Client {
	// Contains a nice layout for use in the rest of the forms
	public partial class CustomLayout : UserControl {

		// DLL imports
		private const int WM_NCLBUTTONDOWN = 0xA1;
		private const int HT_CAPTION = 0x2;
		private const int SW_MINIMIZE = 0x6;
		[DllImportAttribute("user32.dll")]
		private static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
		[DllImportAttribute("user32.dll")]
		private static extern bool ReleaseCapture();
		[DllImportAttribute("user32.dll")]
		private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

		private void Drag_MouseDown(object sender, MouseEventArgs e) {
			if (e.Button == MouseButtons.Left) {
				ReleaseCapture();
				SendMessage(Parent.Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
			}
		}

		public CustomLayout() {
			InitializeComponent();
		}

		private void button_MouseHover(object sender, EventArgs e) {
			((Button) sender).ForeColor = Color.FromArgb(((int) (((byte) (100)))), ((int) (((byte) (100)))), ((int) (((byte) (100)))));
		}

		private void button_MouseLeave(object sender, EventArgs e) {
			((Button) sender).ForeColor = Color.FromArgb(((int) (((byte) (80)))), ((int) (((byte) (80)))), ((int) (((byte) (80)))));
		}

		private void ExitButton_Click(object sender, EventArgs e) {
			((Form) this.Parent).DialogResult = DialogResult.Abort;
			((Form) this.Parent).Close();
		}

		private void Minimize_Click(object sender, EventArgs e) {
			ShowWindow(Parent.Handle, SW_MINIMIZE);
		}

		private void LayoutTitle_Load(object sender, EventArgs e) {
			this.Dock = DockStyle.Fill;
			this.AutoSize = true;
			this.ExitButton.Location = new Point(this.Parent.Size.Width - 25, 0);
			this.Minimize.Location = new Point(this.Parent.Size.Width - 50, 0);
			this.TitlePanel.Size = new Size(this.Parent.Size.Width, 25);
			this.TitlePanel.Location = new Point(0, 0);
			((Form) Parent).FormBorderStyle = FormBorderStyle.None;
			this.SendToBack();
		}
	}
}
