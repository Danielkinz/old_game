﻿namespace Client {
	partial class LoginWindow {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.RegisterOpenButton = new System.Windows.Forms.Button();
			this.SignInButton = new System.Windows.Forms.Button();
			this.PasswordTextBox = new System.Windows.Forms.TextBox();
			this.Title = new System.Windows.Forms.Label();
			this.ConfirmPasswordRegisterTextBox = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.PasswordRegisterTextBox = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.UsernameRegisterTextBox = new System.Windows.Forms.TextBox();
			this.UsernameTextBox = new System.Windows.Forms.TextBox();
			this.RegisterButton = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.EmailRegisterTextBox = new System.Windows.Forms.TextBox();
			this.RegisterPanel = new System.Windows.Forms.Panel();
			this.BackButton = new System.Windows.Forms.Button();
			this.RegisterErrorLabel = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.errorLabel = new System.Windows.Forms.Label();
			this.layout = new Client.CustomLayout();
			this.RegisterPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// RegisterOpenButton
			// 
			this.RegisterOpenButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(75)))));
			this.RegisterOpenButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
			this.RegisterOpenButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
			this.RegisterOpenButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
			this.RegisterOpenButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.RegisterOpenButton.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.RegisterOpenButton.ForeColor = System.Drawing.Color.White;
			this.RegisterOpenButton.Location = new System.Drawing.Point(154, 311);
			this.RegisterOpenButton.Name = "RegisterOpenButton";
			this.RegisterOpenButton.Size = new System.Drawing.Size(119, 37);
			this.RegisterOpenButton.TabIndex = 31;
			this.RegisterOpenButton.Text = "Register";
			this.RegisterOpenButton.UseVisualStyleBackColor = false;
			this.RegisterOpenButton.Click += new System.EventHandler(this.RegisterButton_Click);
			// 
			// SignInButton
			// 
			this.SignInButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(75)))));
			this.SignInButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
			this.SignInButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
			this.SignInButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
			this.SignInButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.SignInButton.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.SignInButton.ForeColor = System.Drawing.Color.White;
			this.SignInButton.Location = new System.Drawing.Point(25, 310);
			this.SignInButton.Name = "SignInButton";
			this.SignInButton.Size = new System.Drawing.Size(119, 37);
			this.SignInButton.TabIndex = 30;
			this.SignInButton.Text = "Sign In";
			this.SignInButton.UseVisualStyleBackColor = false;
			this.SignInButton.Click += new System.EventHandler(this.SignInButton_Click);
			// 
			// PasswordTextBox
			// 
			this.PasswordTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.PasswordTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PasswordTextBox.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold);
			this.PasswordTextBox.ForeColor = System.Drawing.Color.White;
			this.PasswordTextBox.Location = new System.Drawing.Point(25, 243);
			this.PasswordTextBox.Name = "PasswordTextBox";
			this.PasswordTextBox.PasswordChar = '•';
			this.PasswordTextBox.Size = new System.Drawing.Size(248, 29);
			this.PasswordTextBox.TabIndex = 29;
			this.PasswordTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterKeyPress);
			// 
			// Title
			// 
			this.Title.AutoSize = true;
			this.Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.Title.Font = new System.Drawing.Font("Showcard Gothic", 54F);
			this.Title.ForeColor = System.Drawing.Color.White;
			this.Title.Location = new System.Drawing.Point(33, 43);
			this.Title.Name = "Title";
			this.Title.Size = new System.Drawing.Size(230, 89);
			this.Title.TabIndex = 32;
			this.Title.Text = "Game";
			// 
			// ConfirmPasswordRegisterTextBox
			// 
			this.ConfirmPasswordRegisterTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.ConfirmPasswordRegisterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.ConfirmPasswordRegisterTextBox.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ConfirmPasswordRegisterTextBox.ForeColor = System.Drawing.Color.White;
			this.ConfirmPasswordRegisterTextBox.Location = new System.Drawing.Point(25, 152);
			this.ConfirmPasswordRegisterTextBox.Name = "ConfirmPasswordRegisterTextBox";
			this.ConfirmPasswordRegisterTextBox.PasswordChar = '•';
			this.ConfirmPasswordRegisterTextBox.Size = new System.Drawing.Size(248, 29);
			this.ConfirmPasswordRegisterTextBox.TabIndex = 3;
			this.ConfirmPasswordRegisterTextBox.TabStop = false;
			this.ConfirmPasswordRegisterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterKeyPress);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
			this.label5.ForeColor = System.Drawing.Color.White;
			this.label5.Location = new System.Drawing.Point(20, 126);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(167, 25);
			this.label5.TabIndex = 13;
			this.label5.Text = "Confirm Password";
			// 
			// PasswordRegisterTextBox
			// 
			this.PasswordRegisterTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.PasswordRegisterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.PasswordRegisterTextBox.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold);
			this.PasswordRegisterTextBox.ForeColor = System.Drawing.Color.White;
			this.PasswordRegisterTextBox.Location = new System.Drawing.Point(25, 94);
			this.PasswordRegisterTextBox.Name = "PasswordRegisterTextBox";
			this.PasswordRegisterTextBox.PasswordChar = '•';
			this.PasswordRegisterTextBox.Size = new System.Drawing.Size(248, 29);
			this.PasswordRegisterTextBox.TabIndex = 2;
			this.PasswordRegisterTextBox.TabStop = false;
			this.PasswordRegisterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterKeyPress);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
			this.label4.ForeColor = System.Drawing.Color.White;
			this.label4.Location = new System.Drawing.Point(20, 10);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(103, 25);
			this.label4.TabIndex = 9;
			this.label4.Text = "Username:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
			this.label1.ForeColor = System.Drawing.Color.White;
			this.label1.Location = new System.Drawing.Point(20, 68);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(101, 25);
			this.label1.TabIndex = 11;
			this.label1.Text = "Password:";
			// 
			// UsernameRegisterTextBox
			// 
			this.UsernameRegisterTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.UsernameRegisterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.UsernameRegisterTextBox.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold);
			this.UsernameRegisterTextBox.ForeColor = System.Drawing.Color.White;
			this.UsernameRegisterTextBox.Location = new System.Drawing.Point(25, 36);
			this.UsernameRegisterTextBox.Name = "UsernameRegisterTextBox";
			this.UsernameRegisterTextBox.Size = new System.Drawing.Size(248, 29);
			this.UsernameRegisterTextBox.TabIndex = 1;
			this.UsernameRegisterTextBox.TabStop = false;
			this.UsernameRegisterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterKeyPress);
			// 
			// UsernameTextBox
			// 
			this.UsernameTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.UsernameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.UsernameTextBox.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold);
			this.UsernameTextBox.ForeColor = System.Drawing.Color.White;
			this.UsernameTextBox.Location = new System.Drawing.Point(25, 178);
			this.UsernameTextBox.Name = "UsernameTextBox";
			this.UsernameTextBox.Size = new System.Drawing.Size(248, 29);
			this.UsernameTextBox.TabIndex = 28;
			this.UsernameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterKeyPress);
			// 
			// RegisterButton
			// 
			this.RegisterButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(75)))));
			this.RegisterButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
			this.RegisterButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
			this.RegisterButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
			this.RegisterButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.RegisterButton.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold);
			this.RegisterButton.ForeColor = System.Drawing.Color.White;
			this.RegisterButton.Location = new System.Drawing.Point(154, 275);
			this.RegisterButton.Name = "RegisterButton";
			this.RegisterButton.Size = new System.Drawing.Size(119, 37);
			this.RegisterButton.TabIndex = 6;
			this.RegisterButton.TabStop = false;
			this.RegisterButton.Text = "Register";
			this.RegisterButton.UseVisualStyleBackColor = false;
			this.RegisterButton.Click += new System.EventHandler(this.Register_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
			this.label6.ForeColor = System.Drawing.Color.White;
			this.label6.Location = new System.Drawing.Point(20, 184);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 25);
			this.label6.TabIndex = 15;
			this.label6.Text = "Email:";
			// 
			// EmailRegisterTextBox
			// 
			this.EmailRegisterTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.EmailRegisterTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.EmailRegisterTextBox.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.EmailRegisterTextBox.ForeColor = System.Drawing.SystemColors.Window;
			this.EmailRegisterTextBox.Location = new System.Drawing.Point(25, 210);
			this.EmailRegisterTextBox.Name = "EmailRegisterTextBox";
			this.EmailRegisterTextBox.Size = new System.Drawing.Size(248, 29);
			this.EmailRegisterTextBox.TabIndex = 4;
			this.EmailRegisterTextBox.TabStop = false;
			this.EmailRegisterTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterKeyPress);
			// 
			// RegisterPanel
			// 
			this.RegisterPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.RegisterPanel.Controls.Add(this.BackButton);
			this.RegisterPanel.Controls.Add(this.RegisterErrorLabel);
			this.RegisterPanel.Controls.Add(this.RegisterButton);
			this.RegisterPanel.Controls.Add(this.label6);
			this.RegisterPanel.Controls.Add(this.EmailRegisterTextBox);
			this.RegisterPanel.Controls.Add(this.ConfirmPasswordRegisterTextBox);
			this.RegisterPanel.Controls.Add(this.label5);
			this.RegisterPanel.Controls.Add(this.PasswordRegisterTextBox);
			this.RegisterPanel.Controls.Add(this.label4);
			this.RegisterPanel.Controls.Add(this.label1);
			this.RegisterPanel.Controls.Add(this.UsernameRegisterTextBox);
			this.RegisterPanel.Location = new System.Drawing.Point(0, 358);
			this.RegisterPanel.Name = "RegisterPanel";
			this.RegisterPanel.Size = new System.Drawing.Size(300, 325);
			this.RegisterPanel.TabIndex = 35;
			// 
			// BackButton
			// 
			this.BackButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(75)))));
			this.BackButton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
			this.BackButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(55)))), ((int)(((byte)(55)))));
			this.BackButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
			this.BackButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.BackButton.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.BackButton.ForeColor = System.Drawing.Color.White;
			this.BackButton.Location = new System.Drawing.Point(24, 275);
			this.BackButton.Name = "BackButton";
			this.BackButton.Size = new System.Drawing.Size(120, 37);
			this.BackButton.TabIndex = 5;
			this.BackButton.TabStop = false;
			this.BackButton.Text = "Back";
			this.BackButton.UseVisualStyleBackColor = false;
			this.BackButton.Click += new System.EventHandler(this.BackButton_Click);
			// 
			// RegisterErrorLabel
			// 
			this.RegisterErrorLabel.AutoSize = true;
			this.RegisterErrorLabel.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold);
			this.RegisterErrorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(38)))), ((int)(((byte)(22)))));
			this.RegisterErrorLabel.Location = new System.Drawing.Point(20, 246);
			this.RegisterErrorLabel.Name = "RegisterErrorLabel";
			this.RegisterErrorLabel.Size = new System.Drawing.Size(0, 23);
			this.RegisterErrorLabel.TabIndex = 10;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.label2.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
			this.label2.ForeColor = System.Drawing.Color.White;
			this.label2.Location = new System.Drawing.Point(20, 152);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(103, 25);
			this.label2.TabIndex = 33;
			this.label2.Text = "Username:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.label3.Font = new System.Drawing.Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold);
			this.label3.ForeColor = System.Drawing.Color.White;
			this.label3.Location = new System.Drawing.Point(20, 214);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(101, 25);
			this.label3.TabIndex = 34;
			this.label3.Text = "Password:";
			// 
			// errorLabel
			// 
			this.errorLabel.AutoSize = true;
			this.errorLabel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.errorLabel.Font = new System.Drawing.Font("Arial Narrow", 14.25F, System.Drawing.FontStyle.Bold);
			this.errorLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(38)))), ((int)(((byte)(22)))));
			this.errorLabel.Location = new System.Drawing.Point(20, 280);
			this.errorLabel.Name = "errorLabel";
			this.errorLabel.Size = new System.Drawing.Size(0, 23);
			this.errorLabel.TabIndex = 36;
			// 
			// layout
			// 
			this.layout.AutoSize = true;
			this.layout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.layout.Dock = System.Windows.Forms.DockStyle.Fill;
			this.layout.Location = new System.Drawing.Point(0, 0);
			this.layout.Name = "layout";
			this.layout.Size = new System.Drawing.Size(300, 370);
			this.layout.TabIndex = 0;
			// 
			// LoginWindow
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(65)))));
			this.ClientSize = new System.Drawing.Size(300, 370);
			this.Controls.Add(this.RegisterPanel);
			this.Controls.Add(this.RegisterOpenButton);
			this.Controls.Add(this.SignInButton);
			this.Controls.Add(this.PasswordTextBox);
			this.Controls.Add(this.Title);
			this.Controls.Add(this.UsernameTextBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.errorLabel);
			this.Controls.Add(this.layout);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "LoginWindow";
			this.ShowIcon = false;
			this.Text = "Trivia";
			this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EnterKeyPress);
			this.RegisterPanel.ResumeLayout(false);
			this.RegisterPanel.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private CustomLayout layout;
		private System.Windows.Forms.Button RegisterOpenButton;
		private System.Windows.Forms.Button SignInButton;
		private System.Windows.Forms.TextBox PasswordTextBox;
		private System.Windows.Forms.Label Title;
		private System.Windows.Forms.TextBox ConfirmPasswordRegisterTextBox;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox PasswordRegisterTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox UsernameRegisterTextBox;
		private System.Windows.Forms.TextBox UsernameTextBox;
		private System.Windows.Forms.Button RegisterButton;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox EmailRegisterTextBox;
		private System.Windows.Forms.Panel RegisterPanel;
		private System.Windows.Forms.Button BackButton;
		private System.Windows.Forms.Label RegisterErrorLabel;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label errorLabel;
	}
}