﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.Text;

namespace Client {
	public partial class LoginWindow : Form {
		private Thread runningAnimation = null;

		public LoginWindow() {
			InitializeComponent();
		}

		private void RegisterButton_Click(object sender, EventArgs e) {
			errorLabel.Text = "";
			RegisterErrorLabel.Text = "";

			flipTabStop();
			if (runningAnimation != null) runningAnimation.Abort();
			runningAnimation = new Thread(moveUp);
			runningAnimation.Start();
		}

		private void BackButton_Click(object sender, EventArgs e) {
			errorLabel.Text = "";
			RegisterErrorLabel.Text = "";

			flipTabStop();
			if (runningAnimation != null) runningAnimation.Abort();
			runningAnimation = new Thread(moveDown);
			runningAnimation.Start();
		}

		private void moveUp(object obj) {
			while (this.RegisterPanel.Location.Y > 55) {
				Invoke((MethodInvoker) delegate { this.RegisterPanel.Location = new Point(this.RegisterPanel.Location.X, this.RegisterPanel.Location.Y - 10); });
				Thread.Sleep(10);
			}
			Invoke((MethodInvoker) delegate { this.RegisterPanel.Location = new Point(this.RegisterPanel.Location.X, 45); });
		}

		private void moveDown(object obj) {
			while (this.RegisterPanel.Location.Y < 350) {
				Invoke((MethodInvoker) delegate { this.RegisterPanel.Location = new Point(this.RegisterPanel.Location.X, this.RegisterPanel.Location.Y + 10); });
				Thread.Sleep(10);
			}
			Invoke((MethodInvoker) delegate { this.RegisterPanel.Location = new Point(this.RegisterPanel.Location.X, 360); });
		}

		private void flipTabStop() {
			this.ConfirmPasswordRegisterTextBox.TabStop = !this.ConfirmPasswordRegisterTextBox.TabStop;
			this.EmailRegisterTextBox.TabStop = !this.EmailRegisterTextBox.TabStop;
			this.PasswordRegisterTextBox.TabStop = !this.PasswordRegisterTextBox.TabStop;
			this.PasswordTextBox.TabStop = !this.PasswordTextBox.TabStop;
			this.UsernameRegisterTextBox.TabStop = !this.UsernameRegisterTextBox.TabStop;
			this.UsernameTextBox.TabStop = !this.UsernameTextBox.TabStop;
			this.RegisterButton.TabStop = !this.RegisterButton.TabStop;
			this.RegisterOpenButton.TabStop = !this.RegisterOpenButton.TabStop;
			this.SignInButton.TabStop = !this.SignInButton.TabStop;
			this.BackButton.TabStop = !this.BackButton.TabStop;
		}

		private void SignInButton_Click(object sender, EventArgs e) {
			Connection.sendMessage((byte) Connection.LoginServerMessageType.SIGN_IN_REQ,
				Connection.getByteAsString((byte) this.UsernameTextBox.Text.Length) + this.UsernameTextBox.Text +
				Connection.getByteAsString((byte) this.PasswordTextBox.Text.Length) + this.PasswordTextBox.Text);
		}

		public void SignInAction(SignInResult result) {
			if (result == SignInResult.SUCCESS || result == SignInResult.ADMIN_SUCCESS) {
				Program.MainForm.Admin = result == SignInResult.ADMIN_SUCCESS;
				Program.MainForm.Username = this.UsernameTextBox.Text;
				this.Close();
			} else if (result == SignInResult.ALREADY_CONNECTED) {
				showError("User already connected");
			} else if (result == SignInResult.FAIL) {
				showError("Incorrect username or password");
			} else {
				showError("An unknown error has occured");
			}
		}

		private void Register_Click(object sender, EventArgs e) {
			if (PasswordRegisterTextBox.Text != ConfirmPasswordRegisterTextBox.Text) {
				showRegisterError("Passwords mismatch");
				return;
			}
			
			Connection.sendMessage((byte) Connection.LoginServerMessageType.REGISTER_REQ,
				Connection.getByteAsString((byte) this.UsernameRegisterTextBox.Text.Length) + this.UsernameRegisterTextBox.Text +
				Connection.getByteAsString((byte) this.PasswordRegisterTextBox.Text.Length) + this.PasswordRegisterTextBox.Text +
				Connection.getByteAsString((byte) this.EmailRegisterTextBox.Text.Length) + this.EmailRegisterTextBox.Text);
		}

		public void RegisterAction(RegisterResult result) {
			if (result == RegisterResult.SUCCESS) {
				MessageBox.Show("Registration successful!");
				this.BackButton.PerformClick();
			} else if (result == RegisterResult.ILLEGAL_PASSWORD) {
				showRegisterError("Invalid Password");
			} else if (result == RegisterResult.EMAIL_ILLEGAL) {
				showRegisterError("Invalid Email");
			} else if (result == RegisterResult.EMAIL_TAKEN) {
				showRegisterError("Email already taken");
			} else if (result == RegisterResult.USERNAME_TAKEN) {
				showRegisterError("Username already taken");
			} else if (result == RegisterResult.USERNAME_ILLEGAL) {
				showRegisterError("Invalid username");
			} else {
				showRegisterError("An unknown error has occured");
			}
		}

		private void showError(string err) {
			RegisterErrorLabel.Text = "";
			errorLabel.Text = err;
			errorLabel.Location = new Point(this.Size.Width/2 - errorLabel.Size.Width/2, errorLabel.Location.Y);
		}

		private void showRegisterError(string err) {
			errorLabel.Text = "";
			RegisterErrorLabel.Text = err;
			RegisterErrorLabel.Location = new Point(this.Size.Width / 2 - RegisterErrorLabel.Size.Width / 2, RegisterErrorLabel.Location.Y);
		}

		public enum SignInResult : byte {
			FAIL,
			SUCCESS,
			ALREADY_CONNECTED,
			ADMIN_SUCCESS
		};

		public enum RegisterResult : byte {
			SUCCESS,
			ILLEGAL_PASSWORD,
			USERNAME_TAKEN,
			USERNAME_ILLEGAL,
			EMAIL_ILLEGAL,
			EMAIL_TAKEN,
			OTHER
		};

		private void EnterKeyPress(object sender, KeyPressEventArgs e) {
			if (e.KeyChar == 13) {
				if (this.SignInButton.TabStop) this.SignInButton.PerformClick();
				else this.RegisterButton.PerformClick();
			}
		}
	}
}
