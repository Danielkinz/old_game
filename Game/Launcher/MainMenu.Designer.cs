﻿namespace Client {
	partial class MainMenu {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.Title = new System.Windows.Forms.Label();
			this.customLayout1 = new Client.CustomLayout();
			this.SuspendLayout();
			// 
			// Title
			// 
			this.Title.AutoSize = true;
			this.Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Title.ForeColor = System.Drawing.Color.White;
			this.Title.Location = new System.Drawing.Point(12, 40);
			this.Title.Name = "Title";
			this.Title.Size = new System.Drawing.Size(191, 31);
			this.Title.TabIndex = 2;
			this.Title.Text = "Select Region:";
			// 
			// customLayout1
			// 
			this.customLayout1.AutoSize = true;
			this.customLayout1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(55)))));
			this.customLayout1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.customLayout1.Location = new System.Drawing.Point(0, 0);
			this.customLayout1.Name = "customLayout1";
			this.customLayout1.Size = new System.Drawing.Size(600, 400);
			this.customLayout1.TabIndex = 0;
			// 
			// MainMenu
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(600, 400);
			this.Controls.Add(this.Title);
			this.Controls.Add(this.customLayout1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "MainMenu";
			this.Text = "MainMenu";
			this.Load += new System.EventHandler(this.MainMenu_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private CustomLayout customLayout1;
		private System.Windows.Forms.Label Title;
	}
}