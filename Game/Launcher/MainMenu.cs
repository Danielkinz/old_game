﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.Net;

namespace Client {
	// Handles all the received messages from the servers
	public partial class MainMenu : Form {
		// Imports functions and defines data to allow moving the form without the title bar
		public string Username = null;
		public bool Admin = false;
		public string Session = null;
		public string CharacterName = null;
		private Form currentForm = null;
		private DataCenterButton[] dataCenters = null;
		
		// A data center select button
		private class DataCenterButton : Button {
			public DataCenterButton(string name) {
				this.Name = name;
				this.Text = name;
				Click += DataCenterButton_Click;
				this.BackColor = Color.FromArgb(((int) (((byte) (70)))), ((int) (((byte) (70)))), ((int) (((byte) (75)))));
				this.FlatAppearance.BorderColor = Color.FromArgb(((int) (((byte) (50)))), ((int) (((byte) (50)))), ((int) (((byte) (50)))));
				this.FlatAppearance.MouseDownBackColor = Color.FromArgb(((int) (((byte) (55)))), ((int) (((byte) (55)))), ((int) (((byte) (55)))));
				this.FlatAppearance.MouseOverBackColor = Color.FromArgb(((int) (((byte) (60)))), ((int) (((byte) (60)))), ((int) (((byte) (60)))));
				this.FlatStyle = FlatStyle.Flat;
				this.Font = new Font("Arial Narrow", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte) (0)));
				this.ForeColor = Color.White;
				this.TabIndex = 0;
				this.UseVisualStyleBackColor = false;
				this.TabIndex = 0;
			}

			private void DataCenterButton_Click(object sender, EventArgs e) {
				Connection.sendMessage((byte) Connection.LoginServerMessageType.DATA_CENTER_SELECT_REQ,
					Connection.getByteAsString((byte) this.Text.Length) + this.Text);
			}
		}

		public MainMenu() {
			InitializeComponent();
		}

		public void handleSignIn(List<byte> payload) {
			LoginWindow.SignInResult res = (LoginWindow.SignInResult) Connection.getByteFromBuffer(payload);

			if (res == LoginWindow.SignInResult.SUCCESS || res == LoginWindow.SignInResult.ADMIN_SUCCESS) {
				// Reads the session id
				this.Session = Connection.getStringFromBuffer(payload, 64);
			}

			Invoke((MethodInvoker) delegate {
				((LoginWindow) currentForm).SignInAction(res);
			});
		}

		public void handleRegister(List<byte> payload) {
			LoginWindow.RegisterResult res = (LoginWindow.RegisterResult) Connection.getByteFromBuffer(payload);

			Invoke((MethodInvoker) delegate {
				((LoginWindow) currentForm).RegisterAction(res);
			});
		}

		public void handleDataCenterList(List<byte> payload) {
			int amountOfDataCenters = Connection.getByteFromBuffer(payload);
			DataCenterButton[] dataCentersInfo = new DataCenterButton[amountOfDataCenters];

			// Reads the data centers info
			for (int i = 0; i < amountOfDataCenters; i++) {
				dataCentersInfo[i] = new DataCenterButton(
					Connection.getStringFromBuffer(payload, Connection.getByteFromBuffer(payload)));
			}

			Invoke((MethodInvoker) delegate {
				// Deletes the old data center labels
				if (this.dataCenters != null) {
					foreach(DataCenterButton dc in this.dataCenters) {
						this.Controls.Remove(dc);
						dc.Dispose();
					}
				}

				// Displays the new data center labels
				this.dataCenters = dataCentersInfo;
				for (int i = 0; i < dataCentersInfo.Length; i++) {
					this.dataCenters[i].Size = new Size(this.Size.Width - 20, 40);
					this.dataCenters[i].Location = new Point(10, 80 + (i * (this.dataCenters[i].Size.Height + 10)));
					this.Controls.Add(this.dataCenters[i]);
					this.dataCenters[i].BringToFront();
					this.dataCenters[i].Parent = this;
				}
			});
		}

		public void handleDataCenterSelect(List<byte> payload) {
			// Parses the datacenter info
			uint ip = Connection.getUIntFromBuffer(payload);
			ushort port = Connection.getUShortFromBuffer(payload);

			//string ipStr = "" + (int)(ip/Math.Pow(256,3)) + "." + (int) ((ip / Math.Pow(256, 2))%256) + "." + (int) ((ip/256)% Math.Pow(256, 2)) + "." + (int)(ip % 256);
			string ipStr = new IPAddress((uint) IPAddress.HostToNetworkOrder((int) ip)).ToString();

			// Closes the current window and opens the connection window
			Invoke((MethodInvoker) delegate {
				this.Hide();

				if ((currentForm = new ConnectingWindow(ipStr, port)).ShowDialog() == DialogResult.Abort) {
					this.Close();
					return;
				}

				currentForm.Dispose();

				this.Show();
			});
		}

		public void handleSessionReqeust(List<byte> payload) {
			Connection.sendMessage(Connection.getByteAsString((byte) this.Session.Length) + this.Session);
		}

		public void handleCharacterList(List<byte> payload) {
			int len = Connection.getByteFromBuffer(payload);
			Character[] characters = new Character[len];

			// Reads the characters from the payload
			for (int i = 0; i < len; i++) {
				characters[i] = new Character(payload);
			}

			new Thread(new ThreadStart(delegate {
				Invoke((MethodInvoker) delegate {
					// Opens a new window
					if (this.currentForm != null && !(this.currentForm is CharacterSelect)) {
						this.currentForm.Dispose();
						this.currentForm = null;
					}

					// Creates a new window if needed and displays the characters
					if (this.currentForm == null) {
						this.currentForm = new CharacterSelect();
						this.Hide();
						((CharacterSelect) this.currentForm).DisplayCharactrs(characters);
						if (currentForm.ShowDialog() == DialogResult.Abort) {
							this.Close();
							return;
						}
					} else {
						((CharacterSelect) this.currentForm).DisplayCharactrs(characters);
					}
				});
			})).Start();
		}

		public void handleCharacterCreateResult(List<byte> payload) {
			if (this.currentForm is CharacterSelect) {
				((CharacterSelect) this.currentForm).DisplayCharacterCreateResult
					((CharacterSelect.CharacterCreateResult) Connection.getByteFromBuffer(payload));
			}
		}

		public void handleServerRet(List<byte> payload) {
			uint ip = Connection.getUIntFromBuffer(payload);
			ushort port = Connection.getUShortFromBuffer(payload);

			Process process = new Process();
			process.StartInfo.FileName = "Game.exe";
			process.StartInfo.Arguments = Program.MainForm.Session + " " + CharacterName + " " + ip.ToString() + " " + port.ToString();
			process.Start();
			Application.Exit();
		}

		private void MainMenu_Load(object sender, EventArgs e) {
			try {
				this.Hide();
				if ((currentForm = new ConnectingWindow()).ShowDialog() == DialogResult.Abort) {
					this.Close();
					return;
				}
				currentForm.Dispose();
				if ((currentForm = new LoginWindow()).ShowDialog() == DialogResult.Abort) {
					this.Close();
					return;
				}
				currentForm.Dispose();
				currentForm = null;

				this.Show();
				Connection.sendMessage((byte) Connection.LoginServerMessageType.DATA_CENTER_LIST_REQ);
			} catch (Exception) { }
		}


	}
}
