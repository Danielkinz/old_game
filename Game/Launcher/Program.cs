﻿using System;
using System.Net;
using System.Windows.Forms;

namespace Client {
	static class Program {

		private static MainMenu mainMenu;
		private static IPAddress ip = IPAddress.Parse("109.67.127.13");
		private static ushort port = 5300;

		public static MainMenu MainForm { get { return mainMenu; } }
		public static IPAddress IP { get { return ip; } }
		public static ushort Port { get { return port; } }
		public static bool DEBUG { get { return false; } }
		public static bool IsRunning = false;

		[STAThread]
		static void Main() {
			Application.ApplicationExit += delegate {
				IsRunning = false;
				Connection.Disconnect();
			};

			IsRunning = true;
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(mainMenu = new MainMenu());
		}
	}
}
