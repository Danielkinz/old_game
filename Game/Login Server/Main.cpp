#include <thread>
#include <iostream>
#include "net\Server.h"
#include "net\ServerData.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Logger.h"
#include "dcl\tools\StringBuilder.h"

#define STOP getchar(); return 1

using namespace std;
using namespace login_server::net;
using namespace dcl::net;
using namespace dcl::tools;
using namespace boost::asio;

int main(int argc, char* argv[]) {
	// Initialization
	Logger logger("Global");
	logger.log("Login server starting...");

	if (argc < 2) {
		cout << "Syntax: LoginServer.exe <port>" << endl;
		STOP;
	}

	try {
		io_service service;
		Server::start(service, stoul(argv[1]));

		logger.log("Login server is now running!");
		logger.log("Press enter to stop the server");
		getchar();

		logger.log("Stopping...");
		Server::stop();
	} catch (sql::SQLException& e) {
		logger.log(StringBuilder("SQL Exception:\n (", e.getErrorCode(), ") ", e.what()), LogLevel::fatal);
		STOP;
	} catch (Exception& e) {
		logger.log(e, LogLevel::fatal);
		STOP;
	}
	
	return 0;
}
