#include "DataCenter.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"

using namespace std;
using namespace login_server::net;
using namespace login_server::net::client;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;

boost::unordered_map<DataCenter::MessageType, DataCenter::ProtocolHandler> DataCenter::_handlers;

/* ---------------------------- STATIC ---------------------------- */

void DataCenter::init() {
	_handlers[MessageType::DISCONNECT] = &DataCenter::DisconnectHandler;
}

/* ---------------------------- OBJECT ---------------------------- */

DataCenter::DataCenter(dcl::net::Session& session) : Session(session), _name("New Data Center") , _logger(_name) {
	string str;
	byte len;
	Buffer<byte> buffer;

	// Validates the DataCenter using the certificate
	sendData(MessageType::VALIDATION);
	read(buffer);
	str = string((char*) buffer.Read(DATA_CENTER_CERTIFICATE_LENGTH), DATA_CENTER_CERTIFICATE_LENGTH);

	if (str != DATA_CENTER_CERTIFICATE) {
		disconnect();
		throw ValidationException();
	}

	// Retrieves the data center name
	sendData(MessageType::NAME_REQ);
	len = read(buffer).Read();
	str = string((char*) buffer.Read(len), len);

	_name = str;
	_logger.setName(_name);

	// Retrieves the port on which the data center receives clients
	sendData(MessageType::PORT_REQ);
	read(buffer);
	_port = buffer.Read<ushort>();

	_logger.log(StringBuilder("Data center connected: from ", getSocket().remote_endpoint().address().to_string(), ":", _port));
}

DataCenter::DataCenter(DataCenter& other) : Session(other), _name(other._name), _logger(other._name) {}

DataCenter& DataCenter::operator=(dcl::net::Session& other) {}

DataCenter& DataCenter::operator=(DataCenter& other) {}

void DataCenter::sendData(byte code, const string& payload) {
	Buffer<byte> buff(&code, 1);
	buff.Write((byte*)payload.data(), payload.length());
	Session::sendData(buff);
}

void DataCenter::sendData(MessageType code, const std::string& payload) {
	sendData((byte)code, payload);
}

const string& DataCenter::getName() const {
	return _name;
}

unsigned short DataCenter::getPort() const {
	return _port;
}

void DataCenter::disconnect() {
	sendData(MessageType::DISCONNECT);
	Session::disconnect();
}

void DataCenter::transferSession(const string& user, const string& session) {
	_logger.log(StringBuilder("Transferring session for user \"", user, "\" (", session, ")"));
	sendData(MessageType::SESSION_TRANSFER,
		Protocol::ToString<byte>(user.length()) + user +
		Protocol::ToString<byte>(session.length()) + session);
	Session::read().Read();
}

/* ---------------------------- Handlers ---------------------------- */

DataCenter::Message DataCenter::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::DISCONNECT:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void DataCenter::DisconnectHandler(DataCenter::Message& msg) {
	_logger.log(StringBuilder("disconnected"));
	_dataCenters.erase(this->_name);
	Session::disconnect();
}
