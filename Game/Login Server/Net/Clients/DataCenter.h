#ifndef DATA_CENTER_H
#define DATA_CENTER_H

#include "net\Database.h"
#include <boost\unordered_map.hpp>
#include "dcl\net\Messages.h"
#include "dcl\net\Session.h"
#include "dcl\structures\Buffer.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Logger.h"

namespace login_server {
	namespace net {
		namespace client {

			class DataCenter final : public dcl::net::Session {
			private: // Types
				typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
				typedef std::shared_ptr<message_t> Message;
				typedef void(DataCenter::*ProtocolHandler)(Message&);

				// The message codes that define the protocol
				enum class MessageType : byte {
					VALIDATION, NAME_REQ, PORT_REQ,
					SESSION_TRANSFER, 
					DISCONNECT = 255
				};

			private: // Static

				// maps message codes to their handlers
				static boost::unordered_map<MessageType, ProtocolHandler> _handlers;

			public: // Static
				static void init();

			private: // Object
				std::string _name;
				dcl::tools::Logger _logger;
				unsigned short _port;

				void sendData(byte code, const std::string& payload = "");
				void sendData(MessageType code, const std::string& payload = "");

			public: // Object
				DataCenter(dcl::net::Session&);
				DataCenter(DataCenter&);
				DataCenter& operator=(dcl::net::Session&);
				DataCenter& operator=(DataCenter&);

				const std::string& getName() const;
				unsigned short getPort() const;

				virtual void disconnect();

				// Sends the user's new active session to the DataCenter
				void transferSession(const std::string& user, const std::string& session);

			private: // Handlers
				// Parses the arguments from the received message
				Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

				// Protocol Handlers
				void DisconnectHandler(Message&);
			};
		}
	}
}

#endif