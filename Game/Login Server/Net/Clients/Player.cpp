#include "Player.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"

using namespace std;
using namespace login_server::net;
using namespace login_server::net::client;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;

boost::unordered_map<Player::MessageType, Player::ProtocolHandler> Player::_handlers;

/* ---------------------------- STATIC ---------------------------- */

void Player::init() {
	_handlers[MessageType::SIGN_IN_REQ] = &Player::SignInHandler;
	_handlers[MessageType::REGISTER_REQ] = &Player::RegisterHandler;
	_handlers[MessageType::CHANGE_EMAIL_REQ] = &Player::ChangeEmailHandler;
	_handlers[MessageType::CHANGE_PASS_REQ] = &Player::ChangePasswordHandler;
	_handlers[MessageType::DATA_CENTER_LIST_REQ] = &Player::DataCenterListHandler;
	_handlers[MessageType::DATA_CENTER_SELECT_REQ] = &Player::DataCenterSelectHandler;

	_handlers[MessageType::DISCONNECT] = &Player::DisconnectHandler;
}

string Player::createSession(const string& username) const {
	srand(time(NULL) + rand());
	stringstream ss;
	int c;

	for (int i = 0; i < SESSION_LENGTH; i++) {
		c = (rand() + username[i % username.size()]) % 52 + 'A';
		if (c > 'Z') c += 6;
		ss << (char)c;
	}

	return ss.str();
}

/* ---------------------------- OBJECT ---------------------------- */

Player::Player(dcl::net::Session& session) : Session(session), _logger("Unregistered Player") {
	Session::startListening();
}

Player::Player(Player& other) : Session(other), _logger(other._logger) {
	this->_username = other._username;
	this->_session = other._session;
	Session::startListening();
}

Player& Player::operator=(dcl::net::Session& other) {
	Session::operator=(other);
	Session::startListening();
}

Player& Player::operator=(Player& other) {
	Session::operator=(other);

	this->_username = other._username;
	this->_session = other._session;
	this->_logger = other._logger;
	Session::startListening();
}

void Player::sendData(const MessageType code, const string& payload) {
	Buffer<byte> buff((byte*) &code, 1);
	buff.Write((byte*) payload.data(), payload.length());
	Session::sendData(buff);
}

void Player::setSelf(shared_ptr<Player>& self) {
	_self = self;
}

void Player::disconnect() {
	_players.erase(_self);
	Session::disconnect();
}

/* ---------------------------- Handlers ---------------------------- */

bool Player::handleRead(dcl::structures::Buffer<byte>& buffer, const boost::system::error_code& error) {
	Message msg;
	
	if (error) {
		if (error.value() == 10009); // Disconnected message already displayed
		else if (error.value() == 2) _logger.log("disconnected");
		else _logger.log(StringBuilder("Read error: (", error.value(), ") ", error.message()));
		return false;
	}
	try {
		msg = buildReceivedMessage(buffer);
		(this->*(_handlers[(MessageType)msg->getMessageCode()]))(msg);
		return true;
	} catch (Exception& e) {
		_logger.log(e);
		_logger.log("Disconnecting player");
		return false;
	}
}

Player::Message Player::buildReceivedMessage(Buffer<byte>& payload) {
	vector<string> arguments;
	byte code = payload.Read<byte>();

	// Parses the arguements according to the protocol
	switch ((MessageType)code) {
	case MessageType::REGISTER_REQ: // User, Pass, Email
	case MessageType::CHANGE_EMAIL_REQ: // User, Pass, newEmail
	case MessageType::CHANGE_PASS_REQ: // User, oldPass, newPass
		arguments.push_back(Protocol::ReadStringFromBuffer(payload, payload.Read<byte>()));
	case MessageType::SIGN_IN_REQ: // User, Pass
		arguments.push_back(Protocol::ReadStringFromBuffer(payload, payload.Read<byte>()));
	case MessageType::DATA_CENTER_SELECT_REQ: // DataCenterName
		arguments.push_back(Protocol::ReadStringFromBuffer(payload, payload.Read<byte>()));
	case MessageType::DATA_CENTER_LIST_REQ:
	case MessageType::DISCONNECT:
		break;
	default:
		throw Exception("Invalid message received from client! (", (int)code, ")");
	}

	return make_shared<message_t>(code, arguments);
}

void Player::SignInHandler(Message& msg) {
	string& user = msg->getValue()[0];
	string& pass = msg->getValue()[1];

	if (_database.checkUser(user, pass)) {
		this->_username = user;
		_logger.setName(_username);
		_session = createSession(_username);
		_database.setSession(_username, _session);
		
		sendData(MessageType::SIGN_IN_RET, Protocol::ToString<byte>((byte)SignInResult::SUCCESS) + _session);
		_logger.log(StringBuilder("Logged in from \"", getSocket().remote_endpoint().address().to_string(), "\""));
		_logger.log(StringBuilder("Received session: \"", _session, "\""));
	} else {
		sendData(MessageType::SIGN_IN_RET, Protocol::ToString<byte>((byte)SignInResult::FAIL));
		_logger.log(StringBuilder("Failed to log in. User: \"", user, "\", Password: \"", pass, "\""));
	}
}

void Player::RegisterHandler(Message& msg) {
	string& user = msg->getValue()[0];
	string& pass = msg->getValue()[1];
	string& email = msg->getValue()[2];

	if (_database.usernameTaken(user)) {
		_logger.log(StringBuilder("Tried to register with taken username: Username: ", user, ", Password: ", pass, ", Email: ", email));
		sendData(MessageType::REGISTER_RET, Protocol::ToString<byte>((byte)RegisterResult::USERNAME_TAKEN));
		return;
	}

	if (_database.emailTaken(email)) {
		_logger.log(StringBuilder("Tried to register with taken email: Username: ", user, ", Password: ", pass, ", Email: ", email));
		sendData(MessageType::REGISTER_RET, Protocol::ToString<byte>((byte)RegisterResult::EMAIL_TAKEN));
		return;
	}

	_logger.log(StringBuilder("Registered a new account: Username: ", user, ", Password: ", pass, ", Email: ", email));
	_database.addUser(user, pass, email);
	sendData(MessageType::REGISTER_RET, Protocol::ToString<byte>((byte)RegisterResult::SUCCESS));
}

void Player::ChangeEmailHandler(Message& msg) {
	string& user = msg->getValue()[0];
	string& pass = msg->getValue()[1];
	string& newEmail = msg->getValue()[2];

	if (!_database.checkUser(user, pass)) {
		_logger.log(StringBuilder("Failed to change email of user \"", user ,"\""));
		sendData(MessageType::CHANGE_EMAIL_RET, Protocol::ToString<byte>(0));
		return;
	}

	_database.changeEmail(user, newEmail);
	sendData(MessageType::CHANGE_EMAIL_RET, Protocol::ToString<byte>(1));
}

void Player::ChangePasswordHandler(Message& msg) {
	string& user = msg->getValue()[0];
	string& oldPass = msg->getValue()[1];
	string& newPass = msg->getValue()[2];

	if (!_database.checkUser(user, oldPass)) {
		_logger.log(StringBuilder("Failed to change password of user \"", user, "\""));
		sendData(MessageType::CHANGE_PASS_RET, Protocol::ToString<byte>(0));
		return;
	}

	_logger.log(StringBuilder("Changed password of user \"", user, "\" to \"", newPass, "\""));
	_database.changePassword(user, newPass);
	sendData(MessageType::CHANGE_PASS_RET, Protocol::ToString<byte>(1));
}

void Player::DataCenterListHandler(Message&) {
	string payload = Protocol::ToString<byte>(_dataCenters.size());
	
	_logger.log("Requested DataCenter list");

	for (auto dc : _dataCenters)
		payload += Protocol::ToString<byte>(dc.first.size()) + dc.first;
	sendData(MessageType::DATA_CENTER_LIST_RET, payload);
}

void Player::DataCenterSelectHandler(Message& msg) {
	string& requestedCenter = msg->getValue()[0];
	string payload =
		Protocol::ToString<uint>(_dataCenters[requestedCenter]->getSocket().remote_endpoint().address().to_v4().to_uint()) +
		Protocol::ToString<ushort>(_dataCenters[requestedCenter]->getPort());

	_dataCenters[requestedCenter]->transferSession(this->_username, this->_session);
	_logger.log(StringBuilder("Requested transfer to datacenter \"", requestedCenter, "\""));
	sendData(MessageType::DATA_CENTER_SELECT_RET, payload);
}

void Player::DisconnectHandler(Message& msg) {
	_logger.log("disconnected");
	_players.erase(_self);
	Session::disconnect();
	_self = nullptr;
}
