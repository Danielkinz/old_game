#ifndef PLAYER_H
#define PLAYER_H

#include "net\Database.h"
#include <boost\unordered_map.hpp>
#include "dcl\net\Messages.h"
#include "dcl\net\Session.h"
#include "dcl\structures\Buffer.h"
#include "dcl\tools\Logger.h"

namespace login_server {
	namespace net {
		namespace client {
			typedef unsigned char byte;

			class Player final : public dcl::net::Session {
			private: // Types
				typedef dcl::net::ReceivedMessage<std::vector<std::string>> message_t;
				typedef std::shared_ptr<message_t> Message;
				typedef void(Player::*ProtocolHandler)(Message&);

				// Messages codes that specify the protocol
				enum class MessageType : byte {
					SIGN_IN_REQ, SIGN_IN_RET,
					REGISTER_REQ, REGISTER_RET,
					CHANGE_PASS_REQ, CHANGE_PASS_RET,
					CHANGE_EMAIL_REQ, CHANGE_EMAIL_RET,
					DATA_CENTER_LIST_REQ, DATA_CENTER_LIST_RET,
					DATA_CENTER_SELECT_REQ, DATA_CENTER_SELECT_RET,
					DISCONNECT = 255
				};

				enum class SignInResult : byte { FAIL, SUCCESS, ALREADY_CONNECTED, ADMIN_SUCCESS };
				enum class RegisterResult : byte { SUCCESS, ILLEGAL_PASSWORD, USERNAME_TAKEN, USERNAME_ILLEGAL, EMAIL_ILLEGAL, EMAIL_TAKEN, OTHER };

			private: // Static
				// Maps a message code to its protocol handler
				static boost::unordered_map<MessageType, ProtocolHandler> _handlers;

			public: // Static
				static void init();
				std::string createSession(const std::string& username) const;

			private: // Object
				std::string _username;
				std::string _session;
				dcl::tools::Logger _logger;
				std::shared_ptr<Player> _self;

				void sendData(const MessageType code, const std::string& payload);

			public: // Object
				Player(dcl::net::Session&);
				Player(Player&);
				Player& operator=(dcl::net::Session&);
				Player& operator=(Player&);

				void setSelf(std::shared_ptr<Player>&);
				void disconnect();

			protected: // Handlers
				// Handles new messages
				bool handleRead(dcl::structures::Buffer<byte>&, const boost::system::error_code&);

			private: // Handlers
				// Parses the arguments of the received messages
				Message buildReceivedMessage(dcl::structures::Buffer<byte>& payload);

				// protocol handlers

				void SignInHandler(Message&);
				void RegisterHandler(Message&);
				void ChangeEmailHandler(Message&);
				void ChangePasswordHandler(Message&);
				void DataCenterListHandler(Message&);
				void DataCenterSelectHandler(Message&);

				void DisconnectHandler(Message&);
			};
		}
	}
}

#endif