#include "Database.h"

#include "dcl\tools\Utils.h"
#include "dcl\tools\Console.h"

using namespace std;
using namespace sql;
using namespace login_server::net;

Database::Database() : MySQL_DB() {}

Database::~Database() {
	// Deletes all the prepared statements
	for (auto it = _statements.begin(); it != _statements.end(); it++) {
		if (it->second) {
			it->second->close();
			SAFE_DELETE(it->second);
		}
	}
}

Database::Database(const Database& other) : MySQL_DB(other) {
	_statements = other._statements;
}

Database& Database::operator=(const Database& other) {
	MySQL_DB::operator=(other);
	_statements = other._statements;
	return *this;
}

void Database::connect(const std::string& url, const std::string& username, const std::string& password) {
	MySQL_DB::connect(url, username, password, "LoginServerDB");

	// Initializes all the prepared statements
	_statements[ADD_USER] = getConnection()->prepareStatement("CALL addUser(?,?,?);");
	_statements[USERNAME_TAKEN] = getConnection()->prepareStatement("SELECT usernameTaken(?) AS result;");
	_statements[EMAIL_TAKEN] = getConnection()->prepareStatement("SELECT emailTaken(?) AS result");
	_statements[CHECK_USER] = getConnection()->prepareStatement("SELECT checkUser(?,?) AS result;");
	_statements[CHANGE_EMAIL] = getConnection()->prepareStatement("CALL changeEmail(?,?);");
	_statements[CHANGE_PASSWORD] = getConnection()->prepareStatement("CALL changePassword(?,?);");
	_statements[SET_SESSION] = getConnection()->prepareStatement("CALL setSession(?,?);");
}

#define LOCK_MUTEX this->_mtx.lock()
#define UNLOCK_MUTEX this->_mtx.unlock()
#define GUARD_MUTEX std::lock_guard<std::recursive_mutex> lock(this->_mtx)

int Database::addUser(const string& username, const string& password, const string& email) {
	GUARD_MUTEX;
	//byte digest[CryptoPP::SHA1::DIGESTSIZE];
	//CryptoPP::SHA1().CalculateDigest(digest, (const byte*) password.c_str(), password.length());

	sql::PreparedStatement* stmt = _statements[ADD_USER];
	stmt->setString(1, username);
	stmt->setString(2, password);
	stmt->setString(3, email);
	return stmt->executeUpdate();
}

bool Database::usernameTaken(const string& username) {
	unique_ptr<ResultSet> rs = nullptr;

	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[USERNAME_TAKEN];
	stmt->setString(1, username);
	rs.reset(stmt->executeQuery());

	if (!rs->next()) return false;
	return rs->getBoolean("result");
}

bool Database::emailTaken(const string& email) {
	GUARD_MUTEX;
	unique_ptr<ResultSet> rs = nullptr;
	sql::PreparedStatement* stmt = _statements[EMAIL_TAKEN];
	stmt->setString(1, email);
	rs.reset(stmt->executeQuery());

	if (!rs->next()) return false;
	return rs->getBoolean("result");
}

bool Database::checkUser(const string& username, const string& password) {
	GUARD_MUTEX;
	unique_ptr<ResultSet> rs = nullptr;
	//byte digest[CryptoPP::SHA1::DIGESTSIZE];
	//CryptoPP::SHA1().CalculateDigest(digest, (const byte*) password.c_str(), password.length());

	sql::PreparedStatement* stmt = _statements[CHECK_USER];
	stmt->setString(1, username);
	stmt->setString(2, password);
	rs.reset(stmt->executeQuery());

	if (!rs->next()) return false;
	return rs->getBoolean("result");
}

int Database::changeEmail(const string& username, const string& email) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[CHANGE_EMAIL];
	stmt->setString(1, username);
	stmt->setString(2, email);
	return stmt->executeUpdate();
}

int Database::changePassword(const string& username, const string& password) {
	GUARD_MUTEX;
	//byte digest[CryptoPP::SHA1::DIGESTSIZE];
	//CryptoPP::SHA1().CalculateDigest(digest, (const byte*) password.c_str(), password.length());

	sql::PreparedStatement* stmt = _statements[CHANGE_PASSWORD];
	stmt->setString(1, username);
	stmt->setString(2, password);
	return stmt->executeUpdate();
}

int Database::setSession(const string& username, const string& session) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[SET_SESSION];
	stmt->setString(1, username);
	stmt->setString(2, session);
	return stmt->executeUpdate();
}

int Database::clearSession(const string& username) {
	GUARD_MUTEX;
	sql::PreparedStatement* stmt = _statements[SET_SESSION];
	stmt->setString(1, username);
	stmt->setNull(2, sql::DataType::CHAR);
	return stmt->executeUpdate();
}

#undef LOCK_MUTEX
#undef UNLOCK_MUTEX
#undef GUARD_MUTEX
