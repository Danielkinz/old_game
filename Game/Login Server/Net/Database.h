#ifndef DATABASE_H
#define DATABASE_H

#include <mutex>
#include <cryptopp\sha.h>
#include "dcl\net\MySQL_DB.h"

namespace login_server {
	namespace net {
#define DEFAULT_URL "localhost:3306"
#define DEFAULT_USERNAME "Daniel"
#define DEFAULT_PASSWORD "1234"

		class Database final : public dcl::net::MySQL_DB {
		private:
			// Prepared Statement ID's
			enum Statements { 
				ADD_USER, USERNAME_TAKEN, 
				EMAIL_TAKEN, CHECK_USER, 
				CHANGE_EMAIL, CHANGE_PASSWORD, 
				SET_SESSION 
			};

			// A map of Prepared Statement ID's to prepared statements
			std::map<Statements, sql::PreparedStatement*> _statements;
			std::recursive_mutex _mtx;

		public:
			Database();
			virtual ~Database();
			void connect(const std::string& url = DEFAULT_URL, const std::string& username = DEFAULT_USERNAME, const std::string& password = DEFAULT_PASSWORD);

			Database(const Database&);
			Database& operator=(const Database&);

			// Creates a new user with the respective credentials
			int addUser(const std::string& username, const std::string& password, const std::string& email);
			
			// Checks if the username @username is already used
			bool usernameTaken(const std::string& username);

			// Checks if the email @email is already used
			bool emailTaken(const std::string& email);

			// Checks if the credentials @username and @password match
			bool checkUser(const std::string& username, const std::string& password);
			
			// Changes the email of the user called @username to @email
			int changeEmail(const std::string& username, const std::string& email);

			// Changes the password of the user called @username to @password
			int changePassword(const std::string& username, const std::string& password);

			// Sets the active session id of the user called @username to @session
			int setSession(const std::string& username, const std::string& session);

			// Clears the sessionID the user @usernaem currently has active
			int clearSession(const std::string& username);
		};
	}
}

#endif