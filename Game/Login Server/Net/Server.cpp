#include "Server.h"
#include "Clients/GameServer.h"

#include <thread>

using namespace std;
using namespace boost::asio;
using namespace login_server::net;
using namespace login_server::net::client;
using namespace dcl::net;
using namespace dcl::tools;
using namespace dcl::structures;

Server* __server = nullptr;
thread* __server_thread = nullptr;
io_context* __context = nullptr;

/* ---------------------------- OBJECT ---------------------------- */

Server::Server(io_service& service, ushort port) : TCPServer(service, port) {
	ServerData::init();
	Player::init();
	DataCenter::init();
}

void Server::acceptHandler(Session* new_session, const boost::system::error_code& error) {
	if (error) {
		delete new_session;
		return;
	}

	Buffer<byte>& payload = new_session->read();
	byte b = payload.Read<byte>();

	if (b == 0) { // Client
		_SERVER_LOGGER.log("New player connected (" + new_session->getSocket().remote_endpoint().address().to_string() + ")");
		auto player = std::make_shared<Player>(*new_session);
		player->setSelf(player);
		_players.insert(player);
	} else if (b == 1) { // Data Center
		try {
			shared_ptr<DataCenter> center = make_shared<DataCenter>(*new_session);
			_dataCenters[center->getName()] = center;
		} catch (ValidationException& e) {
			_SERVER_LOGGER.log(e);
		}

	} else if (b == 2) { // Game Server
		GameServer gs(*new_session);

	} else { // Error
		_SERVER_LOGGER.log("Error while connecting to new peer: Invalid login code (" + new_session->getSocket().remote_endpoint().address().to_string() + ")", LogLevel::error);
		new_session->disconnect();
	}

	delete new_session;
}

Server::~Server() {
	for (auto p : _players)
		p->disconnect();
	
	for (auto dc : _dataCenters)
		dc.second->disconnect();

	_players.clear();
	_dataCenters.clear();
	_database.disconnect();
	TCPServer::shutDown();
}

/* ---------------------------- STATIC ---------------------------- */

void Server::startIOService(boost::asio::io_service* service) {
	service->run();
}

void Server::start(boost::asio::io_service& service, ushort port) {
	if (__server) throw Exception("Server already running!");
	__context = &service;
	__server = new Server(service, port);
	__server_thread = new thread(&Server::startIOService, &service);
}

void Server::stop() {
	SAFE_DELETE(__server);
	if (!__context->stopped()) __context->stop();
	__server_thread->join();
	SAFE_DELETE(__server_thread);
}