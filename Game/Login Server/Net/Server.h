#ifndef SERVER_H
#define SERVER_H

#include <boost\asio.hpp>
#include "net\ServerData.h"
#include "dcl\net\TCPServer.h"

namespace login_server {
	namespace net {
		typedef unsigned short ushort;

		class Server final : public dcl::net::TCPServer {
		private: // Object
			Server(boost::asio::io_service&, ushort port);

			// Handlers

			// new connection event handler (TCPServer)
			void acceptHandler(dcl::net::Session*, const boost::system::error_code&);

		public: // Object
			~Server();

		private: // Static
			// Function that starts the IO service, RUN FROM A NEW THREAD
			static void startIOService(boost::asio::io_service*);

		public: // Static
			static void start(boost::asio::io_service&, ushort port);
			static void stop();
		};
	}
}

#endif