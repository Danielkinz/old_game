#include "GameServer.h"
#include "net\ServerData.h"
#include "dcl\net\Protocol.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Console.h"
#include "dcl\tools\StringBuilder.h"

using namespace std;
using namespace login_server::net;
using namespace login_server::net::client;
using namespace dcl::structures;
using namespace dcl::net;
using namespace dcl::tools;

/* ---------------------------- OBJECT ---------------------------- */

GameServer::GameServer(dcl::net::Session& session) : Session(session) {
	string str;
	byte len;
	Buffer<byte> buffer;

	// Validates the game server using the certificate
	buffer.Write<byte>((byte) MessageType::VALIDATION);
	sendData(buffer);
	buffer.clear();
	read(buffer);
	str = string((char*)buffer.Read(GAME_SERVER_CERTIFICATE_LENGTH), GAME_SERVER_CERTIFICATE_LENGTH);

	if (str != GAME_SERVER_CERTIFICATE) {
		disconnect();
		throw ValidationException();
	}

	// Retrieves the data center name
	buffer.clear();
	buffer.Write((byte)MessageType::NAME_REQ);
	sendData(buffer);
	buffer.clear();
	len = read(buffer).Read();
	str = string((char*)buffer.Read(len), len);

	if (_dataCenters.find(str) != _dataCenters.end()) {
		buffer.clear();
		buffer.Write<byte>((byte)MessageType::DATA_RET);
		buffer.Write<uint>(_dataCenters[str]->getSocket().remote_endpoint().address().to_v4().to_uint());
		buffer.Write<ushort>(_dataCenters[str]->getPort());
		sendData(buffer);
	} else {
		buffer.clear();
		buffer.Write<byte>((byte)MessageType::DATA_FAIL);
		sendData(buffer);
	}

	disconnect();
}

void GameServer::disconnect() {
	Buffer<byte> buff;
	buff.Write((byte)MessageType::DISCONNECT);
	Session::disconnect();
}
