#ifndef GAME_SERVER_H
#define GAME_SERVER_H

#include "net\Database.h"
#include <boost\unordered_map.hpp>
#include "dcl\net\Messages.h"
#include "dcl\net\Session.h"
#include "dcl\structures\Buffer.h"
#include "dcl\tools\Exception.h"
#include "dcl\tools\Logger.h"

namespace login_server {
	namespace net {
		namespace client {

			class GameServer final : public dcl::net::Session {
			private:

				// The message codes that define the protocol
				enum class MessageType : byte {
					VALIDATION, NAME_REQ, DATA_RET, DATA_FAIL,
					DISCONNECT = 255
				};

			private:
				using dcl::net::Session::sendData;

			public: // Object
				GameServer(dcl::net::Session&);
				GameServer(GameServer&) = delete;
				GameServer& operator=(dcl::net::Session&) = delete;
				GameServer& operator=(GameServer&) = delete;

				virtual void disconnect();
			};
		}
	}
}

#endif