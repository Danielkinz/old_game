#include "ServerData.h"

using namespace login_server::net;

Database ServerData::database;
boost::unordered_set<std::shared_ptr<login_server::net::client::Player>> ServerData::players;
boost::unordered_map<std::string, std::shared_ptr<login_server::net::client::DataCenter>> ServerData::dataCenters;
dcl::tools::Logger ServerData::logger("Server");

void ServerData::init() {
	database.connect();
}