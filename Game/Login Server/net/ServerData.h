#ifndef SERVER_DATA_H
#define SERVER_DATA_H

#include <boost\unordered_map.hpp>
#include <boost\unordered_set.hpp>
#include "net\Database.h"
#include "net\clients\Player.h"
#include "net\clients\DataCenter.h"

namespace login_server {
	namespace net {
		// Class for storing data that is shared accross the entire program
		class ServerData final {
		public:
			static Database database;
			static boost::unordered_set<std::shared_ptr<login_server::net::client::Player>> players;
			static boost::unordered_map<std::string, std::shared_ptr<login_server::net::client::DataCenter>> dataCenters;
			static dcl::tools::Logger logger;

			static void init();
		};
	}
}

// constant names for the static data

#define _database ServerData::database
#define _SERVER_LOGGER ServerData::logger
#define _players ServerData::players
#define _dataCenters ServerData::dataCenters

// Data that is used accross the program

#define SESSION_LENGTH 64
#define DATA_CENTER_CERTIFICATE "ZDQ3NGMzbjczcmMzcjcxZjFjNDczdjRsMWQ0NzEwbg"
#define DATA_CENTER_CERTIFICATE_LENGTH strlen(DATA_CENTER_CERTIFICATE)

#define GAME_SERVER_CERTIFICATE "NTkyNjc1MDViYmRhZjc4ZWFlODkxOTZlMTg5YTBkZjI3OTI0Y2QzZQ"
#define GAME_SERVER_CERTIFICATE_LENGTH strlen(GAME_SERVER_CERTIFICATE)

 // Server initialization info

#define SERVER_PORT 5300

#endif