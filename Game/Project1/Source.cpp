//
//  main.cpp
//  lab6
//
//  Created by Omniah Nagoor on 5/26/1437 AH.
//  Copyright (c) 1437 Omniah Nagoor. All rights reserved.
/////////////////////////////////////////////////


////////////////
//  Headers   //
////////////////
#define _CRT_SECURE_NO_WARNINGS
#include <GL\glew.h>  // A cross-platform open-source C/C++ extension loading library
#include <stdlib.h>
#include <GL\glut.h>   // handle the window-managing operations
#include <iostream>

using namespace std;

//////////////////////
// Global variables //
//////////////////////

GLint win_width = 500,
win_hight = 500;

float transValueX;
float transValueY;
float transValueZ;
float near1;
float far1;

// buffers
GLuint myTexture1;

char image1Path[] = "/Users/mac/Dropbox/UQU/UQU 1436:semester 2/My courses/Computer Graphics/Labs/lab 4/lab 4/lab 4/rock.bmp";

///////////////////////////////////////////////


////////////////
//  Functions //
////////////////

void DrawCube() {
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	float size = 1.0f;

	float A[3] = { size,  size,  size };
	float B[3] = { size,  size, -size };
	float C[3] = { size, -size, -size };
	float D[3] = { size, -size,  size };
	float E[3] = { -size,  size,  size };
	float F[3] = { -size,  size, -size };
	float G[3] = { -size, -size, -size };
	float H[3] = { -size, -size,  size };
	glBegin(GL_QUADS);

	glColor3f(size, 0.0, size);
	glVertex3fv(D);
	glVertex3fv(C);
	glVertex3fv(B);
	glVertex3fv(A);

	//--------------------\\
		    
	glColor3f(0.0, 0.0, 0.0);
	glVertex3fv(G);
	glVertex3fv(H);
	glVertex3fv(E);
	glVertex3fv(F);

	//--------------------\\
		    
	glColor3f(size, 0.0, 0.0);
	glVertex3fv(C);
	glVertex3fv(G);
	glVertex3fv(F);
	glVertex3fv(B);

	//--------------------\\
		    
	glColor3f(0.0, 0.0, size);
	glVertex3fv(H);
	glVertex3fv(D);
	glVertex3fv(A);
	glVertex3fv(E);

	//--------------------\\
		    
	glColor3f(0.0, size, size);
	glVertex3fv(E);
	glVertex3fv(A);
	glVertex3fv(B);
	glVertex3fv(F);

	//--------------------\\
		    
	glColor3f(0.0, size, 0.0);
	glVertex3fv(G);
	glVertex3fv(C);
	glVertex3fv(D);
	glVertex3fv(H);

	glEnd();


}


//http://stackoverflow.com/questions/12518111/how-to-load-a-bmp-on-glut-to-use-it-as-a-texture
GLuint LoadTexture(const char * filename, int width, int height) {
	GLuint texture;
	unsigned char * data;
	FILE * file;

	//The following code will read in our RAW file
	file = fopen(filename, "rb");

	if (file == NULL) {
		cout << "Unable to open the image file" << endl << "Program will exit :(" << endl;
		exit(0);
		return 0;
	}

	data = (unsigned char *)malloc(width * height * 3);
	fread(data, width * height * 3, 1, file);

	fclose(file);


	// reorder the image colors to RGB not BGR
	for (int i = 0; i < width * height; ++i) {
		int index = i * 3;
		unsigned char B, R;
		B = data[index];
		R = data[index + 2];

		data[index] = R;
		data[index + 2] = B;

	}


	/////////////////////////////////////////
	// All Exercises TODO: load another texture image
	//////
	glGenTextures(1, &texture);            //generate the texture with the loaded data
	glBindTexture(GL_TEXTURE_2D, texture); //bind the texture to it's array

	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE); //set texture environment parameters

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	/////////////////////////////////////////

	free(data); //free the texture array

	if (glGetError() != GL_NO_ERROR)
		printf("GLError in genTexture()\n");

	return texture; //return whether it was successfull  
}

static
void special(int key, int x, int y) {
	//handle special keys
	switch (key) {

	case GLUT_KEY_HOME:

		break;
	case GLUT_KEY_LEFT:
		transValueX -= .1;
		cout << " translate x:" << transValueX << endl;
		break;
	case GLUT_KEY_RIGHT:
		transValueX += .1;
		cout << " translate x:" << transValueX << endl;
		break;
	case GLUT_KEY_UP:
		transValueY += .1;
		cout << " translate y:" << transValueY << endl;
		break;
	case GLUT_KEY_DOWN:
		transValueY -= .1;
		cout << " translate y:" << transValueY << endl;
		break;
	default:
		break;
		//----------------------
	}
}


static
void mouse(int button, int state, int x, int y) {

	if (button == GLUT_LEFT_BUTTON) {

		if (state == GLUT_UP) {

		}
	}


	if (button == GLUT_RIGHT_BUTTON) {

		if (state == GLUT_UP) {

		}
	}
}

static
void motion(int x, int y) {}

static void play(void) {
	glutPostRedisplay();
}

static
void key(unsigned char keyPressed, int x, int y) // key handling
{
	switch (keyPressed) {

	case 'z':
		transValueZ += .1;
		cout << " translate z:" << transValueZ << endl;
		break;

	case 'Z':
		transValueZ -= .1;
		cout << " translate z:" << transValueZ << endl;
		break;

	case 'n':
		near1 += .1;
		cout << "near value: " << near1 << endl;
		break;

	case 'N':
		near1 -= .1;
		cout << "near value: " << near1 << endl;
		break;

	case 'f':
		far1 += .1;
		cout << "far: " << far1 << endl;
		break;

	case 'F':
		far1 -= .1;
		cout << "far: " << far1 << endl;
		break;

	case 'q':
	case 27:
		exit(0);
		break;

	default:
		fprintf(stderr, "\nKeyboard commands:\n\n"
			"q, <esc> - Quit\n");
		break;

		//----------------------
	}
}

void init() {


	transValueX = 0.0;
	transValueY = 0.0;
	transValueZ = 2.0;
	near1 = 0.01;
	far1 = 50;


	//------- Texture ---------
	myTexture1 = LoadTexture(image1Path, 300, 225);

	if (myTexture1 == -1) {
		cout << "Error in loading the texture" << endl;
	} else
		cout << "The texture value is: " << myTexture1 << endl;


	/////////////////////////////////////////
	// Exercise 2 TODO: load another texture image
	//////

	//----------------------

	glClearColor(0.70f, 0.70f, 0.70f, 1.0f); // Set background color to black and opaque

	glMatrixMode(GL_PROJECTION);
	gluPerspective(65, (win_width / win_hight), 0.01, 50);

	glMatrixMode(GL_MODELVIEW);
	gluLookAt(5, 5, 5, 0, 0, 0, 0, 1, 0);

}

void reshapeFunc(GLint new_width, GLint new_hight) {
	glViewport(0, 0, new_width, new_hight);
	win_width = new_width;
	win_hight = new_hight;
}


////////////////
//  Display   //
////////////////
void display() {

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	/*
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective( 65, (win_width/win_hight), near, far);*/

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(transValueX, transValueY, transValueZ, 0, 0, 0, 0, 1, 0);
	//----------------------


	/////////////////////////////////////////
	// Exercise 1 TODO: add Texture code for the teapot here
	//////

	glColor3f(0.5, 0.5, 0.8);
	glEnable(GL_DEPTH_TEST);
	glutSolidTeapot(0.6f);
	glDisable(GL_DEPTH_TEST);

	/////////////////////////////////////////

	/////////////////////////////////////////
	// Exercise 2 TODO: add Texture code for the Quad
	//////
	/*
	glBindTexture( GL_TEXTURE_2D, myTexture2 );

	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(0,0,0);
	glTexCoord2f(1.0, 0.0); glVertex3f(1,0,0);
	//.... continue the other vertices and texCoord
	glEnd();*/

	/////////////////////////////////////////
	// Exercise 3 & 4 TODO: change the texture coordenates to be > 1
	//////


	/////////////////////////////////////////
	// Exercise 5 TODO: change the texture coordenates to be < 1
	//////


	glDisable(GL_TEXTURE_2D);
	/////////////////////////////////////////

	glFlush();
	glutSwapBuffers();
}

////////////////
//    Main    //
////////////////
int main(int argc, char** argv) {

	glutInit(&argc, argv);               					     	 // Initialize GLUT
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);     //  Specify the display Mode � RGB, RGBA or color                                                        //  Index, single or double Buffer
	glutInitWindowSize(500, 500);                                    // Set the window's initial width & height
	glutInitWindowPosition(250, 50); 								 // Position the window's initial top-left corner
	glutCreateWindow("OpenGL Lab 6");    							 // Create a window with the given title

	init();

	glutIdleFunc(play);
	glutDisplayFunc(display);
	glutReshapeFunc(reshapeFunc);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(key);
	glutSpecialFunc(special);

	glutMainLoop();           		// Enter the infinitely event-processing loop

	return 0;
}



