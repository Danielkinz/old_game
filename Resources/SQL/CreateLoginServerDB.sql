delimiter //

CREATE DATABASE IF NOT EXISTS LoginServerDB;//
USE LoginServerDB;//

CREATE TABLE IF NOT EXISTS Users (
	_username VARCHAR(36) NOT NULL PRIMARY KEY,
	_password VARCHAR(36) NOT NULL,
	_email VARCHAR(128) NOT NULL,
	_session_id CHAR(64) DEFAULT NULL
);//

DROP PROCEDURE IF EXISTS addUser;//
CREATE PROCEDURE addUser(IN username VARCHAR(36), IN password VARCHAR(36), IN email VARCHAR(128)) BEGIN
INSERT INTO Users (_username, _password, _email)
VALUES (username, password, email)
ON DUPLICATE KEY UPDATE _username = _username;
END;//

DROP FUNCTION IF EXISTS usernameTaken;//
CREATE FUNCTION usernameTaken(username VARCHAR(36)) RETURNS BOOLEAN NOT DETERMINISTIC
RETURN EXISTS(SELECT 1 FROM Users WHERE _username = username);//

DROP FUNCTION IF EXISTS emailTaken;//
CREATE FUNCTION emailTaken(email VARCHAR(128)) RETURNS BOOLEAN NOT DETERMINISTIC
RETURN EXISTS(SELECT 1 FROM Users WHERE _email = email);//

DROP FUNCTION IF EXISTS checkUser;//
CREATE FUNCTION checkUser(username VARCHAR(36), password VARCHAR(36)) RETURNS BOOLEAN NOT DETERMINISTIC
RETURN EXISTS(SELECT 1 FROM Users WHERE _username = username AND _password = password);//

DROP FUNCTION IF EXISTS checkSession;//
CREATE FUNCTION checkSession(username VARCHAR(36), session_id CHAR(64)) RETURNS BOOLEAN NOT DETERMINISTIC
RETURN EXISTS(SELECT 1 FROM Users WHERE _username = username AND session_id = session_id);//

DROP PROCEDURE IF EXISTS changeEmail;//
CREATE PROCEDURE changeEmail(IN username VARCHAR(36),IN email VARCHAR(128)) BEGIN
UPDATE Users SET _email = email WHERE _username = username;
END;//

DROP PROCEDURE IF EXISTS changePassword;//
CREATE PROCEDURE changePassword(IN username VARCHAR(36), IN password VARCHAR(36)) BEGIN
UPDATE Users SET _password = password WHERE _username = username;
END;//

DROP PROCEDURE IF EXISTS setSession;//
CREATE PROCEDURE setSession(IN username VARCHAR(36), IN session_id CHAR(64)) BEGIN
UPDATE Users SET _session_id = session_id WHERE _username = username;
END;//

delimiter ;
