delimiter //

CREATE DATABASE IF NOT EXISTS RegionalDB;//
USE RegionalDB;//

CREATE TABLE IF NOT EXISTS Users (
	_username VARCHAR(36) NOT NULL PRIMARY KEY,
	_session_id CHAR(64) DEFAULT NULL
);//

CREATE TABLE IF NOT EXISTS Characters (
	_character_name VARCHAR(36) NOT NULL PRIMARY KEY,
	_username VARCHAR(36) NOT NULL,
	FOREIGN KEY (_username) REFERENCES Users(_username) ON DELETE CASCADE
);//

DROP PROCEDURE IF EXISTS addUser;//
CREATE PROCEDURE addUser(IN username VARCHAR(36)) BEGIN
INSERT INTO Users(_username) VALUES (username)
ON DUPLICATE KEY UPDATE _username = _username;
END;//

DROP PROCEDURE IF EXISTS setUserSession;//
CREATE PROCEDURE setUserSession(IN username VARCHAR(36), IN session_id CHAR(64)) BEGIN
UPDATE Users SET _session_id = session_id WHERE _username = username;
END;//

DROP PROCEDURE IF EXISTS getUserBySession;//
CREATE PROCEDURE getUserBySession(IN session_id CHAR(64)) BEGIN
SELECT _username FROM Users WHERE _session_id = session_id;
END;//

DROP FUNCTION IF EXISTS isCharacterNameTaken;//
CREATE FUNCTION isCharacterNameTaken(character_name VARCHAR(36)) RETURNS BOOLEAN NOT DETERMINISTIC
RETURN EXISTS(SELECT 1 FROM Users WHERE _username = username);//

DROP PROCEDURE IF EXISTS createCharacter;//
CREATE PROCEDURE createCharacter(IN username VARCHAR(36), IN character_name VARCHAR(36)) BEGIN
INSERT INTO Characters(_character_name, _username)
VALUES (character_name, username)
ON DUPLICATE KEY UPDATE _username = _username;
END;//

DROP PROCEDURE IF EXISTS getCharacter;//
CREATE PROCEDURE getCharacter(IN character_name VARCHAR(36)) BEGIN
SELECT * FROM Characters WHERE _character_name = character_name;
END;//

DROP PROCEDURE IF EXISTS getCharactersList;//
CREATE PROCEDURE getCharactersList(IN username VARCHAR(36)) BEGIN
SELECT * FROM Characters WHERE _username = username;
END;//

delimiter ;
